<?php
class Invoice extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('invoice_model');
		
		if ($this->session->userdata('status') != "login") {
			redirect('auth');
		}
	}
	function index(){
		
		$data['title']='Invoice | Noname.com';
		
		$this->load->view('templates/header', $data);
		$this->load->view('invoice');
		$this->load->view('templates/footer');
	}

	function product_data(){
		$data=$this->invoice_model->product_list();
		echo json_encode($data);
	}

	function data_order(){
		$data=$this->invoice_model->getData_orders();
		echo json_encode($data);
	}

	function getNo_invoice(){
		$data=$this->invoice_model->getNo_invoice();
		echo json_encode($data);
	}

public function cetak_invoice()
	{	
		date_default_timezone_set('Asia/Jakarta');
		$id = $this->input->post('id');
		$catatan = $this->input->post('cat');

		$data_so =  $this->invoice_model->getData_so($id)->result();
		foreach ($data_so as $so) {
			$so_number = $so->so_number;
			$so_date = $so->so_date;
			$no_invoice = $so->nmr_invoice;
			$nama_pelanggan = $so->nama_pelanggan;
			$alamat_pelanggan = $so->alamat;
			$no_tlp_pelanggan = $so->no_tlp;
			$grand_total = $so->sub_total;
			$dp = $so->drop_payment;
			$kekurangan = $so->kekurangan;
			$pelunasan = $so->pelunasan;

			if ($pelunasan == '') {
						$pelunasan = '-';
						$pengembalian2 += 0;
					}else{
						$pelunasan = "Rp. " . number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $pelunasan),0,',','.');
		}
	}
		$grand_total = "Rp. " . number_format($grand_total,2,',','.');
		$dp = "Rp. ".$dp.",00";
		$kekurangan = "Rp. " . number_format($kekurangan,2,',','.');
		$this->load->library('fpdf');

		//Lebar A4= 210mm
		//Margin default= 10mm tiap sisi
		//Bidang Horizontal yang dapat ditulisi= 210 - (10*2) = 190mm

		$this->fpdf->AddPage("L");

		//SetFont ke Arial, B, 10pt
		$this->fpdf->SetFont('Arial','B', 15);

		$this->fpdf->Image(base_url().'assets/img/logo.png', 5, 0, 30, 0);

		//Cell(lebar, tinggi, teks, border, end line, [align])
		$this->fpdf->Cell(0, 15, 'DOUBLE A - HOUSE OF SUBLIMATION',0,1, 'C');
		$this->fpdf->Cell(0, 1, '', 0,1, 'C');
		$this->fpdf->Cell(0, 5,'INVOICE',0,1, 'C');
		

		$this->fpdf->Cell(0, 10, '', 0,1, 'C');

		$this->fpdf->SetFont('Arial','', 11);
		$this->fpdf->Cell(70, 5, 'To :',0);
		$this->fpdf->Cell(70, 5);
		$this->fpdf->Cell(50, 6);

		$this->fpdf->Cell(0, 6, '', 0,1, 'C');

		$this->fpdf->Cell(90, 6,$nama_pelanggan,1);
		$this->fpdf->Cell(3, 6,'',0);
		$this->fpdf->Cell(67, 6);
		$this->fpdf->Cell(3, 6,'',0);
		$this->fpdf->Cell(35, 6);

		$this->fpdf->Cell(0, 6, '', 0,1, 'C');

		$this->fpdf->Cell(90, 12,$alamat_pelanggan,1);
		$this->fpdf->Cell(3, 6,'',0);
		$this->fpdf->Cell(67, 12);
		$this->fpdf->Cell(3, 6,'',0);
		$this->fpdf->Cell(35, 6);
		
		$this->fpdf->Cell(0, 6, '', 0,1, 'C');

		$this->fpdf->Cell(67, 6,'',0);
		$this->fpdf->Cell(3, 6,'',0);
		$this->fpdf->Cell(131, 6,'',0);
		$this->fpdf->Cell(3, 6,'',0);
		$this->fpdf->Cell(72, 6,'DATE : '. date('j F Y - H:i'),1);

		$this->fpdf->Cell(0, 6, '', 0,1, 'C');

		$this->fpdf->Cell(90, 6,$no_tlp_pelanggan,1);
		$this->fpdf->Cell(3, 6,'',0);
		$this->fpdf->Cell(108, 6);
		$this->fpdf->Cell(3, 6,'',0);
		$this->fpdf->Cell(72, 6,'INVOICE NUMBER : '.$no_invoice,1);

		$this->fpdf->Cell(0, 15, '', 0,1, 'C');

		$this->fpdf->SetFont('Arial','B',10);

		$this->fpdf->Cell(7,6,'NO',1,0, 'C');
		$this->fpdf->Cell(65,6,'Deskripsi Barang',1,0, 'C');
		$this->fpdf->Cell(9,6,'Qty',1,0, 'C');
		$this->fpdf->Cell(9,6,'Sat',1,0, 'C');
		$this->fpdf->Cell(65,6,'Deskripsi Pekerjaan',1,0, 'C');
		$this->fpdf->Cell(35,6,'Harga',1,0, 'C');
		$this->fpdf->Cell(14,6,'Diskon',1,0, 'C');
		$this->fpdf->Cell(35,6,'Total',1,0, 'C');
		$this->fpdf->Cell(37,6,'Keterangan',1,1, 'C');
		 
		$this->fpdf->SetFont('Arial','',10);
		//Table with 2 rows and 4 columns
		$this->fpdf->SetWidths(array(7,65,9,9,65,35,14,35,37));
		$this->fpdf->SetAligns(array('C','','C','C','','C','C','C',''));
		$this->fpdf->SetFont('Arial','',10);
		
		$no = 1;
		$data_order = $this->invoice_model->getData_order($id)->result();
		foreach ($data_order as $row) {
			$total = "Rp. " . number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $row->total),0,',','.');
			$des_barang = $row->des_barang;
			$qty = $row->qty;
			$sat = $row->sat;
			$des_pekerjaan = $row->des_pekerjaan;
			$harga = "Rp. " . number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $row->harga),0,',','.');
			$disc = $row->diskon;
			$ket = $row->ket;

			$this->fpdf->Row(array($no++,$des_barang,$qty,$sat,$des_pekerjaan,$harga,$disc,$total,$ket));
		} 

			$this->fpdf->Image(base_url().'assets/img/wm.png', 0, 0, 300, 0);

		$this->fpdf->Cell(0, 7, ' ', 0,1, 'L');
		$this->fpdf->Cell(0, 7, 'Catatan :', 0,1, 'L');
		$this->fpdf->Cell(190, 12,$catatan,1);
		$this->fpdf->Cell(15, 6,'',0);
		$this->fpdf->Cell(35, 6,'Grand Total ',1);
		$this->fpdf->Cell(36, 6,$grand_total,1,'C');

		$this->fpdf->Cell(0, 6, '', 0,1, 'C');
		
		$this->fpdf->Cell(7,6,'',0);
		$this->fpdf->Cell(90, 12,'',0);
		$this->fpdf->Cell(108, 6,'',0);
		$this->fpdf->Cell(35, 6,'Drop Payment ',1);
		$this->fpdf->Cell(36, 6,$dp,1,'C');

		$this->fpdf->Cell(0, 6, '', 0,1, 'C');
		
		$this->fpdf->Cell(7,6,'',0);
		$this->fpdf->SetFont('Arial','B',10);
		$this->fpdf->Cell(90, 12,'PAYMENT : BCA A/N ALFRILO MUHAMMAD RISALDY, 1390460071',0);
		$this->fpdf->SetFont('Arial','',10);
		$this->fpdf->Cell(108, 6,'',0);
		$this->fpdf->Cell(35, 6,'Pelunasan ',1);
		$this->fpdf->Cell(36, 6,$pelunasan,1,'C');

		$this->fpdf->Cell(0, 6, '', 0,1, 'C');
		$this->fpdf->SetFont('Arial','B',10);

		$this->fpdf->Cell(5,6,'',0);
		$this->fpdf->Cell(10, 12,'SUPERVISOR',0);
		$this->fpdf->Cell(84, 6,'',0);
		$this->fpdf->Cell(10, 12,'ADMIN',0);
		$this->fpdf->Cell(96, 6,'',0);
		$this->fpdf->Cell(35, 6,'Kekurangan ',1);
		$this->fpdf->Cell(36, 6,$kekurangan,1,'C');

		$this->fpdf->Cell(0, 20, '', 0,1, 'C');
		$this->fpdf->SetFont('Arial','',10);

		$this->fpdf->Cell(1,6,'',0);
		$this->fpdf->Cell(10, 12,'_________________',0);
		$this->fpdf->Cell(77, 6,'',0);
		$this->fpdf->Cell(10, 12,'_________________',0);

		$this->fpdf->Cell(0, 5, '', 0,1, 'C');

		$this->fpdf->Cell(1,6,'',0);
		$this->fpdf->Cell(10, 12,'Tanggal : '.$so_date,0);
		$this->fpdf->Cell(77, 6,'',0);
		$this->fpdf->Cell(10, 12,'Tanggal : '.$so_date,0);


		$this->fpdf->Output('invoice-'.$so_number, 'I');
	}

}