<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_karyawan_model extends CI_Model{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function getData()
	{
		return $this->db->get('tabel_karyawan');
	}

	public function getData_id($id)
	{

		$this->db->where('id', $id);
		$query = $this->db->get('tabel_karyawan');

		return $query->result();
	}

	public function getData_cashbon()
	{	 
		return $this->db->get_where('saldo_cashbon');
	}

	public function getData_cashflow_cashbon()
	{	 
		return $this->db->get_where('cashflow_cash_bon');
	}

	public function tambah_karyawan()
	{
		$id = uniqid(rand(),true);
		$nama_karyawan = $this->input->post('nama_karyawan');
		$alamat = $this->input->post('alamat');
		$no_tlp = $this->input->post('no_tlp');
		$username = $this->input->post('username');
		$pass = $this->input->post('pass');

		$data = array(
			'id' => $id,
			'nama_karyawan' => $nama_karyawan, 
			'alamat' => $alamat, 
			'no_tlp' => $no_tlp, 
			'username' => $username, 
			'password' => $pass,
			'level' => 'sales'
		);

		$saldo_cashbon = array(
								'id' => $id,
								'nama_karyawan' => $nama_karyawan,
								'saldo_pinjaman' => '500000'
							);

		$this->db->insert('tabel_karyawan', $data);
		$this->db->insert('saldo_cashbon', $saldo_cashbon);
	}

	public function hapus_karyawan(){
		$id = $this->input->post('id');

    	$this->db->where('id', $id);
        $this->db->delete('tabel_karyawan');
    }

    public function update_data_karyawan(){
    	$id = $this->input->post('id');
		$nama_karyawan = $this->input->post('u_nama_karyawan');
		$alamat = $this->input->post('u_alamat');
		$no_tlp = $this->input->post('u_no_tlp');
		$username = $this->input->post('U_username');
		$pass = $this->input->post('confrim_pass_baru');

		$data = array(
			'nama_karyawan' => $nama_karyawan, 
			'alamat' => $alamat, 
			'no_tlp' => $no_tlp, 
			'username' => $username, 
			'password' => $pass
		);

		$where = array('id' => $id);
		$this->db->where($where);
		$this->db->update('tabel_karyawan', $data);
    }
	
}
?>