<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_model extends CI_Model{
	
	var $column_order = array('tanggal','item','pengeluaran','pemasukan','saldo','nama_karyawan','pinjaman','pengembalian','sisa_pinjaman','so_number','so_date','sales','nama_pelanggan','sub_total','drop_payment','kekurangan','pelunasan','saldo','catatan','po_number','client','nilai_kontrak','insentif',null); //set column field database for datatable orderable
	
	

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	private function _get_datatables_query()
	{
		$table = $this->input->post('jenis_laporan');

		if ($table == 'data_so') {
			$order = array('so_date' => 'asc'); // default order 
			$column_search = array('so_number','so_date','nama_pelanggan'); //set column field database for datatable searchable just firstname , lastname , address are searchable
		}elseif ($table == 'cashflow_cash_bon'){
			$order = array('tanggal' => 'asc'); // default order
			$column_search = array('tanggal'); //set column field database for datatable searchable just firstname , lastname , address are searchable 
		}elseif ($table == 'tabel_insentif'){
			$order = array('tanggal' => 'asc'); // default order
			$column_search = array('tanggal','po_number','client'); //set column field database for datatable searchable just firstname , lastname , address are searchable 
		}else{
			$order = array('tanggal' => 'asc'); // default order
			$column_search = array('tanggal','item'); //set column field database for datatable searchable just firstname , lastname , address are searchable 
		}

		$this->db->from($table);

		$i = 0;
	
		foreach ($column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function update_order()
	{
		$so_number = $this->input->post('so_number');
		$des_barang_old = $this->input->post('des_barang_old');
		$des_barang = $this->input->post('des_barang');
		$qty_barang = $this->input->post('qty_barang');
		$sat_barang = $this->input->post('sat_barang');
		$des_pekerjaan = $this->input->post('des_pekerjaan');
		$diskon_barang = $this->input->post('diskon_barang');
		$harga_barang = $this->input->post('harga_barang');
		$total_harga = $this->input->post('total_harga');
		$ket = $this->input->post('ket');
		$dp = $this->input->post('u_dp');
		$tipe_pembayaran = $this->input->post('tipe_pembayaran');

		$data = array(
			'des_barang' => $des_barang, 
			'qty' => $qty_barang, 
			'sat' => $sat_barang, 
			'des_pekerjaan' => $des_pekerjaan, 
			'harga' => $harga_barang, 
			'diskon' => $diskon_barang, 
			'total' => $total_harga, 
			'ket' => $ket

		);

		$where = array(
						'so_number' => $so_number
					 );

		$where2 = array(
						'so_number' => $so_number,
						'des_barang' => $des_barang_old
					 );

		$this->db->where($where2);
		$this->db->update('data_order', $data);

		$get_data_so = $this->db->get_where('data_so', $where)->result();
		$get_data_order = $this->db->get_where('data_order', $where)->result();

		foreach ($get_data_so as $row) {
		 	$total_lama = $row->sub_total;
		 	$nama_pelanggan = $row->nama_pelanggan;
		 	$kekurangan = $row->kekurangan;
		 	$dp_lama = $row->drop_payment;
		 	$saldo_lama = $row->saldo;
		 } 

		 $total_baru = 0;
		 

		foreach ($get_data_order as $row) {
		 	$total_baru += $row->total;
		 }

		 if ($total_baru > $total_lama) {
		 	$sisa_total = $total_baru - $total_lama;
		 	$dp_baru = preg_replace('/[^A-Za-z0-9\  ]/', '', $dp_lama) + preg_replace('/[^A-Za-z0-9\  ]/', '', $dp);
		 	$saldo_baru = preg_replace('/[^A-Za-z0-9\  ]/', '', $saldo_lama) + preg_replace('/[^A-Za-z0-9\  ]/', '', $dp);
		 	$kekurangan_baru = ($kekurangan + $sisa_total) - preg_replace('/[^A-Za-z0-9\  ]/', '', $dp);
		 	
		 	$data_so_baru = array(
							 	   'sub_total' => $total_baru,
							 	   'drop_payment' => $dp_baru,
							 	   'saldo' => $saldo_baru,
							 	   'kekurangan' => $kekurangan_baru
							 	);

		 	$this->db->where($where);
		 	$this->db->update('data_so', $data_so_baru);

		 }else{

		 	$sisa_total = $total_lama - $total_baru;
		 	$kekurangan_baru = $kekurangan - $sisa_total;

		 	$data_so_baru = array(
							 	   'sub_total' => $total_baru, 
							 	   'kekurangan' => $kekurangan_baru 
							 	);
		 	$this->db->where($where);
		 	$this->db->update('data_so', $data_so_baru);

		 }

		 if ($tipe_pembayaran == 'bank') {
			$saldo_bank = $this->db->get('cashflow_bank')->result();

			foreach ($saldo_bank as $bank) {
				$saldo_bank = $bank->saldo;
			}

			$item = 'Penamabahan Order atas nama ';

			$now_saldo = $saldo_bank + preg_replace('/[^A-Za-z0-9\  ]/', '', $dp);
			$isi_saldo = array(
								'tanggal' => date('d/m/y'),
								'item' => $item.$nama_pelanggan,
								'pengeluaran' => ' ',
								'pemasukan' => $dp,
								'saldo' => $now_saldo
							);

			$this->db->insert('cashflow_bank', $isi_saldo);

		}else{
			$saldo_cash = $this->db->get('cashflow_cash')->result();

			foreach ($saldo_cash as $cash) {
				$saldo_cash = $cash->saldo;
			}

			$item = 'Penamabahan Order atas nama ';

			$now_saldo = $saldo_cash + preg_replace('/[^A-Za-z0-9\  ]/', '', $dp);
			$isi_saldo = array(
								'tanggal' => date('d/m/y'),
								'item' => $item.$nama_pelanggan,
								'pengeluaran' => ' ',
								'pemasukan' => $dp,
								'saldo' => $now_saldo
							);

			$this->db->insert('cashflow_cash', $isi_saldo);
		}

		$get_pendapatan = $this->db->get('pendapatan_so')->result();

		foreach ($get_pendapatan as $row) {
			$jumlah_pendapatan = $row->jumlah;
		}

		$new_jumlah_pendapatan = $jumlah_pendapatan + preg_replace('/[^A-Za-z0-9\  ]/', '', $dp);
		$update_pendapatan = array('jumlah' => $new_jumlah_pendapatan);
		$this->db->update('pendapatan_so', $update_pendapatan);

	}

	public function count_all()
	{	
		$table = $this->input->post('jenis_laporan');
		$this->db->from($table);

		return $this->db->count_all_results();
	}

	public function get_by_id($id)
	{

		$this->db->where('so_number',$id);
		$query = $this->db->get('data_order');

		return $query->result();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->table);
	}

	public function get_data_cashflow($tabel)
	{
	return $this->db->get($tabel);
	}

	public function getData_so(){
   		
    	$data = array('so_number');
        return $this->db->get_where('data_so', $data);
    }

    public function getData_so_sebelum(){
   		
    	$data = array('so_number');
        return $this->db->get_where('data_so_sebelum', $data);
    }

	public function getData_aset(){
   		
        return $this->db->get_where('tabel_aset');
    }

    public function getData_bahan(){
   		
        return $this->db->get_where('tabel_bahan');
    }

     public function getData_jahit(){
   		
        return $this->db->get_where('tabel_jahit');
    }

     public function getData_pengiriman(){
   		
        return $this->db->get_where('tabel_pengiriman');
    }

     public function getData_beban(){
   		
        return $this->db->get_where('tabel_beban_bulanan');
    }

     public function getData_etc(){
   		
        return $this->db->get_where('tabel_etc');
    }

    public function getData_cashbon(){
   		
        return $this->db->get_where('cashflow_cash_bon');
    }

    public function getData_insentif(){
   		
        return $this->db->get_where('tabel_insentif');
    }
}
?>