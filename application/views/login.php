<!DOCTYPE html>
<html lang="en">
<head>
	<title><?php echo $tittle; ?></title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" type="image/png" href="<?php echo base_url('assets/img/icons/favicon.ico');?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets/vendor/bootstrap/css/bootstrap.min.css');?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url ('system/fonts/font-awesome-4.7.0/css/font-awesome.min.css');?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url ('system/fonts/Linearicons-Free-v1.0.0/icon-font.min.css');?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets/vendor/animate/animate.css');?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets/vendor/css-hamburgers/hamburgers.min.css');?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets/vendor/select2/select2.min.css');?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets/css/util.css');?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets/css/main.css');?>">
</head>
<body>
<?php echo form_open('auth/proses_login'); ?>	
	<div class="limiter">
		<div class="container-login100" style="background-image: url('<?php echo base_url ('assets/img/img-01.jpg');?>');">
			<div class="wrap-login100 p-t-190 p-b-30">
				<form class="login100-form validate-form">
					<div class="login100-form-avatar">
						<img src="<?php echo base_url ('assets/img/avatar-01.jpg');?>" alt="AVATAR">
					</div>

					<span class="login100-form-title p-t-20 p-b-45">
						Login Hela NYA
					</span>

					<div class="wrap-input100 validate-input m-b-10" data-validate = "Username is required">
						<input class="input100" type="text" name="name" placeholder="Username" required />
						<small class="username-error"></small><br>
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-user"></i>
						</span>
					</div>

					<div class="wrap-input100 validate-input m-b-10" data-validate = "Password is required">
						<input class="input100" type="password" name="pwd" placeholder="Password" required />
						<small class="password-error"></small><br>
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-lock"></i>
						</span>
					</div>

					<div class="container-login100-form-btn p-t-10">
						<button class="login100-form-btn" input type="submit" name="login" value="LOGIN">
							Login
						</button>
					</div>
						<div class="text-center w-full p-t-25 p-b-230">
						<a href="<?php echo site_url('ganti-pass');?>" class="txt1">
							Forgot Username / Password?
						</a>
					</div>
				</form>
			</div>
		</div>
	</div>
<?php echo form_close(); ?>

		<!-- Modal Beban Bulanan -->
        <div class="modal fade" id="<?php echo $error; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="alert alert-danger" role="alert">
			  Username atau Password anda salah!
			</div>
          </div>
        </div>

	<script src="<?php echo base_url ('assets/vendor/jquery/jquery-3.2.1.min.js');?>"></script>
	<script src="<?php echo base_url ('assets/vendor/bootstrap/js/popper.js');?>"></script>
	<script src="<?php echo base_url ('assets/vendor/bootstrap/js/bootstrap.min.js');?>"></script>
	<script src="<?php echo base_url ('assets/vendor/select2/select2.min.js');?>"></script>
	<script src="<?php echo base_url ('assets/js/main.js');?>"></script>
	<script type="text/javascript">
		$('#Modal-error').modal('show');
	</script>
</body>
</html>
