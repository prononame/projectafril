<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'auth';  /* nanti ganti ke "auth" */
$route['home'] = 'home';
$route['auth/proses-login'] = 'auth/proses_login';
$route['sales-order'] = 'sales_order';
$route['purchase-order'] = 'purchase_order';
$route['order-prioritas'] = 'order_prioritas';
$route['sales-order/proses-temp-order'] = 'sales_order/proses_temp_order';
$route['sales-order/proses-ambil-total'] = 'sales_order/proses_ambil_total';
$route['sales-order/deleteTemp'] = 'sales_order/deleteTemp';
$route['sales-order/data-temp-order'] = 'sales_order/data_temp_order';
$route['sales-order/proses-order'] = 'sales_order/proses_order';
$route['sales-order/cetak-so'] = 'sales_order/cetak_so';
$route['delivery-order'] = 'delivery_order';
$route['delivery-order/getdelivery'] = 'delivery_order/getDeliveryByid';
$route['delivery-order/detail-delivery'] = 'delivery_order/getDelivery';
$route['delivery-order/cetak-delivery'] = 'delivery_order/cetak_delivery';
$route['delivery-order/proses-pengeluaran'] = 'delivery_order/proses_insert';
$route['invoice/cetak-invoice'] = 'invoice/cetak_invoice';
$route['invoice'] = 'invoice';
$route['invoice/tabel-id'] = 'invoice/tabel_id';
$route['invoice/get-id'] = 'invoice/getid';
$route['invoice/order-list'] = 'invoice/order_list';
$route['pengeluaran'] = 'pengeluaran';
$route['pengeluaran/proses-pengeluaran'] = 'pengeluaran/proses_insert';
$route['pemasukan'] = 'pemasukan';
$route['pemasukan/proses-pemasukan'] = 'pemasukan/proses_insert';
$route['pemasukan/getData-so'] = 'pemasukan/getData';
$route['laporan'] = 'laporan';
$route['laporan-sales'] = 'laporan/v_sales';
$route['s-pemasukan'] = 's_pemasukan';
$route['s-pengeluaran'] = 's_pengeluaran';
$route['s-pengeluaran/peminjaman-cashbon'] = 's_pengeluaran/peminjaman_cashbon';
$route['s-pengeluaran/beban-bulanan'] = 's_pengeluaran/beban_bulanan';
$route['s-pengeluaran/insentif'] = 's_pengeluaran/insentif';
$route['data-karyawan'] = 'data_karyawan';
$route['data-karyawan/tambah-karyawan'] = 'data_karyawan/tambah_karyawan';
$route['data-karyawan/hapus-karyawan'] = 'data_karyawan/hapus_karyawan';
$route['homeadmin'] = 'admin';
$route['member'] = 'member';
$route['ganti-pass'] = 'ganti_pass';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
