<?php
class Delivery_order extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('delivery_order_model');

		if ($this->session->userdata('status') != "login") {
			redirect('auth');
		}
	}

	public function index()
	{
		$data['title']='Delivery Order | Noname.com';
		
		$this->load->view('templates/header', $data);
		$this->load->view('delivery_order');
		$this->load->view('templates/footer');
	}

	function list_data(){
		$data=$this->delivery_order_model->data_list();
		echo json_encode($data);
	}

	function update_status(){
		
		$data=$this->delivery_order_model->update_status();
		echo json_encode($data);
	}

	public function cetak_delivery()
	{	
		date_default_timezone_set('Asia/Jakarta');
		$date = date('d/m/y');
		$id = $this->input->post('id');
		$catatan = $this->input->post('cat');

		$data_po =  $this->delivery_order_model->getData_po($id)->result();
		foreach ($data_po as $po) {
			$so_number = $po->so_number;
			$po_number = $po->po_number;
			$po_date = $po->po_date;
			$ship_via = $po->ship_via;
			$nama_penerima = $po->nama_penerima;
			$alamat = $po->alamat;
			$no_tlp = $po->no_tlp; 
		}

		$data_so =  $this->delivery_order_model->getData_so($id)->result();
		foreach ($data_so as $so) {
			$so_date = $so->so_date;
		}
	
		$this->load->library('fpdf');

		//Lebar A4= 210mm
		//Margin default= 10mm tiap sisi
		//Bidang Horizontal yang dapat ditulisi= 210 - (10*2) = 190mm

		$this->fpdf->AddPage();

		//SetFont ke Arial, B, 10pt
		$this->fpdf->SetFont('Arial','B', 15);

		$this->fpdf->Image(base_url().'assets/img/logo.png', 5, 0, 30, 0);

		//Cell(lebar, tinggi, teks, border, end line, [align])
		$this->fpdf->Cell(0, 15, 'DOUBLE A - HOUSE OF SUBLIMATION',0,1, 'C');
		$this->fpdf->Cell(0, 1, '', 0,1, 'C');
		$this->fpdf->Cell(0, 5, 'DELIVERY ORDER',0,1, 'C');

		$this->fpdf->Cell(0, 10, '', 0,1, 'C');

		$this->fpdf->SetFont('Arial','', 10);
		$this->fpdf->Cell(70, 5, 'Dari :',0);
		$this->fpdf->Cell(70, 5, 'Untuk :',0);
		$this->fpdf->Cell(50, 6, 'SO DATE : '.$so_date,1);

		$this->fpdf->Cell(0, 6, '', 0,1, 'C');

		$this->fpdf->Cell(67, 6,'DOUBLE A - HOUSE OF SUBLIMATION',1);
		$this->fpdf->Cell(3, 6,'',0);
		$this->fpdf->Cell(67, 6,$nama_penerima,1);
		$this->fpdf->Cell(3, 6,'',0);
		$this->fpdf->Cell(50, 6,'SO NUMBER : '.$so_number,1);

		$this->fpdf->Cell(0, 6, '', 0,1, 'C');

		$this->fpdf->Cell(67, 12,'JL.Gunung Batu No. Sekian',1);
		$this->fpdf->Cell(3, 6,'',0);
		$this->fpdf->Cell(67, 12,$alamat,1);
		$this->fpdf->Cell(3, 6,'',0);
		$this->fpdf->Cell(50, 6,'SHIP VIA : '.$ship_via,1);

		$this->fpdf->Cell(0, 6, '', 0,1, 'C');

		$this->fpdf->Cell(67, 6,'',0);
		$this->fpdf->Cell(3, 6,'',0);
		$this->fpdf->Cell(67, 6,'',0);
		$this->fpdf->Cell(3, 6,'',0);
		$this->fpdf->Cell(50, 6,'PO DATE : '.$po_date,1);

		$this->fpdf->Cell(0, 6, '', 0,1, 'C');

		$this->fpdf->Cell(67, 6,'No Telp : 098712717272',1);
		$this->fpdf->Cell(3, 6,'',0);
		$this->fpdf->Cell(67, 6,'No Telp : '.$no_tlp,1);
		$this->fpdf->Cell(3, 6,'',0);
		$this->fpdf->Cell(50, 6,'PO NUMBER : '.$po_number,1);

		$this->fpdf->Cell(0, 15, '', 0,1, 'C');

		$this->fpdf->SetFont('Arial','B',10);

		$this->fpdf->Cell(7,6,'NO',1,0,'C');
		$this->fpdf->Cell(80,6,'Deskripsi Barang',1,0,'C');
		$this->fpdf->Cell(11,6,'Qty',1,0,'C');
		$this->fpdf->Cell(11,6,'Sat',1,0,'C');
		$this->fpdf->Cell(80,6,'Ket',1,1,'C');
		 
		$this->fpdf->SetFont('Arial','',10);
		
		$no = 1;
		$j = 0;
		$data_order = $this->delivery_order_model->getData_order($id)->result();
		foreach ($data_order as $row) {

			$t = $row->qty;
			$j = $j + $t;

			$this->fpdf->Cell(7,6,$no++,1,0, 'C');
			$this->fpdf->Cell(80,6,$row->des_barang,1,0);
			$this->fpdf->Cell(11,6,$row->qty,1,0, 'C');
			$this->fpdf->Cell(11,6,$row->sat,1,0, 'C');
			$this->fpdf->Cell(80,6,$row->ket,1,1);
		}
			$this->fpdf->SetFont('Arial','B',10);

			$this->fpdf->Cell(7,6,'',1,0, 'C');
			$this->fpdf->Cell(80,6,'Total',1,0, 'C');
			$this->fpdf->Cell(11,6,$j,1,0, 'C');
			$this->fpdf->Cell(11,6,'pcs',1,0, 'C');
			$this->fpdf->Cell(80,6,'',1,1);
			
			$this->fpdf->Image(base_url().'assets/img/wm.png', 0, 20, 222, 0);

		$this->fpdf->Cell(0, 7, ' ', 0,1, 'L');
		$this->fpdf->Cell(0, 7, 'Catatan :', 0,1, 'L');
		$this->fpdf->Cell(190, 12,$catatan,1);
		$this->fpdf->Cell(30, 6,'',0);
		$this->fpdf->Cell(0, 7, ' ', 0,1, 'L');
		

		$this->fpdf->Cell(0, 6, '', 0,1, 'C');
		$this->fpdf->SetFont('Arial','B',10);

		$this->fpdf->Cell(11,6,'',0);
		$this->fpdf->Cell(10, 12,'KURIR',0);
		$this->fpdf->Cell(35, 6,'',0);
		$this->fpdf->Cell(10, 12,'SUPERVISOR',0);
		$this->fpdf->Cell(45, 6,'',0);
		$this->fpdf->Cell(10, 12,'ADMIN',0);

		$this->fpdf->Cell(23, 6,'',0);

		$this->fpdf->Cell(0, 20, '', 0,1, 'C');
		$this->fpdf->SetFont('Arial','',10);

		$this->fpdf->Cell(1,6,'',0);
		$this->fpdf->Cell(10, 12,'_________________',0);
		$this->fpdf->Cell(40, 6,'',0);
		$this->fpdf->Cell(10, 12,'_________________',0);
		$this->fpdf->Cell(40, 6,'',0);
		$this->fpdf->Cell(12, 12,'_________________',0);

		$this->fpdf->Cell(0, 5, '', 0,1, 'C');

		$this->fpdf->Cell(1,6,'',0);
		$this->fpdf->Cell(12, 12,'Tanggal : '.$date,0);
		$this->fpdf->Cell(40, 6,'',0);
		$this->fpdf->Cell(10, 12,'Tanggal : '.$date,0);
		$this->fpdf->Cell(40, 6,'',0);
		$this->fpdf->Cell(10, 12,'Tanggal : '.$date,0);
		$this->fpdf->Cell(40, 6,'',0);
		$this->fpdf->Cell(10, 12,'Jam :'.date('H:i'),0);


		$this->fpdf->Output($so_number.'_DO.pdf', 'I');
	}

}