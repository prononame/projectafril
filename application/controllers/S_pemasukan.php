<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class S_pemasukan extends CI_Controller {

	public function __construct(){
		parent::__construct();

		$this->load->model('s_pemasukan_model');
		$this->load->library('form_validation');

		if ($this->session->userdata('status') != "login") {
			redirect('auth');
		}
	}

	public function index()
	{
		$data['title']='Pemasukan | Noname.com';

		$this->load->view('templates/header', $data);
		$this->load->view('s_pemasukan');
		$this->load->view('templates/footer');
	}

	public function saldo_awal()
	{
		$this->s_pemasukan_model->insert_saldo_awal();
		redirect('s_pemasukan');
	}

	public function safty_oprasional()
	{
		$this->s_pemasukan_model->insert_safty_oprasional();
		redirect('s_pemasukan');
	}

}
