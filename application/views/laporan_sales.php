<div class="wrapper ">
    <div class="sidebar" data-color="purple" data-background-color="white" data-image="<?php echo base_url('assets/img/sidebar-1.jpg'); ?>">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
      <div class="logo">
        <a href="#" class="simple-text logo-normal">
          <h6>DOUBLE A - HOUSE OF SUBLIMATION</h6>
        </a>
      </div>
      <div class="sidebar-wrapper">
        <ul class="nav">
          <li class="nav-item">
            <a class="nav-link" href="<?php echo site_url('sales-order');?>">
              <i class="fa fa-shopping-cart"></i>
              <p>Sales Order</p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo site_url('purchase-order');?>">
              <i class="fa fa-shopping-cart"></i>
              <p>Purchase Order</p>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="<?php echo site_url('invoice');?>">
              <i class="material-icons">content_paste</i>
              <p>Invoice</p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url('delivery-order');?>">
              <i class="material-icons">content_paste</i>
              <p>Delivery Order</p>
            </a>
          </li>
          <li class="nav-item active">
            <a class="nav-link" href="<?php echo site_url('laporan-sales');?>">
              <i class="material-icons">content_paste</i>
              <p>Laporan</p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo site_url('pemasukan');?>">
              <i class="material-icons">location_ons</i>
              <p>Pemasukan</p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo site_url('pengeluaran');?>">
              <i class="material-icons">bubble_chart</i>
              <p>Pengeluaran</p>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <a class="navbar-brand" href="<?php echo site_url('laporan-sales'); ?>">Laporan</a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">
            <form class="navbar-form">
              <div class="input-group no-border">
               
                  <div class="ripple-container"></div>
                </button>
              </div>
            </form>
            <ul class="navbar-nav">
              <li class="nav-item dropdown">
                <a class="nav-link" href="#pablo" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">account_circle</i>
                  <p class="d-lg-none d-md-block">
                    Account
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <a class="dropdown-item" href="#"><?php echo $this->session->userdata('nama') ?></a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php echo site_url('/logout') ?>">Log out</a>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->

      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-lg-12 col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">LAPORAN CASHFLOW</h4>
                  <p class="card-category"></p>
                </div>
                <div class="card-body">
                  <div class="row">
                    <div class="form-group col-md-2">
                      <label for="jenis_laporan_cashflow">Jenis Laporan</label>
                      <select class="form-control" name="jenis_laporan_cashflow" id="jenis_laporan_cashflow" required>
                        <option value="nol">Choose..</option>
                        <option value="cashflow_bank">Bank</option>
                        <option value="cashflow_cash">Cash</option>
                      </select>
                    </div>
                  </div>
                  <br>
                  <div class="row">
                    <div class="table-responsive">
                      <div id="table-cashflow"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-12 col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">LAPORAN ORDER</h4>
                  <p class="card-category"></p>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                      <table id="sales_order" name="table-sales-order" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead class="text-center">
                          <tr>
                            <th scope="col" style="color: purple; font-weight: bold;"><b>SO Number</b></th>
                            <th scope="col" style="color: purple; font-weight: bold;"><b>SO Date</b></th>
                            <th scope="col" style="color: purple; font-weight: bold;"><b>Sales</b></th>
                            <th scope="col" style="color: purple; font-weight: bold;"><b>Client</b></th>
                            <th scope="col" style="color: purple; font-weight: bold;"><b>Nilai Kontrak</b></th>
                            <th scope="col" style="color: purple; font-weight: bold;"><b>Drop Payment</b></th>
                            <th scope="col" style="color: purple; font-weight: bold;"><b>Pelunasan</b></th>
                            <th scope="col" style="color: purple; font-weight: bold;"><b>Kekurangan</b></th>
                            <th scope="col" style="color: purple; font-weight: bold;"><b>Saldo</b></th>
                            <th scope="col" style="color: purple; font-weight: bold;"><b>Catatan</b></th>
                          </tr>
                        </thead>
                        <tbody>
                      
                        </tbody>
                      </table>
                    </div>
                </div>
              </div>
            </div>
            <div class="col-lg-12 col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">LAPORAN ORDER BULAN SEBELUMNYA</h4>
                  <p class="card-category"></p>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                      <table id="sales_order_sebelum" name="table-sales-order-sebelum" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead class="text-center">
                          <tr>
                            <th scope="col" style="color: purple; font-weight: bold;"><b>SO Number</b></th>
                            <th scope="col" style="color: purple; font-weight: bold;"><b>SO Date</b></th>
                            <th scope="col" style="color: purple; font-weight: bold;"><b>Sales</b></th>
                            <th scope="col" style="color: purple; font-weight: bold;"><b>Client</b></th>
                            <th scope="col" style="color: purple; font-weight: bold;"><b>Nilai Kontrak</b></th>
                            <th scope="col" style="color: purple; font-weight: bold;"><b>Drop Payment</b></th>
                            <th scope="col" style="color: purple; font-weight: bold;"><b>Pelunasan</b></th>
                            <th scope="col" style="color: purple; font-weight: bold;"><b>Kekurangan</b></th>
                            <th scope="col" style="color: purple; font-weight: bold;"><b>Saldo</b></th>
                            <th scope="col" style="color: purple; font-weight: bold;"><b>Catatan</b></th>
                          </tr>
                        </thead>
                        <tbody>
                      
                        </tbody>
                      </table>
                    </div>
                </div>
              </div>
            </div>
            <div class="col-lg-12 col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">LAPORAN KEUANGAN</h4>
                  <p class="card-category"></p>
                </div>
                <div class="card-body">
                  <div class="row">
                    <div class="form-group col-md-2">
                      <label for="jenis_laporan_pengeluaran">Jenis Laporan</label>
                      <select class="form-control" name="jenis_laporan_pengeluaran" id="jenis_laporan_pengeluaran" required>
                        <option value="nol">Choose..</option>
                        <option value="tabel_aset">Aset</option>
                        <option value="tabel_bahan">Bahan</option>
                        <option value="tabel_jahit">Jahit</option>
                        <option value="tabel_pengiriman">Pengiriman</option>
                        <option value="tabel_gojek">Gojek</option>
                        <option value="tabel_beban_bulanan">Beban</option>
                        <option value="tabel_etc">Tidak Terduga</option>
                      </select>
                    </div>
                  </div>
                  <br>
                  <div class="row">
                    <div class="table-responsive">
                      <div id="table-pengeluaran"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-12 col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">LAPORAN CASH BON</h4>
                  <p class="card-category"></p>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                      <table id="cashbon" name="table-cashbon" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead class="text-center">
                          <tr>
                            <th scope="col" style="color: purple; font-weight: bold;"><b>Tanggal</b></th>
                            <th scope="col" style="color: purple; font-weight: bold;"><b>Nama</b></th>
                            <th scope="col" style="color: purple; font-weight: bold;"><b>Pinjaman</b></th>
                            <th scope="col" style="color: purple; font-weight: bold;"><b>Pengembalian</b></th>
                            <th scope="col" style="color: purple; font-weight: bold;"><b>Sisa Pinjaman</b></th>
                          </tr>
                        </thead>
                        <tbody>
                      
                        </tbody>
                      </table>
                    </div>
                </div>
              </div>
            </div>
            <div class="col-lg-12 col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">LAPORAN INSENTIF</h4>
                  <p class="card-category"></p>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                      <table id="t_insentif" name="table-insentif" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead class="text-center">
                          <tr>
                            <th scope="col" style="color: purple; font-weight: bold;"><b>Tanggal</b></th>
                            <th scope="col" style="color: purple; font-weight: bold;"><b>Nomor PO</b></th>
                            <th scope="col" style="color: purple; font-weight: bold;"><b>Sales/Marketing</b></th>
                            <th scope="col" style="color: purple; font-weight: bold;"><b>Client</b></th>
                            <th scope="col" style="color: purple; font-weight: bold;"><b>Nilai Kontrak</b></th>
                            <th scope="col" style="color: purple; font-weight: bold;"><b>Insentif</b></th>
                          </tr>
                        </thead>
                        <tbody>
                      
                        </tbody>
                      </table>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>


<script type="text/javascript" src="<?php echo base_url().'assets/js/jquery-3.2.1.js'?>"></script>
<script src="<?php echo base_url('assets/datatables/js/jquery.dataTables.min.js')?>"></script>
<script src="<?php echo base_url('assets/datatables/js/dataTables.bootstrap.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/js/dataTables.bootstrap4.js'?>"></script>

<script type="text/javascript">

var save_method; //for save method string
var table;
var table_so;
var table_so_sebelum;
var table_insentif;

$(document).ready(function() {

  $("#jenis_laporan_cashflow").on('change',function(){
    var jenis_laporan_cashflow = $(this).val();
    $('#table-cashflow').html(
                          '<table id="cashflow" name="table-cashflow" class="table table-striped table-bordered" cellspacing="0" width="100%">' +
                            '<thead class="text-center">' +
                              '<tr>' +
                                '<th scope="col" style="color: purple; font-weight: bold;"><b>Tanggal</b></th>' +
                                '<th scope="col" style="color: purple; font-weight: bold;"><b>Item</b></th>' +
                                '<th scope="col" style="color: purple; font-weight: bold;"><b>Pengeluaran</b></th>' +
                                '<th scope="col" style="color: purple; font-weight: bold;"><b>Pemasukan</b></th>' +
                                '<th scope="col" style="color: purple; font-weight: bold;"><b>Saldo</b></th>' +
                              '</tr>' +
                            '</thead>' +
                              '<tbody>' +
                      
                              '</tbody>' +
                          '</table>');
    $('[name=table-cashflow]').attr('id', jenis_laporan_cashflow);
    var id = $('[name=table-cashflow]').attr("id");
    var n_id = '#' + id;
      //datatables
      table = $(n_id).DataTable({ 

          "ordering": false,
          "processing": true, //Feature control the processing indicator.
          "serverSide": true, //Feature control DataTables' server-side processing mode.
          "order": [], //Initial no order.

          // Load data for the table's content from an Ajax source
          "ajax": {
              "url": "<?php echo site_url('laporan/show_data_cashflow')?>",
              "type": "POST",
              "data": {jenis_laporan:jenis_laporan_cashflow}
          },

          //Set column definition initialisation properties.
          "columnDefs": [
          { 
              "targets": [ -1 ], //last column
              "orderable": false, //set not orderable
          },
          ],

      });

    });

  $("#jenis_laporan_pengeluaran").on('change',function(){
    var jenis_laporan_pengeluaran = $(this).val();
    $('#table-pengeluaran').html(
                          '<table id="pengeluaran" name="table-pengeluaran" class="table table-striped table-bordered" cellspacing="0" width="100%">' +
                            '<thead class="text-center">' +
                              '<tr>' +
                                '<th scope="col" style="color: purple; font-weight: bold;"><b>Tanggal</b></th>' +
                                '<th scope="col" style="color: purple; font-weight: bold;"><b>Item</b></th>' +
                                '<th scope="col" style="color: purple; font-weight: bold;"><b>Pengeluaran</b></th>' +
                                '<th scope="col" style="color: purple; font-weight: bold;"><b>Keterangan</b></th>' +
                              '</tr>' +
                            '</thead>' +
                              '<tbody>' +
                      
                              '</tbody>' +
                          '</table>');
    $('[name=table-pengeluaran]').attr('id', jenis_laporan_pengeluaran);
    var id = $('[name=table-pengeluaran]').attr("id");
    var n_id = '#' + id;
      //datatables
      table = $(n_id).DataTable({

          "ordering": false,
          "processing": true, //Feature control the processing indicator.
          "serverSide": true, //Feature control DataTables' server-side processing mode.
          "order": [], //Initial no order.

          // Load data for the table's content from an Ajax source
          "ajax": {
              "url": "<?php echo site_url('laporan/show_data_pengeluaran')?>",
              "type": "POST",
              "data": {jenis_laporan:jenis_laporan_pengeluaran}
          },

          //Set column definition initialisation properties.
          "columnDefs": [
          { 
              "targets": [ -1 ], //last column
              "orderable": false, //set not orderable
          },
          ],

      });

    });

    table = $('#cashbon').DataTable({

            "ordering": false,
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.

            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo site_url('laporan/show_data_cashbon')?>",
                "type": "POST",
                "data": {jenis_laporan:'cashflow_cash_bon'}
            },

            //Set column definition initialisation properties.
            "columnDefs": [
            { 
                "targets": [ -1 ], //last column
                "orderable": false, //set not orderable
            },
            ],

        });

    table_so = $('#sales_order').DataTable({

            "ordering": false,
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.

            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo site_url('laporan/show_data_so')?>",
                "type": "POST",
                "data": {jenis_laporan:'data_so'}
            },

            //Set column definition initialisation properties.
            "columnDefs": [
            { 
                "targets": [ -1 ], //last column
                "orderable": false, //set not orderable
            },
            ],

        });

    table_so_sebelum = $('#sales_order_sebelum').DataTable({

            "ordering": false,
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.

            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo site_url('laporan/show_data_so')?>",
                "type": "POST",
                "data": {jenis_laporan:'data_so_sebelum'}
            },

            //Set column definition initialisation properties.
            "columnDefs": [
            { 
                "targets": [ -1 ], //last column
                "orderable": false, //set not orderable
            },
            ],

        });

    table_insentif = $('#t_insentif').DataTable({

            "ordering": false,
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.

            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo site_url('laporan/show_data_insentif')?>",
                "type": "POST",
                "data": {jenis_laporan:'tabel_insentif'}
            },

            //Set column definition initialisation properties.
            "columnDefs": [
            { 
                "targets": [ -1 ], //last column
                "orderable": false, //set not orderable
            },
            ],

        });


    //datepicker
    $('.datepicker').datepicker({
        autoclose: true,
        format: "yyyy-mm-dd",
        todayHighlight: true,
        orientation: "top auto",
        todayBtn: true,
        todayHighlight: true,  
    });

    //set input/textarea/select event when change value, remove class error and remove text help block 
    $("input").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("textarea").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("select").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });

});



function add_person()
{
    save_method = 'add';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_form').modal('show'); // show bootstrap modal
    $('.modal-title').text('Add Person'); // Set Title to Bootstrap modal title
}

function edit_person(id)
{
    save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string

    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo site_url('person/ajax_edit/')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {

            $('[name="id"]').val(data.id);
            $('[name="firstName"]').val(data.firstName);
            $('[name="lastName"]').val(data.lastName);
            $('[name="gender"]').val(data.gender);
            $('[name="address"]').val(data.address);
            $('[name="dob"]').datepicker('update',data.dob);
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Person'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}

function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax 
}

function save()
{
    $('#btnSave').text('saving...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    var url;

    if(save_method == 'add') {
        url = "<?php echo site_url('person/ajax_add')?>";
    } else {
        url = "<?php echo site_url('person/ajax_update')?>";
    }

    // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {

            if(data.status) //if success close modal and reload ajax table
            {
                $('#modal_form').modal('hide');
                reload_table();
            }
            else
            {
                for (var i = 0; i < data.inputerror.length; i++) 
                {
                    $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                    $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
                }
            }
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        }
    });
}

function delete_person(id)
{
    if(confirm('Are you sure delete this data?'))
    {
        // ajax delete data to database
        $.ajax({
            url : "<?php echo site_url('person/ajax_delete')?>/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                //if success reload ajax table
                $('#modal_form').modal('hide');
                reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });

    }
}

</script>