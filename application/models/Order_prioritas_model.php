<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order_prioritas_model extends CI_Model{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function getJumlah_order(){
		$tanggal = date('d/m/y');
		$data = array('tanggal_order' => $tanggal );
		$query =  $this->db->get_where('jumlah_order', $data)->result();

		if ($query == false) {
			$jumlah = '1';
			$data2 = array('tanggal_order' =>$tanggal,
						  'jumlah' => $jumlah,
						  'prioritas' => $jumlah );

			$this->db->insert('jumlah_order', $data2);
			
			return $this->db->get_where('jumlah_order', $data);
		}else{

			return $this->db->get_where('jumlah_order', $data);
		}
    }

	public function getData(){
        return $this->db->get('temp_sales_order_prioritas');
    }

    public function getData_by_id($id){
    	$this->db->where('id', $id);
        return $this->db->get('temp_sales_order_prioritas');
    }

    public function deleteTemp($id){
    	$this->db->where('id', $id);
        $this->db->delete('temp_sales_order_prioritas');
    }

    public function getData_order(){
    	$temp_date = date('d/m/y');
    	$data_tgl = array('tanggal_order' => $temp_date);
    	$query =  $this->db->get_where('jumlah_order', $data_tgl)->result();
    	foreach ($query as $row) {
    		$jmlh = $row->prioritas - 1;
    	}

    	$so_number = 'P'.date('dmy').'-'.$jmlh;
    	$data = array('so_number' => $so_number);
        return $this->db->get_where('data_order', $data);
    }

    public function getData_so(){
    	$temp_date = date('d/m/y');
    	$data_tgl = array('tanggal_order' => $temp_date);
    	$query =  $this->db->get_where('jumlah_order', $data_tgl)->result();
    	foreach ($query as $row) {
    		$jmlh = $row->prioritas - 1;
    	}

    	$so_number = 'P'.date('dmy').'-'.$jmlh;
    	$data = array('so_number' => $so_number);
        return $this->db->get_where('data_so', $data);
    }

    public function getData_po(){
    	$temp_date = date('d/m/y');
    	$data_tgl = array('tanggal_order' => $temp_date);
    	$query =  $this->db->get_where('jumlah_order', $data_tgl)->result();
    	foreach ($query as $row) {
    		$jmlh = $row->prioritas - 1;
    	}

    	$so_number = 'P'.date('dmy').'-'.$jmlh;
    	$data = array('so_number' => $so_number);
        return $this->db->get_where('data_po', $data);
    }
	
	public function save_temp_order()
	{
		$id = uniqid(rand(),true);
		$des_barang = $this->input->post('des_barang');
		$qty_barang = $this->input->post('qty_barang');
		$sat_barang = $this->input->post('sat_barang');
		$des_pekerjaan = $this->input->post('des_pekerjaan');
		$diskon_barang = $this->input->post('diskon_barang');
		$harga_barang = $this->input->post('harga_barang');
		$total_harga = $this->input->post('total_harga');
		$ket = $this->input->post('ket');

		$data = array(
			'id' => $id,
			'des_barang' => $des_barang, 
			'qty' => $qty_barang, 
			'sat' => $sat_barang, 
			'des_pekerjaan' => $des_pekerjaan, 
			'harga' => $harga_barang, 
			'diskon' => $diskon_barang, 
			'total' => $total_harga, 
			'ket' => $ket

		);

		$this->db->insert('temp_sales_order_prioritas', $data);
	}

	public function edit_temp_order($id)
	{
		$des_barang = $this->input->post('des_barang');
		$qty_barang = $this->input->post('qty_barang');
		$sat_barang = $this->input->post('sat_barang');
		$des_pekerjaan = $this->input->post('des_pekerjaan');
		$diskon_barang = $this->input->post('diskon_barang');
		$harga_barang = $this->input->post('harga_barang');
		$total_harga = $this->input->post('total_harga');
		$ket = $this->input->post('ket');

		$data = array(
			'des_barang' => $des_barang, 
			'qty' => $qty_barang, 
			'sat' => $sat_barang, 
			'des_pekerjaan' => $des_pekerjaan, 
			'harga' => $harga_barang, 
			'diskon' => $diskon_barang, 
			'total' => $total_harga, 
			'ket' => $ket

		);
		$this->db->where('id', $id);
		$this->db->update('temp_sales_order_prioritas', $data);
	}

	public function insert_data_order()
	{
		$so_number = $this->input->post('so_number');
		$so_date = $this->input->post('so_date');
		$sales = $this->session->userdata('nama');
		$nama_pelanggan = $this->input->post('nama_pelanggan');
		$alamat_pelanggan = $this->input->post('alamat_pelanggan');
		$telfon_pelanggan = $this->input->post('telfon_pelanggan');
		$sub_total = $this->input->post('sub_total');
		$drop_payment = $this->input->post('dp');
		$tipe_pembayaran = $this->input->post('tipe_pembayaran');
		$kekurangan = $this->input->post('kekurangan');
		$catatan = $this->input->post('catatan');

		$cek_pelanggan = array('nama_pelanggan' => $nama_pelanggan);
		$data_pelanggan = $this->db->get_where('tabel_pelanggan', $cek_pelanggan)->result();

		if (count($data_pelanggan) == 0) {
			$new_pelanggan = array(
									'id' => uniqid(rand(), true),
									'nama_pelanggan' => $nama_pelanggan,
									'alamat' => $alamat_pelanggan,
									'no_tlp' => $telfon_pelanggan 
									);
			$this->db->insert('tabel_pelanggan', $new_pelanggan);
		}

		$get_pendapatan = $this->db->get('pendapatan_so')->result();

		foreach ($get_pendapatan as $row) {
			$jumlah_pendapatan = $row->jumlah;
		}

		$new_jumlah_pendapatan = $jumlah_pendapatan + preg_replace('/[^A-Za-z0-9\  ]/', '', $drop_payment);
		$update_pendapatan = array('jumlah' => $new_jumlah_pendapatan);
		$this->db->update('pendapatan_so', $update_pendapatan);

		if ($kekurangan <= 0) {
			$ket = 'Lunas';
			$pelunasan = $drop_payment;
			$item = 'Pelunasan Order atas nama ';
			$drop_payment = '0';
			$saldo = $pelunasan;
		}else{
			$ket = 'Belum Lunas';
			$pelunasan = '0';
			$item = 'Pembayaran DP Order atas nama ';
			$saldo = $drop_payment;
		}

		if ($tipe_pembayaran == 'bank') {
			$saldo_bank = $this->db->get('cashflow_bank')->result();

			foreach ($saldo_bank as $bank) {
				$saldo_bank = $bank->saldo;
			}

			$now_saldo = $saldo_bank + preg_replace('/[^A-Za-z0-9\  ]/', '', $drop_payment);
			$isi_saldo = array(
								'tanggal' => date('d/m/y'),
								'item' => $item.$nama_pelanggan,
								'pengeluaran' => ' ',
								'pemasukan' => $drop_payment,
								'saldo' => $now_saldo
							);

			$this->db->insert('cashflow_bank', $isi_saldo);

		}else{
			$saldo_cash = $this->db->get('cashflow_cash')->result();

			foreach ($saldo_cash as $cash) {
				$saldo_cash = $cash->saldo;
			}

			$now_saldo = $saldo_cash + preg_replace('/[^A-Za-z0-9\  ]/', '', $drop_payment);
			$isi_saldo = array(
								'tanggal' => date('d/m/y'),
								'item' => $item.$nama_pelanggan,
								'pengeluaran' => ' ',
								'pemasukan' => $drop_payment,
								'saldo' => $now_saldo
							);

			$this->db->insert('cashflow_cash', $isi_saldo);
		}

		$po_number = $this->input->post('po_number');
		$po_date = $this->input->post('po_date');
		$ship_via = $this->input->post('ship_via');
		$nama_penerima = $this->input->post('nama_penerima');
		$alamat_pengiriman = $this->input->post('alamat_pengiriman');
		$telfon_pengiriman = $this->input->post('telfon_pengiriman');


		$data_so = array(
			'so_number' => $so_number,
			'so_date' => $so_date,
			'sales' => $sales,
			'nama_pelanggan' => $nama_pelanggan, 
			'alamat' => $alamat_pelanggan, 
			'no_tlp' => $telfon_pelanggan, 
			'sub_total' => $sub_total, 
			'drop_payment' => $drop_payment, 
			'kekurangan' => $kekurangan,
			'pelunasan' => $pelunasan,
			'saldo' => $saldo,
			'catatan' => $catatan,
			'ket' => $ket

		);

		$data_po = array(
			'so_number' => $so_number,
			'po_number' => $po_number,
			'po_date' => $po_date,
			'sales' => $sales, 
			'ship_via' => $ship_via,
			'nama_penerima' => $nama_penerima, 
			'alamat' => $alamat_pengiriman, 
			'no_tlp' => $telfon_pengiriman,
			'status' => 'Belum Dikirim'
			

		);

		$query1 =  $this->db->get('temp_sales_order_prioritas')->result();

		foreach ($query1 as $row) {
			
			$data_order = array(
			'so_number' => $so_number,
			'so_date' => $so_date, 
			'nama_pelanggan' => $nama_pelanggan,
			'des_barang' => $row->des_barang, 
			'qty' => $row->qty, 
			'sat' => $row->sat, 
			'des_pekerjaan' => $row->des_pekerjaan, 
			'harga' => $row->harga, 
			'diskon' => $row->diskon, 
			'total' => $row->total, 
			'ket' => $row->ket

			);

			$this->db->insert('data_order', $data_order);
		}
		
		$this->db->insert('data_so', $data_so);
		$this->db->insert('data_po', $data_po);

		$tanggal = date('d/m/y');
		$dataTgl = array('tanggal_order' => $tanggal );
		$query2 =  $this->db->get_where('jumlah_order', $dataTgl)->result();

		foreach ($query2 as $row2) {
			
			$temp_jmlh = $row2->prioritas;
			
		}
		$new_jmlh = $temp_jmlh + 1;

			$data_tanggal = array(
				'prioritas' => $new_jmlh
			);
		$this->db->where($dataTgl);
		$this->db->update('jumlah_order', $data_tanggal);

	}

	public function delete_temp()
	{
		$query =  $this->db->get('temp_sales_order_prioritas')->result();

		foreach ($query as $row) {
			
			$data = array(
			
			'id' => $row->id

			);

			$this->db->where($data);
			$this->db->delete('temp_sales_order_prioritas');
		}
		
	}
}
?>