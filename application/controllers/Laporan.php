<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller {

	public function __construct(){
		parent::__construct();

		$this->load->model('laporan_model');
		$this->load->model('data_karyawan_model');
		$this->load->library('form_validation');

		if ($this->session->userdata('status') != "login") {
			redirect('auth');
		}
	}

	public function index()
	{
		$data['title']='Laporan | Noname.com';

		$this->load->view('templates/header', $data);
		$this->load->view('laporan');
		$this->load->view('templates/footer');
	}

	public function v_sales()
	{

		$data['title']='Laporan | Noname.com';

		$this->load->view('templates/header', $data);
		$this->load->view('laporan_sales');
		$this->load->view('templates/footer');
	}

	function show_data_cashflow()
	{
		$list = $this->laporan_model->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $person) {
			$no++;
			$row = array();
			$row[] = $person->tanggal;
			$row[] = $person->item;

			if ($person->pengeluaran == ' ') {
				$row[] = $person->pengeluaran;
			}else{
				$row[] = "Rp. ".number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $person->pengeluaran),2,',','.');
			}

			if ($person->pemasukan == ' ') {
				$row[] = $person->pemasukan;
			}else{
				$row[] = "Rp. ".number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $person->pemasukan),2,',','.');
			}

			$row[] = "Rp. " .number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $person->saldo),2,',','.');
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->laporan_model->count_all(),
						"recordsFiltered" => $this->laporan_model->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	function show_data_pengeluaran()
	{
		$list = $this->laporan_model->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $person) {
			$no++;
			$row = array();
			$row[] = $person->tanggal;
			$row[] = $person->item;
			$row[] = "Rp. ".number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $person->pengeluaran),2,',','.');
			$row[] = $person->ket;
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->laporan_model->count_all(),
						"recordsFiltered" => $this->laporan_model->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	function show_data_cashbon()
	{

		$list = $this->laporan_model->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $person) {
			$no++;
			$row = array();
			$row[] = $person->tanggal;
			$row[] = $person->nama_karyawan;

			if ($person->pinjaman == ' ') {
				$row[] = $person->pinjaman;
			}else{
				$row[] = "Rp. " .number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $person->pinjaman),2,',','.');
			}

			if ($person->pengembalian == ' ') {
				$row[] = $person->pengembalian;
			}else{
				$row[] = "Rp. " . number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $person->pengembalian),2,',','.');
			}

			$row[] = "Rp. " . number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $person->sisa_pinjaman),2,',','.');
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->laporan_model->count_all(),
						"recordsFiltered" => $this->laporan_model->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	function show_data_so()
	{

		$list = $this->laporan_model->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $person) {
			$no++;
			$row = array();
			$row[] = $person->so_number;
			$row[] = $person->so_date;
			$row[] = $person->nama_pelanggan;
			$row[] = $person->alamat;
		    $row[] = "Rp. " .number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $person->sub_total),2,',','.');
			

			if ($person->drop_payment == 0) {
				$row[] = $person->drop_payment;
			}else{
				$row[] = "Rp. ".number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $person->drop_payment),2,',','.');
			}
			
			if ($person->kekurangan == 0) {
				$row[] = $person->kekurangan;
			}else{
				$row[] = "Rp. ".number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $person->kekurangan),2,',','.');
			}

			if ($person->pelunasan == 0) {
				$row[] = $person->pelunasan;
			}else{
				$row[] = "Rp. ".number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $person->pelunasan),2,',','.');
			}

			$row[] = "Rp. ".number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $person->saldo),2,',','.');
			$row[] = $person->catatan;
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->laporan_model->count_all(),
						"recordsFiltered" => $this->laporan_model->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	function show_data_so_su()
	{

		$list = $this->laporan_model->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $person) {
			$no++;
			$row = array();
			$row[] = $person->so_number;
			$row[] = $person->so_date;
			$row[] = $person->nama_pelanggan;
			$row[] = $person->alamat;
		    $row[] = "Rp. " .number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $person->sub_total),2,',','.');
			

			if ($person->drop_payment == 0) {
				$row[] = $person->drop_payment;
			}else{
				$row[] = "Rp. ".number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $person->drop_payment),2,',','.');
			}
			
			if ($person->kekurangan == 0) {
				$row[] = $person->kekurangan;
			}else{
				$row[] = "Rp. ".number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $person->kekurangan),2,',','.');
			}

			if ($person->pelunasan == 0) {
				$row[] = $person->pelunasan;
			}else{
				$row[] = "Rp. ".number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $person->pelunasan),2,',','.');
			}

			$row[] = "Rp. ".number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $person->saldo),2,',','.');
			$row[] = $person->catatan;

			$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_person('."'".$person->so_number."'".')"><i class="glyphicon glyphicon-pencil"></i> Detail</a>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->laporan_model->count_all(),
						"recordsFiltered" => $this->laporan_model->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	function show_data_insentif()
	{

		$list = $this->laporan_model->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $person) {
			$no++;
			$row = array();
			$row[] = $person->tanggal;
			$row[] = $person->po_number;
			$row[] = $person->sales;
			$row[] = $person->client;
		    $row[] = "Rp. ".number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $person->nilai_kontrak),2,',','.');
		    $row[] = "Rp. ".number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $person->insentif),2,',','.');
			
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->laporan_model->count_all(),
						"recordsFiltered" => $this->laporan_model->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function detail_order($id)
	{
		$data = $this->laporan_model->get_by_id($id);
		echo json_encode($data);
	}

	public function update_order()
	{
		$this->laporan_model->update_order();
		redirect('laporan');
	}

	public function cetak_cashflow()
	{
		$this->load->library('fpdf');

		//Lebar A4= 210mm
		//Margin default= 10mm tiap sisi
		//Bidang Horizontal yang dapat ditulisi= 210 - (10*2) = 190mm

		$this->fpdf->AddPage("L");
		$this->fpdf->SetAutoPageBreak(true, 0);
		//SetFont ke Arial, B, 10pt
		$this->fpdf->SetFont('Arial','B', 15);

		//Cell(lebar, tinggi, teks, border, end line, [align])
		$this->fpdf->Cell(0, 15, 'DOUBLE A - HOUSE OF SUBLIMATION',0,1, 'C');
		$this->fpdf->Cell(0, 1, '', 0,1, 'C');
		$this->fpdf->Cell(0, 5, 'Laporan Alur Keuangan BANK Bulan '.DATE('F Y'),0,1, 'C');

		$this->fpdf->Cell(0, 10, '', 0,1, 'C');

		$this->fpdf->SetFont('Arial','B',10);

		$this->fpdf->Cell(7,6,'NO',1,0,'C');
		$this->fpdf->Cell(17,6,'Tanggal',1,0,'C');
		$this->fpdf->Cell(132,6,'Item',1,0,'C');
		$this->fpdf->Cell(40,6,'Pengeluaran',1,0,'C');
		$this->fpdf->Cell(40,6,'Pemasukan',1,0,'C');
		$this->fpdf->Cell(40,6,'Saldo',1,1,'C');
		 
		$this->fpdf->SetFont('Arial','',10);
		$out = 0;
		$in = 0;
		$no = 1;
		$t = 1;
		$cashflow_bank =  $this->laporan_model->get_data_cashflow('cashflow_bank')->result();
		foreach ($cashflow_bank as $row) {

			if ($row->pengeluaran == ' ') {
				$pengeluaran = '-';
				$out += 0;
			}else{
				$pengeluaran = "Rp. " . number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $row->pengeluaran),0,',','.');
				$out += preg_replace('/[^A-Za-z0-9\  ]/', '', $row->pengeluaran);
			}

			if ($row->pemasukan == ' ') {
				$pemasukan = '-';
				$in += 0;
			}else{
				$pemasukan = "Rp. " . number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $row->pemasukan),0,',','.');
				$in += preg_replace('/[^A-Za-z0-9\  ]/', '', $row->pemasukan);
			}

			$total = $in - $out;

			$saldo = "Rp. " . number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $row->saldo),0,',','.');

			if ($t == 21) {
				$this->fpdf->AddPage("L");
				$this->fpdf->Cell(0, 10, '', 0,1, 'C');
				$this->fpdf->Cell(7,6,$no++,1,0,'C');
			$this->fpdf->Cell(17,6,$row->tanggal,1,0,'C');
			$this->fpdf->Cell(132,6,$row->item,1,0);
			$this->fpdf->Cell(40,6,$pengeluaran,1,0,'C');
			$this->fpdf->Cell(40,6,$pemasukan,1,0,'C');
			$this->fpdf->Cell(40,6,$saldo,1,1,'C');
			$t = 1;
			}else{
				$t += 1;
				$this->fpdf->Cell(7,6,$no++,1,0,'C');
			$this->fpdf->Cell(17,6,$row->tanggal,1,0,'C');
			$this->fpdf->Cell(132,6,$row->item,1,0);
			$this->fpdf->Cell(40,6,$pengeluaran,1,0,'C');
			$this->fpdf->Cell(40,6,$pemasukan,1,0,'C');
			$this->fpdf->Cell(40,6,$saldo,1,1,'C');
			}

			
		} 

		$this->fpdf->Cell(100, 7, '', 0,1);
		$this->fpdf->Cell(97, 12,'',0);
		$this->fpdf->Cell(100, 6,'',0);
		$this->fpdf->Cell(39, 6,'Total Pemasukan',1);
		$this->fpdf->Cell(40, 6,"Rp. " . number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $in),0,',','.'),1,0,'C');

		$this->fpdf->Cell(0, 6, '', 0,1, 'C');
		
		$this->fpdf->Cell(7,6,'',0);
		$this->fpdf->Cell(90, 12,'',0);
		$this->fpdf->Cell(100, 6,'',0);
		$this->fpdf->Cell(39, 6,'Total Pengeluaran',1);
		$this->fpdf->Cell(40, 6,"Rp. " . number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $out),0,',','.'),1,0,'C');

		$this->fpdf->Cell(0, 6, '', 0,1, 'C');
		
		$this->fpdf->Cell(7,6,'',0);
		$this->fpdf->Cell(90, 12,'',0);
		$this->fpdf->Cell(100, 6,'',0);
		$this->fpdf->Cell(39, 6,'Total Saldo',1);
		$this->fpdf->Cell(40, 6,"Rp. " . number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $total),0,',','.'),1,0,'C');

		$this->fpdf->AddPage("L");
		$this->fpdf->SetAutoPageBreak(true, 0);
		//SetFont ke Arial, B, 10pt
		$this->fpdf->SetFont('Arial','B', 15);

		//Cell(lebar, tinggi, teks, border, end line, [align])
		$this->fpdf->Cell(0, 15, 'DOUBLE A - HOUSE OF SUBLIMATION',0,1, 'C');
		$this->fpdf->Cell(0, 1, '', 0,1, 'C');
		$this->fpdf->Cell(0, 5, 'Laporan Alur Keuangan CASH Bulan '.DATE('F Y'),0,1, 'C');

		$this->fpdf->Cell(0, 10, '', 0,1, 'C');

		$this->fpdf->SetFont('Arial','B',10);

		$this->fpdf->Cell(7,6,'NO',1,0,'C');
		$this->fpdf->Cell(17,6,'Tanggal',1,0,'C');
		$this->fpdf->Cell(132,6,'Item',1,0,'C');
		$this->fpdf->Cell(40,6,'Pengeluaran',1,0,'C');
		$this->fpdf->Cell(40,6,'Pemasukan',1,0,'C');
		$this->fpdf->Cell(40,6,'Saldo',1,1,'C');
		 
		$this->fpdf->SetFont('Arial','',10);
		
		$in2 = 0;
		$out2 = 0;
		$no = 1;
		$t = 1;
		$cashflow_cash =  $this->laporan_model->get_data_cashflow('cashflow_cash')->result();
		foreach ($cashflow_cash as $row) {

			if ($row->pengeluaran == ' ') {
				$pengeluaran = '-';
				$out2 += 0;
			}else{
				$pengeluaran = "Rp. " . number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $row->pengeluaran),0,',','.');
				$out2 += preg_replace('/[^A-Za-z0-9\  ]/', '', $row->pengeluaran);
			}

			if ($row->pemasukan == ' ') {
				$pemasukan = '-';
				$in2 += 0;
			}else{
				$pemasukan = "Rp. " . number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $row->pemasukan),0,',','.');
				$in2 += preg_replace('/[^A-Za-z0-9\  ]/', '', $row->pemasukan);
			}

			$total2 = $in2 - $out2;

			$saldo = "Rp. " . number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $row->saldo),0,',','.');

			if ($t == 21) {
				$this->fpdf->AddPage("L");
				$this->fpdf->Cell(0, 10, '', 0,1, 'C');
				$this->fpdf->Cell(7,6,$no++,1,0,'C');
			$this->fpdf->Cell(17,6,$row->tanggal,1,0,'C');
			$this->fpdf->Cell(132,6,$row->item,1,0);
			$this->fpdf->Cell(40,6,$pengeluaran,1,0,'C');
			$this->fpdf->Cell(40,6,$pemasukan,1,0,'C');
			$this->fpdf->Cell(40,6,$saldo,1,1,'C');
			$t = 1;
			}else{
				$t += 1;
				$this->fpdf->Cell(7,6,$no++,1,0,'C');
			$this->fpdf->Cell(17,6,$row->tanggal,1,0,'C');
			$this->fpdf->Cell(132,6,$row->item,1,0);
			$this->fpdf->Cell(40,6,$pengeluaran,1,0,'C');
			$this->fpdf->Cell(40,6,$pemasukan,1,0,'C');
			$this->fpdf->Cell(40,6,$saldo,1,1,'C');
			}


		} 
		
		$this->fpdf->Cell(100, 7, '', 0,1, 'L');
		$this->fpdf->Cell(97, 12,'',0);
		$this->fpdf->Cell(100, 6,'',0);
		$this->fpdf->Cell(39, 6,'Total Pemasukan',1 ,'R');
		$this->fpdf->Cell(40, 6,"Rp. " . number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $in2),0,',','.'),1,0,'C');

		$this->fpdf->Cell(0, 6, '', 0,1, 'C');
		
		$this->fpdf->Cell(7,6,'',0);
		$this->fpdf->Cell(90, 12,'',0);
		$this->fpdf->Cell(100, 6,'',0);
		$this->fpdf->Cell(39, 6,'Total Pengeluaran',1);
		$this->fpdf->Cell(40, 6,"Rp. " . number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $out2),0,',','.'),1,0,'C');

		$this->fpdf->Cell(0, 6, '', 0,1, 'C');
		
		$this->fpdf->Cell(7,6,'',0);
		$this->fpdf->Cell(90, 12,'',0);
		$this->fpdf->Cell(100, 6,'',0);
		$this->fpdf->Cell(39, 6,'Total Saldo',1);
		$this->fpdf->Cell(40, 6,"Rp. " . number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $total2),0,',','.'),1,0,'C');

		$this->fpdf->Cell(0, 6, '', 0,1, 'C');
		$this->fpdf->Cell(0, 6, '', 0,1, 'C');
		$this->fpdf->Cell(0, 6, '', 0,1, 'C');
		
		$this->fpdf->Output('Laporan-Cashflow-Bulan-'.date('F Y').'.pdf', 'I');

	}

	public function cetak_order()
	{

		$this->load->library('fpdf');

		//Lebar A4= 210mm
		//Margin default= 10mm tiap sisi
		//Bidang Horizontal yang dapat ditulisi= 210 - (10*2) = 190mm

		$this->fpdf->AddPage("L");

		//SetFont ke Arial, B, 10pt
		$this->fpdf->SetFont('Arial','B', 15);
		//Cell(lebar, tinggi, teks, border, end line, [align])
		$this->fpdf->Cell(0, 15, 'DOUBLE A - HOUSE OF SUBLIMATION',0,1, 'C');
		$this->fpdf->Cell(0, 1, '', 0,1, 'C');
		$this->fpdf->Cell(0, 5, 'LAPORAN ORDER '.date('F Y'),0,1, 'C');

		$this->fpdf->Cell(0, 10, '', 0,1, 'C');

		$this->fpdf->SetFont('Arial','B',10);

		$this->fpdf->Cell(7,6,'NO',1,0,'C');
		$this->fpdf->Cell(22,6,'So Number',1,0,'C');
		$this->fpdf->Cell(20,6,'So Date',1,0,'C');
		$this->fpdf->Cell(28,6,'Sales',1,0,'C');
		$this->fpdf->Cell(30,6,'Client',1,0,'C');
		$this->fpdf->Cell(32,6,'Nilai Kontrak',1,0,'C');
		$this->fpdf->Cell(32,6,'Drop Payment',1,0,'C');
		$this->fpdf->Cell(30,6,'Pelunasan',1,0,'C');
		$this->fpdf->Cell(25,6,'Kekurangan',1,0,'C');
		$this->fpdf->Cell(30,6,'Saldo',1,0,'C');
		$this->fpdf->Cell(20,6,'Catatan',1,1,'C');

		$this->fpdf->SetFont('Arial','',10);
		//Table with 2 rows and 4 columns
		$this->fpdf->SetWidths(array(7,22,20,28,30,32,32,30,25,30,20));
		$this->fpdf->SetAligns(array('C','C','C','C','C','','','',''));
		$this->fpdf->SetFont('Arial','',10);
		
		
		$saldo2 = 0;
		$total2 = 0;
		$no = 1;
		$data_so =  $this->laporan_model->getData_so()->result();
		foreach ($data_so as $so) {
			$so_number = $so->so_number;
			$so_date = $so->so_date;
			$sales = $so->sales;
			$dp = "Rp. " . number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $so->drop_payment),0,',','.');
			$pelunasan = "Rp. " . number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $so->pelunasan),0,',','.');
			$saldo = "Rp. " . number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $so->saldo),0,',','.');
			$catatan = $so->catatan;
			$nama_pelanggan = $so->nama_pelanggan;
			$total = "Rp. " . number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $so->sub_total),0,',','.');
			$kekurangan = "Rp. " . number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $so->kekurangan),0,',','.'); 

			if ($so->sub_total == ' ') {
				$total2 += 0;
			}else{
				$total2 += preg_replace('/[^A-Za-z0-9\  ]/', '', $so->sub_total);

			if ($so->saldo == ' ') {
				$saldo2 += 0;
			}else{
				$saldo2 += preg_replace('/[^A-Za-z0-9\  ]/', '', $so->saldo);
			}
				
			$subkekurangan = $total2 - $saldo2;

		    $this->fpdf->Row(array($no++,$so_number,$so_date,$sales,$nama_pelanggan,$total,$dp,$pelunasan,$kekurangan,$saldo,$catatan));
			
			} 

	}

		$this->fpdf->Cell(100, 7, '', 0,1, 'L');
		$this->fpdf->Cell(97, 12,'',0);
		$this->fpdf->Cell(104, 6,'',0);
		$this->fpdf->Cell(35, 6,'Sub Nilai Kontrak',1);
		$this->fpdf->Cell(40, 6,"Rp. " . number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $total2),0,',','.'),1,0,'C');

		$this->fpdf->Cell(0, 6, '', 0,1, 'C');
		
		$this->fpdf->Cell(7,6,'',0);
		$this->fpdf->Cell(90, 12,'',0);
		$this->fpdf->Cell(104, 6,'',0);
		$this->fpdf->Cell(35, 6,'Sub Saldo',1);
		$this->fpdf->Cell(40, 6,"Rp. " . number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $saldo2),0,',','.'),1,0,'C');

		$this->fpdf->Cell(0, 6, '', 0,1, 'C');
		
		$this->fpdf->Cell(7,6,'',0);
		$this->fpdf->Cell(90, 12,'',0);
		$this->fpdf->Cell(104, 6,'',0);
		$this->fpdf->Cell(35, 6,'Sub Kekurangan',1);
		$this->fpdf->Cell(40, 6,"Rp. " . number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $subkekurangan),0,',','.'),1,0,'C');

		$this->fpdf->Output('Laporan-Order-Bulan-'. date('F Y').'.pdf', 'I');
	}
	
	public function cetak_order_bulanlalu()
	{
		$this->load->library('fpdf');

		//Lebar A4= 210mm
		//Margin default= 10mm tiap sisi
		//Bidang Horizontal yang dapat ditulisi= 210 - (10*2) = 190mm

		$this->fpdf->AddPage("L");

		//SetFont ke Arial, B, 10pt
		$this->fpdf->SetFont('Arial','B', 15);
		//Cell(lebar, tinggi, teks, border, end line, [align])
		$this->fpdf->Cell(0, 15, 'DOUBLE A - HOUSE OF SUBLIMATION',0,1, 'C');
		$this->fpdf->Cell(0, 1, '', 0,1, 'C');
		$this->fpdf->Cell(0, 5, 'LAPORAN ORDER '. date('F Y',strtotime("-1 month")),0,1, 'C');

		$this->fpdf->Cell(0, 10, '', 0,1, 'C');

		$this->fpdf->SetFont('Arial','B',10);

		$this->fpdf->Cell(7,6,'NO',1,0,'C');
		$this->fpdf->Cell(22,6,'So Number',1,0,'C');
		$this->fpdf->Cell(20,6,'So Date',1,0,'C');
		$this->fpdf->Cell(28,6,'Sales',1,0,'C');
		$this->fpdf->Cell(30,6,'Client',1,0,'C');
		$this->fpdf->Cell(32,6,'Nilai Kontrak',1,0,'C');
		$this->fpdf->Cell(32,6,'Drop Payment',1,0,'C');
		$this->fpdf->Cell(30,6,'Pelunasan',1,0,'C');
		$this->fpdf->Cell(25,6,'Kekurangan',1,0,'C');
		$this->fpdf->Cell(30,6,'Saldo',1,0,'C');
		$this->fpdf->Cell(20,6,'Catatan',1,1,'C');

		$this->fpdf->SetFont('Arial','',10);
		//Table with 2 rows and 4 columns
		$this->fpdf->SetWidths(array(7,22,20,28,30,32,32,30,25,30,20));
		$this->fpdf->SetFont('Arial','',10);
		
		
		$saldo2 = 0;
		$total2 = 0;
		$no = 1;
		$data_so =  $this->laporan_model->getData_so_sebelum()->result();
		foreach ($data_so as $so) {
			$so_number = $so->so_number;
			$so_date = $so->so_date;
			$sales = $so->sales;
			$dp = "Rp. " . number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $so->drop_payment),0,',','.');
			$pelunasan = "Rp. " . number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $so->pelunasan),0,',','.');
			$saldo = "Rp. " . number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $so->saldo),0,',','.');
			$catatan = $so->catatan;
			$nama_pelanggan = $so->nama_pelanggan;
			$total = "Rp. " . number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $so->sub_total),0,',','.');
			$kekurangan = "Rp. " . number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $so->kekurangan),0,',','.'); 

			if ($so->sub_total == ' ') {
				$total2 += 0;
			}else{
				$total2 += preg_replace('/[^A-Za-z0-9\  ]/', '', $so->sub_total);

			if ($so->saldo == ' ') {
				$saldo2 += 0;
			}else{
				$saldo2 += preg_replace('/[^A-Za-z0-9\  ]/', '', $so->saldo);
			}
				
			$subkekurangan = $total2 - $saldo2;

		    $this->fpdf->Row(array($no++,$so_number,$so_date,$sales,$nama_pelanggan,$total,$dp,$pelunasan,$kekurangan,$saldo,$catatan)); 

	}

		$this->fpdf->Cell(100, 7, '', 0,1, 'L');
		$this->fpdf->Cell(97, 12,'',0);
		$this->fpdf->Cell(104, 6,'',0);
		$this->fpdf->Cell(35, 6,'Sub Nilai Kontrak',1);
		$this->fpdf->Cell(40, 6,"Rp. " . number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $total2),0,',','.'),1,0,'C');

		$this->fpdf->Cell(0, 6, '', 0,1, 'C');
		
		$this->fpdf->Cell(7,6,'',0);
		$this->fpdf->Cell(90, 12,'',0);
		$this->fpdf->Cell(104, 6,'',0);
		$this->fpdf->Cell(35, 6,'Sub Saldo',1);
		$this->fpdf->Cell(40, 6,"Rp. " . number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $saldo2),0,',','.'),1,0,'C');

		$this->fpdf->Cell(0, 6, '', 0,1, 'C');
		
		$this->fpdf->Cell(7,6,'',0);
		$this->fpdf->Cell(90, 12,'',0);
		$this->fpdf->Cell(104, 6,'',0);
		$this->fpdf->Cell(35, 6,'Sub Kekurangan',1);
		$this->fpdf->Cell(40, 6,"Rp. " . number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $subkekurangan),0,',','.'),1,0,'C');

		}


		$this->fpdf->Output('Laporan-Order-Bulan-'. date('F Y',strtotime("-1 month")).'.pdf', 'I');

		}

		public function cetak_keuangan()
	{
		$this->load->library('fpdf');

		//Lebar A4= 210mm
		//Margin default= 10mm tiap sisi
		//Bidang Horizontal yang dapat ditulisi= 210 - (10*2) = 190mm

		$this->fpdf->AddPage();

		//SetFont ke Arial, B, 10pt
		$this->fpdf->SetFont('Arial','B', 15);
		//Cell(lebar, tinggi, teks, border, end line, [align])
		$this->fpdf->Cell(0, 15, 'DOUBLE A - HOUSE OF SUBLIMATION',0,1, 'C');
		$this->fpdf->Cell(0, 1, '', 0,1, 'C');
		$this->fpdf->Cell(0, 5, 'LAPORAN KEUANGAN ASET '.date('F Y'),0,1, 'C');

		$this->fpdf->Cell(0, 10, '', 0,1, 'C');

		$this->fpdf->SetFont('Arial','B',10);

		$this->fpdf->Cell(7,6,'NO',1,0,'C');
		$this->fpdf->Cell(22,6,'Tanggal',1,0,'C');
		$this->fpdf->Cell(75,6,'Item',1,0,'C');
		$this->fpdf->Cell(32,6,'Pengeluaran',1,0,'C');
		$this->fpdf->Cell(55,6,'Keterangan',1,1,'C');
 
		$this->fpdf->SetFont('Arial','',10);
		
		$no = 1;
		$tot = 0;
		$data_aset =  $this->laporan_model->getData_aset()->result();
		foreach ($data_aset as $aset) {
			$tgl = $aset->tanggal;
			$item = $aset->item;
			$ket = $aset->ket;
			$pengeluaran = "Rp. " . number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $aset->pengeluaran),0,',','.');

			if ($aset->pengeluaran == '') {
				$tot += 0;
			}else{
				$tot += preg_replace('/[^A-Za-z0-9\  ]/', '', $aset->pengeluaran);
			}
		
			$this->fpdf->Cell(7,6,$no++,1,0,'C');
			$this->fpdf->Cell(22,6,$tgl,1,0,'C');
			$this->fpdf->Cell(75,6,$item,1,0);
			$this->fpdf->Cell(32,6,$pengeluaran,1,0,'C');
			$this->fpdf->Cell(55,6,$ket,1,1);

			}
			$this->fpdf->SetFont('Arial','B',10);
			$this->fpdf->Cell(29,6,'',0);
			$this->fpdf->Cell(75,6,'Sub Total',1,0,'C');
			$this->fpdf->Cell(32,6,"Rp. " . number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $tot),0,',','.'),1,0,'C');


			$this->fpdf->AddPage();

			$this->fpdf->SetFont('Arial','B', 15);
		//Cell(lebar, tinggi, teks, border, end line, [align])
		$this->fpdf->Cell(0, 15, 'DOUBLE A - HOUSE OF SUBLIMATION',0,1, 'C');
		$this->fpdf->Cell(0, 1, '', 0,1, 'C');
		$this->fpdf->Cell(0, 5, 'LAPORAN KEUANGAN BAHAN '.date('F Y'),0,1, 'C');

		$this->fpdf->Cell(0, 10, '', 0,1, 'C');

			$this->fpdf->SetFont('Arial','B',10);

		$this->fpdf->Cell(7,6,'NO',1,0,'C');
		$this->fpdf->Cell(22,6,'Tanggal',1,0,'C');
		$this->fpdf->Cell(75,6,'Item',1,0,'C');
		$this->fpdf->Cell(32,6,'Pengeluaran',1,0,'C');
		$this->fpdf->Cell(55,6,'Keterangan',1,1,'C');

		 
		$this->fpdf->SetFont('Arial','',10);
		$tot2= 0;
		$no = 1;
		$data_bahan =  $this->laporan_model->getData_bahan()->result();
		foreach ($data_bahan as $bahan) {
			$tgl = $bahan->tanggal;
			$item = $bahan->item;
			$ket = $bahan->ket;
			$pengeluaran = "Rp. " . number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $bahan->pengeluaran),0,',','.');

			if ($aset->pengeluaran == '') {
				$tot2 += 0;
			}else{
				$tot2 += preg_replace('/[^A-Za-z0-9\  ]/', '', $bahan->pengeluaran);
			}
			
		
			$this->fpdf->Cell(7,6,$no++,1,0,'C');
			$this->fpdf->Cell(22,6,$tgl,1,0,'C');
			$this->fpdf->Cell(75,6,$item,1,0);
			$this->fpdf->Cell(32,6,$pengeluaran,1,0,'C');
			$this->fpdf->Cell(55,6,$ket,1,1);

			}

			$this->fpdf->SetFont('Arial','B',10);
			$this->fpdf->Cell(29,6,'',0);
			$this->fpdf->Cell(75,6,'Sub Total',1,0,'C');
			$this->fpdf->Cell(32,6,"Rp. " . number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $tot2),0,',','.'),1,0,'C');

			$this->fpdf->AddPage();

			$this->fpdf->SetFont('Arial','B', 15);
		//Cell(lebar, tinggi, teks, border, end line, [align])
		$this->fpdf->Cell(0, 15, 'DOUBLE A - HOUSE OF SUBLIMATION',0,1, 'C');
		$this->fpdf->Cell(0, 1, '', 0,1, 'C');
		$this->fpdf->Cell(0, 5, 'LAPORAN KEUANGAN JAHIT '.date('F Y'),0,1, 'C');

		$this->fpdf->Cell(0, 10, '', 0,1, 'C');

			$this->fpdf->SetFont('Arial','B',10);

		$this->fpdf->Cell(7,6,'NO',1,0,'C');
		$this->fpdf->Cell(22,6,'Tanggal',1,0,'C');
		$this->fpdf->Cell(75,6,'Item',1,0,'C');
		$this->fpdf->Cell(32,6,'Pengeluaran',1,0,'C');
		$this->fpdf->Cell(55,6,'Keterangan',1,1,'C');

		 
		$this->fpdf->SetFont('Arial','',10);
		$tot3 = 0;
		$no = 1;
		$data_jahit =  $this->laporan_model->getData_jahit()->result();
		foreach ($data_jahit as $jahit) {
			$tgl = $jahit->tanggal;
			$item = $jahit->item;
			$ket = $jahit->ket;
			$pengeluaran = "Rp. " . number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $jahit->pengeluaran),0,',','.');

			if ($aset->pengeluaran == '') {
				$tot3 += 0;
			}else{
				$tot3 += preg_replace('/[^A-Za-z0-9\  ]/', '', $jahit->pengeluaran);
			}
			
		
			$this->fpdf->Cell(7,6,$no++,1,0,'C');
			$this->fpdf->Cell(22,6,$tgl,1,0,'C');
			$this->fpdf->Cell(75,6,$item,1,0);
			$this->fpdf->Cell(32,6,$pengeluaran,1,0,'C');
			$this->fpdf->Cell(55,6,$ket,1,1);

			}

			$this->fpdf->SetFont('Arial','B',10);
			$this->fpdf->Cell(29,6,'',0);
			$this->fpdf->Cell(75,6,'Sub Total',1,0,'C');
			$this->fpdf->Cell(32,6,"Rp. " . number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $tot3),0,',','.'),1,0,'C');

			$this->fpdf->AddPage();

			$this->fpdf->SetFont('Arial','B', 15);
		//Cell(lebar, tinggi, teks, border, end line, [align])
		$this->fpdf->Cell(0, 15, 'DOUBLE A - HOUSE OF SUBLIMATION',0,1, 'C');
		$this->fpdf->Cell(0, 1, '', 0,1, 'C');
		$this->fpdf->Cell(0, 5, 'LAPORAN KEUANGAN PENGIRIMAN '.date('F Y'),0,1, 'C');

		$this->fpdf->Cell(0, 10, '', 0,1, 'C');

			$this->fpdf->SetFont('Arial','B',10);

		$this->fpdf->Cell(7,6,'NO',1,0,'C');
		$this->fpdf->Cell(22,6,'Tanggal',1,0,'C');
		$this->fpdf->Cell(75,6,'Item',1,0,'C');
		$this->fpdf->Cell(32,6,'Pengeluaran',1,0,'C');
		$this->fpdf->Cell(55,6,'Keterangan',1,1,'C');

		 
		$this->fpdf->SetFont('Arial','',10);
		$tot4 = 0;
		$no = 1;
		$data_pengiriman =  $this->laporan_model->getData_pengiriman()->result();
		foreach ($data_pengiriman as $pengiriman) {
			$tgl = $pengiriman->tanggal;
			$item = $pengiriman->item;
			$ket = $pengiriman->ket;
			$pengeluaran = "Rp. " . number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $pengiriman->pengeluaran),0,',','.');

			if ($aset->pengeluaran == '') {
				$tot4 += 0;
			}else{
				$tot4 += preg_replace('/[^A-Za-z0-9\  ]/', '', $pengiriman->pengeluaran);
			}
			
		
			$this->fpdf->Cell(7,6,$no++,1,0,'C');
			$this->fpdf->Cell(22,6,$tgl,1,0,'C');
			$this->fpdf->Cell(75,6,$item,1,0);
			$this->fpdf->Cell(32,6,$pengeluaran,1,0,'C');
			$this->fpdf->Cell(55,6,$ket,1,1);

			}

			$this->fpdf->SetFont('Arial','B',10);
			$this->fpdf->Cell(29,6,'',0);
			$this->fpdf->Cell(75,6,'Sub Total',1,0,'C');
			$this->fpdf->Cell(32,6,"Rp. " . number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $tot4),0,',','.'),1,0,'C');

			$this->fpdf->AddPage();

			$this->fpdf->SetFont('Arial','B', 15);
		//Cell(lebar, tinggi, teks, border, end line, [align])
		$this->fpdf->Cell(0, 15, 'DOUBLE A - HOUSE OF SUBLIMATION',0,1, 'C');
		$this->fpdf->Cell(0, 1, '', 0,1, 'C');
		$this->fpdf->Cell(0, 5, 'LAPORAN KEUANGAN BEBAN BULANAN '.date('F Y'),0,1, 'C');

		$this->fpdf->Cell(0, 10, '', 0,1, 'C');

			$this->fpdf->SetFont('Arial','B',10);

		$this->fpdf->Cell(7,6,'NO',1,0,'C');
		$this->fpdf->Cell(22,6,'Tanggal',1,0,'C');
		$this->fpdf->Cell(75,6,'Item',1,0,'C');
		$this->fpdf->Cell(32,6,'Pengeluaran',1,0,'C');
		$this->fpdf->Cell(55,6,'Keterangan',1,1,'C');

		 
		$this->fpdf->SetFont('Arial','',10);
		$tot5 = 0;
		$no = 1;
		$data_beban =  $this->laporan_model->getData_beban()->result();
		foreach ($data_beban as $beban) {
			$tgl = $beban->tanggal;
			$item = $beban->item;
			$ket = $beban->ket;
			$pengeluaran = "Rp. " . number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $beban->pengeluaran),0,',','.');

			if ($aset->pengeluaran == '') {
				$tot5 += 0;
			}else{
				$tot5 += preg_replace('/[^A-Za-z0-9\  ]/', '', $beban->pengeluaran);
			}
			
		
			$this->fpdf->Cell(7,6,$no++,1,0,'C');
			$this->fpdf->Cell(22,6,$tgl,1,0,'C');
			$this->fpdf->Cell(75,6,$item,1,0);
			$this->fpdf->Cell(32,6,$pengeluaran,1,0,'C');
			$this->fpdf->Cell(55,6,$ket,1,1);

			}

			$this->fpdf->SetFont('Arial','B',10);
			$this->fpdf->Cell(29,6,'',0);
			$this->fpdf->Cell(75,6,'Sub Total',1,0,'C');
			$this->fpdf->Cell(32,6,"Rp. " . number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $tot5),0,',','.'),1,0,'C');

			$this->fpdf->AddPage();

			$this->fpdf->SetFont('Arial','B', 15);
		//Cell(lebar, tinggi, teks, border, end line, [align])
		$this->fpdf->Cell(0, 15, 'DOUBLE A - HOUSE OF SUBLIMATION',0,1, 'C');
		$this->fpdf->Cell(0, 1, '', 0,1, 'C');
		$this->fpdf->Cell(0, 5, 'LAPORAN KEUANGAN TAK TERDUGA '.date('F Y'),0,1, 'C');

		$this->fpdf->Cell(0, 10, '', 0,1, 'C');

			$this->fpdf->SetFont('Arial','B',10);

		$this->fpdf->Cell(7,6,'NO',1,0,'C');
		$this->fpdf->Cell(22,6,'Tanggal',1,0,'C');
		$this->fpdf->Cell(75,6,'Item',1,0,'C');
		$this->fpdf->Cell(32,6,'Pengeluaran',1,0,'C');
		$this->fpdf->Cell(55,6,'Keterangan',1,1,'C');

		 
		$this->fpdf->SetFont('Arial','',10);
		$tot6 = 0;
		$no = 1;
		$data_etc =  $this->laporan_model->getData_etc()->result();
		foreach ($data_etc as $etc) {
			$tgl = $etc->tanggal;
			$item = $etc->item;
			$ket = $etc->ket;
			$pengeluaran = "Rp. " . number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $etc->pengeluaran),0,',','.');

			if ($aset->pengeluaran == '') {
				$tot6 += 0;
			}else{
				$tot6 += preg_replace('/[^A-Za-z0-9\  ]/', '', $etc->pengeluaran);
			}
			
		
			$this->fpdf->Cell(7,6,$no++,1,0,'C');
			$this->fpdf->Cell(22,6,$tgl,1,0,'C');
			$this->fpdf->Cell(75,6,$item,1,0);
			$this->fpdf->Cell(32,6,$pengeluaran,1,0,'C');
			$this->fpdf->Cell(55,6,$ket,1,1);

			}

			$this->fpdf->SetFont('Arial','B',10);
			$this->fpdf->Cell(29,6,'',0);
			$this->fpdf->Cell(75,6,'Sub Total',1,0,'C');
			$this->fpdf->Cell(32,6,"Rp. " . number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $tot6),0,',','.'),1,0,'C');

		$this->fpdf->Output('Laporan-Keuangan-'.date('F Y').'.pdf', 'I');

	}

	public function cetak_cashbon()
	{
		$this->load->library('fpdf');

		//Lebar A4= 210mm
		//Margin default= 10mm tiap sisi
		//Bidang Horizontal yang dapat ditulisi= 210 - (10*2) = 190mm

		$this->fpdf->AddPage();

		//SetFont ke Arial, B, 10pt
		$this->fpdf->SetFont('Arial','B', 15);

		//Cell(lebar, tinggi, teks, border, end line, [align])
		$this->fpdf->Cell(0, 15, 'DOUBLE A - HOUSE OF SUBLIMATION',0,1, 'C');
		$this->fpdf->Cell(0, 1, '', 0,1, 'C');
		$this->fpdf->Cell(0, 5, 'LAPORAN CASH BON PEGAWAI BULAN '. date('F Y'),0,1, 'C');

		$this->fpdf->SetFont('Arial','B',10);

		$data = array();
		$arr = array();
		$data_karyawan =  $this->data_karyawan_model->getData()->result();
		foreach ($data_karyawan as $row) {

			$this->fpdf->Cell(0, 10, '', 0,1, 'C');
			$this->fpdf->Cell(0, 10,$row->nama_karyawan, 0,1, 'C');

			$this->fpdf->SetFont('Arial','B',10);
			$this->fpdf->Cell(35,6,'',0);
			$this->fpdf->Cell(7,6,'NO',1,0,'C');
			$this->fpdf->Cell(17,6,'Tanggal',1,0,'C');
			$this->fpdf->Cell(35,6,'Pinjaman',1,0,'C');
			$this->fpdf->Cell(35,6,'Pengembalian',1,0,'C');
			$this->fpdf->Cell(35,6,'Sisa Pinjaman',1,1,'C');

			
			$arr['nama'] = $row->nama_karyawan;
			$no = 1;
			$pinjaman2 = 0;
			$pengembalian2 = 0;
			$data_cashbon =  $this->data_karyawan_model->getData_cashflow_cashbon()->result();
			foreach ($data_cashbon as $row2) {
				if ($row->id == $row2->id) {
					$tgl = $row2->tanggal;
					if ($row2->pinjaman == ' ') {
						$pinjaman = '-';
						$pinjaman2 += 0;
					}else{
						$pinjaman = "Rp. " . number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $row2->pinjaman),0,',','.');
						$pinjaman2 += preg_replace('/[^A-Za-z0-9\  ]/', '', $row2->pinjaman);
					}

					if ($row2->pengembalian == ' ') {
						$pengembalian = '-';
						$pengembalian2 += 0;
					}else{
						$pengembalian = "Rp. " . number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $row2->pengembalian),0,',','.');
						$pengembalian2 += preg_replace('/[^A-Za-z0-9\  ]/', '', $row2->pengembalian);
					}

					$sisa_pinjaman = "Rp. " . number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $row2->sisa_pinjaman),0,',','.');

					 $totalsisapinjaman = $pinjaman2 - $pengembalian2;

			$this->fpdf->SetFont('Arial','B',10);
			$this->fpdf->Cell(35,6,'',0);
			$this->fpdf->Cell(7,6,$no++,1,0,'C');
			$this->fpdf->Cell(17,6,$tgl,1,0,'C');
			$this->fpdf->Cell(35,6,$pinjaman,1,0,'C');
			$this->fpdf->Cell(35,6,$pengembalian,1,0,'C');
			$this->fpdf->Cell(35,6,$sisa_pinjaman,1,1,'C');
					
				}
			}
			$arr['pinjaman2'] = $pinjaman2;
			$arr['pengembalian2'] = $pengembalian2;
			$arr['total'] = $totalsisapinjaman;
			$data[] = $arr;
			$this->fpdf->Cell(70, 4, '', 0,1);
			$this->fpdf->Cell(85, 8,'',0);
			$this->fpdf->Cell(1, 6,'',0);
			$this->fpdf->Cell(45, 6,'Sub Total Pinjaman',1);
			$this->fpdf->Cell(33, 6,"Rp. " . number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $pinjaman2),0,',','.'),1,0,'C');

			$this->fpdf->Cell(0, 6, '', 0,1, 'C');
		
			$this->fpdf->Cell(85, 6,'',0);
			$this->fpdf->Cell(1, 6,'',0);
			$this->fpdf->Cell(45, 6,'Sub Total Pengembalian',1);
			$this->fpdf->Cell(33, 6,"Rp. " . number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $pengembalian2),0,',','.'),1,0,'C');

			$this->fpdf->Cell(0, 6, '', 0,1, 'C');
			
			$this->fpdf->Cell(85, 8,'',0);
			$this->fpdf->Cell(1, 6,'',0);
			$this->fpdf->Cell(45, 6,'Sub Total Sisa Pinjaman',1);
			$this->fpdf->Cell(33, 6,"Rp. " . number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $totalsisapinjaman),0,',','.'),1,0,'C');

		}

		$this->fpdf->Cell(0, 20, '', 0,1, 'C');

		$this->fpdf->SetFont('Arial','B',10);

		$this->fpdf->Cell(30,6,'',0);
		$this->fpdf->Cell(7,6,'NO',1,0,'C');
		$this->fpdf->Cell(50,6,'Nama',1,0,'C');
		$this->fpdf->Cell(25,6,'Pinjaman',1,0,'C');
		$this->fpdf->Cell(27,6,'Pengembalian',1,0,'C');
		$this->fpdf->Cell(27,6,'Sisa Pinjaman',1,1,'C');
		$n = 1;
		$tot_pinjam = 0;
		$tot_pengem = 0;
		$tot_sisa = 0;
		foreach ($data as $row) {
			$tot_pinjam += $row['pinjaman2'];
			$tot_pengem += $row['pengembalian2'];
			$tot_sisa += $row['total'];
			$this->fpdf->Cell(30,6,'',0);
			$this->fpdf->Cell(7,6,$n++,1,0,'C');
			$this->fpdf->Cell(50,6,$row['nama'],1,0,'C');
			$this->fpdf->Cell(25,6,"Rp. " . number_format(preg_replace('/[^A-Za-z0-9\  ]/', '',$row['pinjaman2']),0,',','.'),1,0,'C');
			$this->fpdf->Cell(27,6,"Rp. " . number_format(preg_replace('/[^A-Za-z0-9\  ]/', '',$row['pengembalian2']),0,',','.'),1,0,'C');
			$this->fpdf->Cell(27,6,"Rp. " . number_format(preg_replace('/[^A-Za-z0-9\  ]/', '',$row['total']),0,',','.'),1,1,'C');
		
		}
			$this->fpdf->Cell(30,6,'',0);
			$this->fpdf->Cell(7,6,'',0,0);
			$this->fpdf->Cell(50,6,'Sub Total',1,0,'C');
			$this->fpdf->Cell(25,6,"Rp. " . number_format(preg_replace('/[^A-Za-z0-9\  ]/', '',$tot_pinjam),0,',','.'),1,0,'C');
			$this->fpdf->Cell(27,6,"Rp. " . number_format(preg_replace('/[^A-Za-z0-9\  ]/', '',$tot_pengem),0,',','.'),1,0,'C');
			$this->fpdf->Cell(27,6,"Rp. " . number_format(preg_replace('/[^A-Za-z0-9\  ]/', '',$tot_sisa),0,',','.'),1,0,'C');

			$this->fpdf->Output('Laporan-Cashbon-'. date('F Y').'.pdf', 'I');

		}

	public function cetak_insentif()
	{
		$this->load->library('fpdf');

		//Lebar A4= 210mm
		//Margin default= 10mm tiap sisi
		//Bidang Horizontal yang dapat ditulisi= 210 - (10*2) = 190mm

		$this->fpdf->AddPage();

		//SetFont ke Arial, B, 10pt
		$this->fpdf->SetFont('Arial','B', 15);
		//Cell(lebar, tinggi, teks, border, end line, [align])
		$this->fpdf->Cell(0, 15, 'DOUBLE A - HOUSE OF SUBLIMATION',0,1, 'C');
		$this->fpdf->Cell(0, 1, '', 0,1, 'C');
		$this->fpdf->Cell(0, 5, 'LAPORAN INSENTIF '. date('F Y'),0,1, 'C');

		$this->fpdf->Cell(0, 10, '', 0,1, 'C');

		$this->fpdf->SetFont('Arial','B',10);

		$this->fpdf->Cell(7,6,'NO',1,0,'C');
		$this->fpdf->Cell(18,6,'Tanggal',1,0,'C');
		$this->fpdf->Cell(25,6,'Po Number',1,0,'C');
		$this->fpdf->Cell(37,6,'Sales',1,0,'C');
		$this->fpdf->Cell(37,6,'Client',1,0,'C');
		$this->fpdf->Cell(33,6,'Nilai Kontrak',1,0,'C');
		$this->fpdf->Cell(33,6,'Insentif',1,1,'C');
 
		$this->fpdf->SetFont('Arial','',10);
		

		$insentif2 = 0;
		$no = 1;
		$data_intensif =  $this->laporan_model->getData_insentif()->result();
		foreach ($data_intensif as $intensif) {
			$tgl = $intensif->tanggal;
			$ponumber = $intensif->po_number;
			$sales = $intensif->sales;
			$client = $intensif->client;
			$nilai_kontrak = "Rp. " . number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $intensif->nilai_kontrak),0,',','.');
			$insentif = "Rp. " . number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $intensif->insentif),0,',','.');

			if ($intensif->insentif == ' ') {
				$insentif2 += 0;
			}else{
				$insentif2 += preg_replace('/[^A-Za-z0-9\  ]/', '', $intensif->insentif);
			}
			
			$this->fpdf->Cell(7,6,$no++,1,0,'C');
			$this->fpdf->Cell(18,6,$tgl,1,0,'C');
			$this->fpdf->Cell(25,6,$ponumber,1,0,'C');
			$this->fpdf->Cell(37,6,$sales,1,0,'C');
			$this->fpdf->Cell(37,6,$client,1,0,'C');
			$this->fpdf->Cell(33,6,$nilai_kontrak,1,0,'C');
			$this->fpdf->Cell(33,6,$insentif,1,1,'C');
			} 
			$this->fpdf->SetFont('Arial','B',10);

			$this->fpdf->Cell(100, 7, '', 0,1, 'L');
			$this->fpdf->Cell(97, 12,'',0);
			$this->fpdf->Cell(27, 6,'',0);
			$this->fpdf->Cell(33, 6,'Sub Total Insentif',1);
			$this->fpdf->Cell(33, 6,"Rp. " . number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $insentif2),0,',','.'),1,0,'C');

		$this->fpdf->Output('Laporan-Insentif-'. date('F Y').'.pdf', 'I');

		}
}
