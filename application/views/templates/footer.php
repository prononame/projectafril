<!--   Core JS Files   -->
  <script src="<?php echo base_url('assets/js/core/jquery.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/jquery.price_format.js'); ?>"></script>
  <script src="<?php echo base_url('assets/ui/jquery-1.8.2.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/ui/jquery.autocomplete.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/jquery.mask.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/core/popper.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/core/bootstrap-material-design.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/plugins/perfect-scrollbar.jquery.min.js'); ?>"></script>
  <!-- Plugin for the momentJs  -->
  <script src="<?php echo base_url('assets/js/plugins/moment.min.js'); ?>"></script>
  <!--  Plugin for Sweet Alert -->
  <script src="<?php echo base_url('assets/js/plugins/sweetalert2.js'); ?>"></script>
  <!-- Forms Validations Plugin -->
  <script src="<?php echo base_url('assets/js/plugins/jquery.validate.min.js'); ?>"></script>
  <!-- Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
  <script src="<?php echo base_url('assets/js/plugins/jquery.bootstrap-wizard.js'); ?>"></script>
  <!--  Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
  <script src="<?php echo base_url('assets/js/plugins/bootstrap-selectpicker.js'); ?>"></script>
  <!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
  <script src="<?php echo base_url('assets/js/plugins/bootstrap-datetimepicker.min.js'); ?>"></script>
  <!--  DataTables.net Plugin, full documentation here: https://datatables.net/  -->
  <script src="<?php echo base_url('assets/js/plugins/jquery.dataTables.min.js'); ?>"></script>
  <!--  Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
  <script src="<?php echo base_url('assets/js/plugins/bootstrap-tagsinput.js'); ?>"></script>
  <!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
  <script src="<?php echo base_url('assets/js/plugins/jasny-bootstrap.min.js'); ?>"></script>
  <!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
  <script src="<?php echo base_url('assets/js/plugins/fullcalendar.min.js'); ?>"></script>
  <!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
  <script src="<?php echo base_url('assets/js/plugins/jquery-jvectormap.js'); ?>"></script>
  <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
  <script src="<?php echo base_url('assets/js/plugins/nouislider.min.js'); ?>"></script>
  <!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
  <!-- Library for adding dinamically elements -->
  <script src="<?php echo base_url('assets/js/plugins/arrive.min.js'); ?>"></script>
  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
  <script type="text/javascript" src="<?php echo base_url('assets/js/jquery-1.9.1.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/sweetalert.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/sweetalert-dev'); ?>"></script>
  <script src="<?php echo base_url('assets/js/qunit-1.18.0.js'); ?>"></script>
  <!-- Chartist JS -->
  <script src="<?php echo base_url('assets/js/plugins/chartist.min.js'); ?>"></script>
  <!--  Notifications Plugin    -->
  <script src="<?php echo base_url('assets/js/plugins/bootstrap-notify.js'); ?>"></script>
  <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="<?php echo base_url('assets/js/material-dashboard.js?v=2.1.1'); ?>" type="text/javascript"></script>
  <!-- Material Dashboard DEMO methods, don't include it in your project! -->
  <script src="<?php echo base_url('assets/demo/demo.js'); ?>"></script>
  <script>
    $(document).ready(function() {
      $().ready(function() {

        $('#harga_barang').mask("000.0000.000.000", {reverse:true});
        $('#dp').mask("000.0000.000.000", {reverse:true});
        $('#inputPengeluaran').mask("000.0000.000.000", {reverse:true});
        $('#inputPengeluaran_gopay').mask("000.0000.000.000", {reverse:true});
        $('#jmlh_pinjaman').mask("000.0000.000.000", {reverse:true});
        $('#jmlh_pembayaran').mask("000.0000.000.000", {reverse:true});
        $('#insentif').mask("000.0000.000.000", {reverse:true});
        $('#nilai_kontrak').mask("000.0000.000.000", {reverse:true});
        $('#jmlh_saldo').mask("000.0000.000.000", {reverse:true});
        $('#jmlh_oprasional').mask("000.0000.000.000", {reverse:true});
        $('#jmlh_drop_cash').mask("000.0000.000.000", {reverse:true});
        $('#jmlh_pemasukan_bank').mask("000.0000.000.000", {reverse:true});
        $('#jmlh_saldo_gopay').mask("000.0000.000.000", {reverse:true});
        $('#oprasional_bulan_depan').mask("000.0000.000.000", {reverse:true});
        $('#saldo_awal_cash').mask("000.0000.000.000", {reverse:true});
        $('#saldo_awal_bank').mask("000.0000.000.000", {reverse:true});
                
        $sidebar = $('.sidebar');

        $sidebar_img_container = $sidebar.find('.sidebar-background');

        $full_page = $('.full-page');

        $sidebar_responsive = $('body > .navbar-collapse');

        window_width = $(window).width();

        fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();

        if (window_width > 767 && fixed_plugin_open == 'Dashboard') {
          if ($('.fixed-plugin .dropdown').hasClass('show-dropdown')) {
            $('.fixed-plugin .dropdown').addClass('open');
          }

        }

        $('.fixed-plugin a').click(function(event) {
          // Alex if we click on switch, stop propagation of the event, so the dropdown will not be hide, otherwise we set the  section active
          if ($(this).hasClass('switch-trigger')) {
            if (event.stopPropagation) {
              event.stopPropagation();
            } else if (window.event) {
              window.event.cancelBubble = true;
            }
          }
        });

        $('.fixed-plugin .active-color span').click(function() {
          $full_page_background = $('.full-page-background');

          $(this).siblings().removeClass('active');
          $(this).addClass('active');

          var new_color = $(this).data('color');

          if ($sidebar.length != 0) {
            $sidebar.attr('data-color', new_color);
          }

          if ($full_page.length != 0) {
            $full_page.attr('filter-color', new_color);
          }

          if ($sidebar_responsive.length != 0) {
            $sidebar_responsive.attr('data-color', new_color);
          }
        });

        $('.fixed-plugin .background-color .badge').click(function() {
          $(this).siblings().removeClass('active');
          $(this).addClass('active');

          var new_color = $(this).data('background-color');

          if ($sidebar.length != 0) {
            $sidebar.attr('data-background-color', new_color);
          }
        });

        $('.fixed-plugin .img-holder').click(function() {
          $full_page_background = $('.full-page-background');

          $(this).parent('li').siblings().removeClass('active');
          $(this).parent('li').addClass('active');


          var new_image = $(this).find("img").attr('src');

          if ($sidebar_img_container.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
            $sidebar_img_container.fadeOut('fast', function() {
              $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
              $sidebar_img_container.fadeIn('fast');
            });
          }

          if ($full_page_background.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
            var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

            $full_page_background.fadeOut('fast', function() {
              $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
              $full_page_background.fadeIn('fast');
            });
          }

          if ($('.switch-sidebar-image input:checked').length == 0) {
            var new_image = $('.fixed-plugin li.active .img-holder').find("img").attr('src');
            var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

            $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
            $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
          }

          if ($sidebar_responsive.length != 0) {
            $sidebar_responsive.css('background-image', 'url("' + new_image + '")');
          }
        });

        $('.switch-sidebar-image input').change(function() {
          $full_page_background = $('.full-page-background');

          $input = $(this);

          if ($input.is(':checked')) {
            if ($sidebar_img_container.length != 0) {
              $sidebar_img_container.fadeIn('fast');
              $sidebar.attr('data-image', '#');
            }

            if ($full_page_background.length != 0) {
              $full_page_background.fadeIn('fast');
              $full_page.attr('data-image', '#');
            }

            background_image = true;
          } else {
            if ($sidebar_img_container.length != 0) {
              $sidebar.removeAttr('data-image');
              $sidebar_img_container.fadeOut('fast');
            }

            if ($full_page_background.length != 0) {
              $full_page.removeAttr('data-image', '#');
              $full_page_background.fadeOut('fast');
            }

            background_image = false;
          }
        });

        $('.switch-sidebar-mini input').change(function() {
          $body = $('body');

          $input = $(this);

          if (md.misc.sidebar_mini_active == true) {
            $('body').removeClass('sidebar-mini');
            md.misc.sidebar_mini_active = false;

            $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar();

          } else {

            $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar('destroy');

            setTimeout(function() {
              $('body').addClass('sidebar-mini');

              md.misc.sidebar_mini_active = true;
            }, 300);
          }

          // we simulate the window Resize so the charts will get updated in realtime.
          var simulateWindowResize = setInterval(function() {
            window.dispatchEvent(new Event('resize'));
          }, 180);

          // we stop the simulation of Window Resize after the animations are completed
          setTimeout(function() {
            clearInterval(simulateWindowResize);
          }, 1000);

        });
      });
    });
  </script>
  <script>
    $(document).ready(function() {
      // Javascript method's body can be found in assets/js/demos.js
      md.initDashboardPageCharts();

    });
  </script>
  <script type="text/javascript">
          $(document).ready(function(){

            $('#keseluruhan').css("visibility", "hidden");

                                $('#harga_barang').on('keyup', function(){
                                  var qtyBarang = $('#qty_barang').val();
                                  var diskonBarang = $('#diskon_barang').val();
                                  var arr = diskonBarang.split("%");
                                  var diskon_temp = "";

                                    for (var i = 0; i < arr.length; i++) {
                                      diskon_temp += arr[i]
                                    }
                                  var temp_hargaBarang = $('#harga_barang').val();
                                  var arr2 = temp_hargaBarang.split(".");
                                  var hargaBarang = "";

                                    for (var i = 0; i < arr2.length; i++) {
                                      hargaBarang += arr2[i]
                                    }

                                    if (qtyBarang === "") {
                                      $('#total_harga').val(hargaBarang);
                                    }else{
                                      var totalHarga = qtyBarang * hargaBarang;
                                      totalHarga = totalHarga - (totalHarga * (diskon_temp / 100));
                                      $('#total_harga').val(totalHarga); 
                                    }
                              });

                                $('#qty_barang').on('keyup', function(){
                                  var qtyBarang = $('#qty_barang').val();
                                  var diskonBarang = $('#diskon_barang').val();
                                  var arr = diskonBarang.split("%");
                                  var diskon_temp = "";

                                    for (var i = 0; i < arr.length; i++) {
                                      diskon_temp += arr[i]
                                    }
                                  var temp_hargaBarang = $('#harga_barang').val();
                                  var arr2 = temp_hargaBarang.split(".");
                                  var hargaBarang = "";

                                    for (var i = 0; i < arr2.length; i++) {
                                      hargaBarang += arr2[i]
                                    }

                                    if (qtyBarang === "") {
                                      $('#total_harga').val(hargaBarang);
                                    }else{
                                      var totalHarga = qtyBarang * hargaBarang;
                                      totalHarga = totalHarga - (totalHarga * (diskon_temp / 100));
                                      $('#total_harga').val(totalHarga); 
                                    }
                              });

                                $('#diskon_barang').on('focusout', function(){
                                  var diskonBarang = $('#diskon_barang').val();
                                  var totHarga = $('#total_harga').val();

                                  var arr = diskonBarang.split("%");
                                  var diskon_temp = "";

                                    for (var i = 0; i < arr.length; i++) {
                                      diskon_temp += arr[i]
                                    }
                                  
                                  var diskon = totHarga * (diskon_temp / 100);
                                  var grandDiskon = totHarga - diskon;

                                  var persen = diskonBarang + "%";

                                  $('#diskon_barang').val(persen);
                                  $('#total_harga').val(grandDiskon);
                                  
                              });

                                $('#diskon_barang').on('keydown', function(){
                                  var qtyBarang = $('#qty_barang').val();
                                  var temp_hargaBarang = $('#harga_barang').val();
                                  var arr = temp_hargaBarang.split(".");
                                  var hargaBarang = "";

                                    for (var i = 0; i < arr.length; i++) {
                                      hargaBarang += arr[i]
                                    }

                                    if (qtyBarang === "") {
                                      $('#total_harga').val(hargaBarang);
                                    }else{
                                      var totalHarga = qtyBarang * hargaBarang;
                                    $('#total_harga').val(totalHarga);
                                    }
                                  
                              });

            $('#dp').on('keyup', function(){
                var temp_subTotal = $('#sub_total').val();
                var arr1 = temp_subTotal.split(".");
                var subTotal = "";

                  for (var i = 0; i < arr1.length; i++) {
                    subTotal += arr1[i]
                  }

                var temp_dp = $('#dp').val();
                var arr2 = temp_dp.split(".");
                var dp = "";

                  for (var i = 0; i < arr2.length; i++) {
                    dp += arr2[i]
                  }

                  if (dp >= temp_subTotal/2) {
                    $('#ket_dp').css('color', 'green');
                    $('#ket_dp').html('Nominal Sesuai');
                  }else{
                    $('#ket_dp').css('color', 'red');
                    $('#ket_dp').html('Nominal DP minimal 50%');
                  }

                  var kekurangan = subTotal - dp;
                  $('#kekurangan').val(kekurangan);

                  if (($('#ket_dp').html() == 'Nominal Sesuai') && (kekurangan >= 0)) {
                    $('#submitOrder').css('visibility', 'visible');
                  }else{
                    $('#submitOrder').css('visibility', 'hidden');
                  }
                  
            });
            
            // jQuery Proses Temp Order
            $("#tombol-temp").click(function(){
              if (($('#des_barang').val() == "") || ($('#qty_barang').val() == "") || ($('#sat_barang').val() == "") || ($('#des_pekerjaan').val() == "") || ($('#harga_barang').val() == "") || ($('#diskon_barang').val() == "") || ($('#total_harga').val() == "") || ($('#ket').val() == "")) {
                alert("Data belum lengkap!");

              }else{

                var data = $('#temp-order').serialize();
                $.ajax({
                  type: 'POST',
                  url: "<?php echo base_url('sales-order/proses-temp-order');?>",
                  data: data,
                  success: function() {
                    $('#tabel-temp').load("<?php echo base_url('sales-order/data-temp-order');?>");
                    $('#keseluruhan').css("visibility", "visible");
                    $('#des_barang').val("");
                    $('#qty_barang').val("");
                    $('#sat_barang').val("");
                    $('#des_pekerjaan').val("");
                    $('#harga_barang').val("");
                    $('#diskon_barang').val("");
                    $('#total_harga').val("");
                    $('#ket').val("");
                    $.ajax({
                      type: 'ajax',
                      url: "<?php echo base_url('sales-order/proses-ambil-total');?>",
                      ansync : false,
                      dataType: 'json',
                      success: function(data) {
                        var tot = 0;
                        for (var i = 0; i < data.length; i++) {
                         var temp = data[i].total;
                         tot = parseInt(tot) + parseInt(temp);
                        }
                        $('#sub_total').val(tot);
                        $('#submitOrder').css("visibility", "visible");
                      }
                    });
                  }
                });

              }
            });

            // jQuery Proses Temp Order Prioritas
            $("#tombol-temp-p").click(function(){
              if (($('#des_barang').val() == "") || ($('#qty_barang').val() == "") || ($('#sat_barang').val() == "") || ($('#des_pekerjaan').val() == "") || ($('#harga_barang').val() == "") || ($('#diskon_barang').val() == "") || ($('#total_harga').val() == "") || ($('#ket').val() == "")) {
                alert("Data belum lengkap!");

              }else{

                var data = $('#temp-order-p').serialize();
                $.ajax({
                  type: 'POST',
                  url: "<?php echo site_url('order_prioritas/proses_temp_order');?>",
                  data: data,
                  success: function() {
                    $('#tabel-temp').load("<?php echo site_url('order_prioritas/data_temp_order');?>");
                    $('#keseluruhan').css("visibility", "visible");
                    $('#des_barang').val("");
                    $('#qty_barang').val("");
                    $('#sat_barang').val("");
                    $('#des_pekerjaan').val("");
                    $('#harga_barang').val("");
                    $('#diskon_barang').val("");
                    $('#total_harga').val("");
                    $('#ket').val("");
                    $.ajax({
                      type: 'ajax',
                      url: "<?php echo site_url('order_prioritas/proses_ambil_total');?>",
                      ansync : false,
                      dataType: 'json',
                      success: function(data) {
                        var tot = 0;
                        for (var i = 0; i < data.length; i++) {
                         var temp = data[i].total;
                         tot = parseInt(tot) + parseInt(temp);
                        }
                        $('#sub_total').val(tot);
                        $('#submitOrder').css("visibility", "visible");
                      }
                    });
                  }
                });

              }
            });

            //function validasi pengeluaran
            $("#typePengeluaran").on('change',function(){
              if ($('#typePengeluaran').val() == 'nol') {
                $('#submitPengeluaran').css('visibility', 'hidden');
              }else if ($(this).val() == 'tabel_gojek'){
                $('#input_p').html('<input type="text" class="form-control" id="inputPengeluaran_gopay" name="pengeluaran" onkeypress="return hanyaAngka(event)" required>');
                var isi_saldo_gopay;
                if ($('#sisa_saldo_gopay').val() == 'Rp. 0') {
                  $('#ket_saldo_gopay').html('Sisa Saldo Rp. 0, silakan isi saldo terlebih dahulu!');
                  $('#submitPengeluaran').css('visibility', 'hidden');
                }else{
                 $.ajax({
                  url : "<?php echo site_url('pengeluaran/get_saldo_gopay')?>",
                  type: "GET",
                  dataType: "JSON",
                  success: function(data) {
                    for (var i = 0; i < data.length; i++) {
                       isi_saldo_gopay = data[i].jumlah; 
                   }
                  }
                 });  
                }
                $('#inputPengeluaran_gopay').on('keyup', function() {

                var temp_input = $(this).val();
                var arr1 = temp_input.split(".");
                var input_pengeluaran = "";

                for (var i = 0; i < arr1.length; i++) {
                    input_pengeluaran += arr1[i]
                }

                input_pengeluaran = parseInt(input_pengeluaran) + 0;
                isi_saldo_gopay = parseInt(isi_saldo_gopay) + 0;
                if (input_pengeluaran > isi_saldo_gopay) {
                    $('#ket_saldo_gopay').html('Saldo tidak mencukupi, silakan isi saldo terlebih dahulu!');
                    $('#submitPengeluaran').css('visibility', 'hidden');
                   }else{
                    $('#ket_saldo_gopay').html('');
                    $('#submitPengeluaran').css('visibility', 'visible');
                   }

              });

              $('#jenis_pembayaran').html('');

              }else{
                $('#input_p').html('<input type="text" class="form-control" id="inputPengeluaran" name="pengeluaran" onkeypress="return hanyaAngka(event)" required>');
                $('#ket_saldo_gopay').html('');
                $('#jenis_pembayaran').html('Jenis Pembayaran :'+
                                              '<br>'+
                                            '<div class="form-check form-check-inline">'+
                                              '<input type="radio" name="tipe_pembayaran" id="tipe_pembayaran_bank" value="bank" required>'+
                                              '<label class="form-check-label" for="tipe_pembayaran_bank">Bank</label>'+
                                            '</div>'+
                                            '<div class="form-check form-check-inline">'+
                                              '<input type="radio" name="tipe_pembayaran" id="tipe_pembayaran_cash" value="cash" required>'+
                                              '<label class="form-check-label" for="tipe_pembayaran_cash">Cash</label>'+
                                            '</div>'+
                                            '<br>');
                $('#submitPengeluaran').css('visibility', 'visible');
              }
            });  

            //function validasi pemasukan
            $("#typePemasukan").on('change',function(){
              if ($('#typePemasukan').val() == 'nol') {
                $('#selectCari').css('visibility', 'hidden');
              }else{
                $('#selectCari').css('visibility', 'visible');
              }
            }); 

            //function menampilkan data so untuk pembayaran
            $("#selectSO").on('change',function(){
              if ($('#typePemasukan').val() == 'nol') {
                $('#tabelData').css('visibility', 'hidden');
              }else{

              var nmr_so = $(this).val();
              $.ajax({
                type: 'ajax',
                url: "<?php echo base_url('pemasukan/getData-so');?>",
                ansync : false,
                dataType: 'json',
                 success: function(data) {
                  $('#tabelData').css('visibility', 'visible');
                  $('#tabelData').html('<div class="table-responsive">' +
                                          '<table class="table">' +    
                                            '<caption>Detail Order</caption>' +
                                              '<thead>' +
                                                '<tr>' +
                                                  '<th scope="col">So Number</th>' +
                                                  '<th scope="col">So Date</th>' +
                                                  '<th scope="col">Nama Pelanggan</th>' +
                                                  '<th scope="col">Alamat</th>' +
                                                  '<th scope="col">Nomor Telfon</th>' +
                                                  '<th scope="col">Sub Total</th>' +
                                                  '<th scope="col">Drop Payment</th>' +
                                                  '<th scope="col">Kekurangan</th>' +
                                                '</tr>' +
                                              '</thead>' +
                                              '<tbody>' +
                                                '<tr>' +
                                                  '<th scope="row" id="so_number"></th>' +
                                                  '<td id="so_date"></td>' +
                                                  '<td id="nama_pelanggan"></td>' +
                                                  '<td id="alamat"></td>' +
                                                  '<td id="no_tlp"></td>' +
                                                  '<td id="sub_total"></td>' +
                                                  '<td id="drop_payment"></td>' +
                                                  '<td id="kekurangan"></td>' +
                                                '</tr>' +
                                              '</tbody>' +
                                            '</table>' +
                                          '</div>' +
                                          '<br>'+
                                          '<div class="row">' +
                                            '<div class="form-group col-md-4">' +
                                              '<input type="text" class="form-control" name="number_so" id="number_so" readonly>' +
                                            '</div>' +
                                            '<div class="form-group col-md-4">' +
                                              '<label for="pembayaran">Pembayaran</label>' +
                                              '<input type="text" class="form-control" name="pembayaran" onkeypress="return hanyaAngka(event)" id="pembayaran" required>' +
                                            '</div>' +
                                          '</div>' +
                                          'Jenis Pembayaran :' +
                                          '<br>' +
                                          '<div class="form-check form-check-inline">' +
                                            '<input type="radio" name="tipe_pembayaran" id="tipe_pembayaran_bank" value="bank" required>' +
                                            '<label class="form-check-label" for="tipe_pembayaran_bank">Bank</label>' +
                                          '</div>' +
                                          '<div class="form-check form-check-inline">' +
                                            '<input type="radio" name="tipe_pembayaran" id="tipe_pembayaran_cash" value="cash" required>' +
                                            '<label class="form-check-label" for="tipe_pembayaran_cash">Cash</label>' +
                                          '</div>' +
                                          '<br>' +
                                          '<button type="submit" name="submitPemasukan" id="submitPemasukan" style="visibility: hidden;" class="btn btn-primary">Submit</button>');

                    for (var i = 0; i < data.length; i++) {

                      if (nmr_so == data[i].so_number) {
                        $('#so_number').html(data[i].so_number);
                        $('#so_date').html(data[i].so_date);
                        $('#nama_pelanggan').html(data[i].nama_pelanggan);
                        $('#alamat').html(data[i].alamat);
                        $('#no_tlp').html(data[i].no_tlp);
                        $('#sub_total').html(data[i].sub_total);
                        $('#drop_payment').html(data[i].drop_payment);
                        $('#kekurangan').html(data[i].kekurangan);
                        $('#number_so').val(data[i].so_number);

                        break;
                      }
                    }
                    $('#pembayaran').mask("000.0000.000.000", {reverse:true});

                    $('#pembayaran').on('keyup', function(){
                      var temp_pembayaran = $(this).val();
                      var arr1 = temp_pembayaran.split(".");
                      var pembayaran = "";

                        for (var i = 0; i < arr1.length; i++) {
                          pembayaran += arr1[i]
                        }

                        $kekurangan = parseInt($('#kekurangan').html());

                        if (pembayaran > $kekurangan) {
                          $('#submitPemasukan').css('visibility', 'hidden');
                        }else{
                          $('#submitPemasukan').css('visibility', 'visible');
                        }
                        
                  });

                 }
              });

              }
            });

            // jQuery Proses Delete Order
            $("[name=delete-karyawan]").click(function(){
              var element = $(this);
              var id = element.attr("id");
              var isi = 'id=' + id;
                if(confirm('Are you sure delete this data?'))
                {
                  $.ajax({
                  type: 'POST',
                  url: "<?php echo base_url('data-karyawan/hapus-karyawan'); ?>",
                  data: isi,
                    success: function() {
                      location.href = "<?php echo base_url('data-karyawan');?>";
                    }
                });
              }
            });

            //function validasi pengeluaran : pinjaman cashbon
            $("#atas_nama").on('change',function(){
              if ($('#atas_nama').val() == 'nol') {
                $('#tombol-pinjam-cashbon').css('visibility', 'hidden');
              }else{
                $('#tombol-pinjam-cashbon').css('visibility', 'visible');

                $.ajax({
                type: 'ajax',
                url: "<?php echo site_url('pengeluaran/data_cashbon');?>",
                ansync : false,
                dataType: 'json',
                 success: function(data) {
                  for (var i = 0; i < data.length; i++) {

                      if ($('#atas_nama').val() == data[i].id) {
                        var saldo_pinjaman = data[i].saldo_pinjaman;
                        break;
                      }
                    }
                    $('#jmlh_pinjaman').on('keyup', function () {
                      var temp_pinjaman = $(this).val();
                      var arr1 = temp_pinjaman.split(".");
                      var jmlh_pinjaman = "";

                        for (var i = 0; i < arr1.length; i++) {
                          jmlh_pinjaman += arr1[i];
                        }
                        var s = parseInt(saldo_pinjaman) * 1;
                        var j = parseInt(jmlh_pinjaman) * 1;
                      if (j > s) {
                        $('#ket_pinjaman').html('<h6 style="color: red;">Maksimal Pinjaman : Rp. '+saldo_pinjaman+'</h6>');
                        $('#tombol-pinjam-cashbon').css('visibility', 'hidden');
                      }else{
                        $('#ket_pinjaman').html('<h6 style="color: green;">Nominal Pinjaman Sesuai.</h6>');
                        $('#tombol-pinjam-cashbon').css('visibility', 'visible');
                      }
                    });
                 }

               });


              }
            });

            //function validasi pemasukan : pembayaran cashbon
            $("#atas_nama_pembayar").on('change',function(){
              if ($('#atas_nama_pembayar').val() == 'nol') {
                $('#tombol-bayar-cashbon').css('visibility', 'hidden');
              }else{
                $('#tombol-bayar-cashbon').css('visibility', 'visible');

                $.ajax({
                type: 'ajax',
                url: "<?php echo site_url('pemasukan/data_cashbon');?>",
                ansync : false,
                dataType: 'json',
                success: function(data) {
                  for (var i = 0; i < data.length; i++) {

                      if ($('#atas_nama_pembayar').val() == data[i].id) {
                        var saldo_pinjaman = data[i].saldo_pinjaman;
                        break;
                      }
                    }
                    var kekurangan = parseInt(500000) - parseInt(saldo_pinjaman);
                    $('#ket_bayar').html('<h6 style="color: green;">Nominal yang harus dibayar Rp. '+kekurangan+'</h6>');
                    $('#jmlh_pembayaran').on('keyup', function () {
                      var temp_bayar = $(this).val();
                      var arr1 = temp_bayar.split(".");
                      var jmlh_pembayaran = "";

                        for (var i = 0; i < arr1.length; i++) {
                          jmlh_pembayaran += arr1[i];
                        }
                        var s = parseInt(saldo_pinjaman) * 1;
                        var j = parseInt(jmlh_pembayaran) * 1;
                      if (j > s) {
                        $('#tombol-bayar-cashbon').css('visibility', 'hidden');
                      }else{
                        $('#tombol-bayar-cashbon').css('visibility', 'visible');
                      }
                    });
                 }

               });


              }
            });

            $("#jenis_simpanan").on('change',function(){
              if ($('#jenis_simpanan').val() == 'nol') {
                $('#tombol-saldo-awal').css('visibility', 'hidden');
              }else{
                $('#tombol-saldo-awal').css('visibility', 'visible');
              }
            }); 

            //function untuk menampilkan pesan order berhasil
            $('#Modal-success').modal('show');

            $('#Modal-success').on('hide.bs.modal', function () {
              location.href = "<?php echo base_url('sales-order/cetak-so');?>";
            });

            $('#Modal-success-p').modal('show');

            $('#Modal-success-p').on('hide.bs.modal', function () {
              location.href = "<?php echo site_url('order_prioritas/cetak_so');?>";
            });

            $("[name=update-karyawan]").click(function(){
              var id = $(this).attr("id");

              $.ajax({
                url : "<?php echo site_url('data_karyawan/get_data_karyawan/')?>/" + id,
                type: "GET",
                dataType: "JSON",
                success: function(data) {
                  for (var i = 0; i < data.length; i++) {
                      var nama = data[i].nama_karyawan;
                      var alamat = data[i].alamat;
                      var no_tlp = data[i].no_tlp;
                      var username = data[i].username; 
                      var pass = data[i].password; 
                 }
                 $('#u_id').val(id);
                 $('#u_nama_karyawan').val(nama);
                 $('#u_alamat').val(alamat);
                 $('#u_no_tlp').val(no_tlp);
                 $('#u_username').val(username);
                 $('#u_pass_lama').val(pass);
                 $('#u_pass_baru').val(pass);
                 $('#u_confrim_pass_baru').val(pass);

                 $('#u_pass_lama').on('focusout', function () {
                   if ($('#u_pass_lama').val() != pass) {
                    $('#error_pass_lama').html('Password Tidak Sama!');
                    $('[name=tombol-update-karyawan]').attr('disabled', true);
                   }else{
                    $('#error_pass_lama').html('');
                    $('[name=tombol-update-karyawan]').attr('disabled', false);
                   }
                 });

                }
               });

            });

            $("[name=tombol-update-karyawan]").click(function(){
              if ($('#u_pass_baru').val() != $('#u_confrim_pass_baru').val() ){
                $('#error_confrim_pass_lama').html('Password Tidak Sama!');
              }else{
                var data = $('#u_karyawan').serialize();
                $.ajax({
                  type: 'POST',
                  url: "<?php echo site_url('data_karyawan/update_data_karyawan');?>",
                  data: data,
                  success: function() {
                    location.href = "<?php echo site_url('data_karyawan');?>";
                  }
                });

              }
            });

            //autocomplete data pelanggan
            $('#nama_pelanggan').autocomplete({
                // serviceUrl berisi URL ke controller/fungsi yang menangani request kita
                serviceUrl: '<?php echo site_url('sales_order/autocomplete'); ?>',
                // fungsi ini akan dijalankan ketika user memilih salah satu hasil request
                onSelect: function (suggestion) {
                    $('#alamat_pelanggan').val(''+suggestion.alamat); // membuat id 'v_nim' untuk ditampilkan
                    $('#telfon_pelanggan').val(''+suggestion.telfon); // membuat id 'v_jurusan' untuk ditampilkan
                }
            });

            //autocomplete data perusahhan
            $('#nama_supplier').autocomplete({
                // serviceUrl berisi URL ke controller/fungsi yang menangani request kita
                serviceUrl: '<?php echo site_url('purchase_order/autocomplete'); ?>',
                // fungsi ini akan dijalankan ketika user memilih salah satu hasil request
                onSelect: function (suggestion) {
                    $('#alamat_supplier').val(''+suggestion.alamat); // membuat id 'v_nim' untuk ditampilkan
                    $('#telfon_supplier').val(''+suggestion.telfon); // membuat id 'v_jurusan' untuk ditampilkan
                }
            });


    });     

          //function hanya input angka
          function hanyaAngka(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
             if (charCode > 31 && (charCode < 48 || charCode > 57))
       
              return false;
            return true;
          }
</script>
</body>

</html>