<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengeluaran extends CI_Controller {

	public function __construct(){
		parent::__construct();

		$this->load->model('pengeluaran_model');
		$this->load->model('data_karyawan_model');
		$this->load->library('form_validation');

		if ($this->session->userdata('status') != "login") {
			redirect('auth');
		}
	}

	public function index()
	{
		$data['title']='Pengeluaran | Noname.com';
		$data_penegluaran = array(
									'data_karyawan' => $this->data_karyawan_model->getData_cashbon()->result(),
									'saldo_gopay' => $this->pengeluaran_model->getSaldo_gopay()->result() 
								);

		$this->load->view('templates/header', $data);
		$this->load->view('pengeluaran', $data_penegluaran);
		$this->load->view('templates/footer');
	}

	public function data_cashbon()
	{
		$data = $this->data_karyawan_model->getData_cashbon()->result();
		echo json_encode($data);
	}

	public function proses_insert()
	{	
		$this->pengeluaran_model->insertPengeluaran();
		redirect('pengeluaran');
	}

	public function peminjaman_cashbon()
	{
		$this->pengeluaran_model->peminjaman_cashbon();
		redirect('pengeluaran');
	}

	public function insentif()
	{
		$this->pengeluaran_model->insentif();
		redirect('pengeluaran');
	}

	public function gopay()
	{
		$this->pengeluaran_model->gopay();
		redirect('pengeluaran');
	}

	public function get_saldo_gopay()
	{
		$data = $this->pengeluaran_model->getSaldo_gopay()->result();
		echo json_encode($data);
	}


}
