                        <div class="table-responsive">
                          <table class="table">
                            <thead>
                              <tr>
                                <th scope="col"><b>No</b></th>
                                <th scope="col"><b>Deskripsi Barang</b></th>
                                <th scope="col"><b>Qty</b></th>
                                <th scope="col"><b>Sat</b></th>
                                <th scope="col"><b>Deskripsi Pekerjaan</b></th>
                                <th scope="col"><b>Harga</b></th>
                                <th scope="col"><b>Diskon</b></th>
                                <th scope="col"><b>Total</b></th>
                                <th scope="col"><b>Keterangan</b></th>
                                <th scope="col"><b></b></th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php
                              $no = 1;
                              foreach ($temp_sales_order as $row) {
                              ?>
                              <tr>
                                <th><?php echo $no++ ?></th>
                                <td><?php echo $row->des_barang ?></td>
                                <td><?php echo $row->qty ?></td>
                                <td><?php echo $row->sat ?></td>
                                <td><?php echo $row->des_pekerjaan ?></td>
                                <td><?php echo $row->harga ?></td>
                                <td><?php echo $row->diskon ?></td>
                                <td id="total"><?php echo $row->total ?></td>
                                <td><?php echo $row->ket ?></td>
                                <td><button class='btn btn-info' name="tombol-edit" id='<?php echo $row->id ?>'>Edit</button><button class='btn btn-danger' name="tes" id='<?php echo $row->id ?>'>Delete</button></td>
                              </tr>
                              <?php
                            }
                            ?>
                            </tbody>
                          </table>
                        </div>
                        <!-- Modal -->
                        <div class="modal fade" id="edit-orderModal" tabindex="-1" role="dialog" aria-labelledby="orderModal" aria-hidden="true">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Order Barang</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                              <div class="modal-body">
                                <form method="post" id="form-edit-order">
                                    <div class="row">
                                       <div class="table-responsive">
                                        <table class="table">
                                            <tr>
                                              <th>Deskripsi Barang :</th>
                                              <th><input type="text" class="form-control" name="des_barang" id="e_des_barang" placeholder="Deskripsi Barang"></th>
                                            </tr>
                                            <tr>
                                              <th>Qty :</th>
                                              <td><input type="text" class="form-control" name="qty_barang" id="e_qty_barang" onkeypress="return hanyaAngka(event)" placeholder="Qty"></td>
                                            </tr>
                                            <tr>
                                              <th>Satuan :</th>
                                              <td><input type="text" class="form-control" name="sat_barang" id="e_sat_barang" placeholder="Sat"></td>
                                            </tr>
                                            <tr>
                                              <th>Deskripsi Pekerjaan :</th>
                                              <td><input type="text" class="form-control" name="des_pekerjaan" id="e_des_pekerjaan" placeholder="Deskripsi Pekerjaan"></td>
                                            </tr>
                                            <tr>
                                              <th>Harga :</th>
                                              <td><input type="text" class="form-control" name="harga_barang" id="e_harga_barang" onkeypress="return hanyaAngka(event)" placeholder="Harga"></td>
                                            </tr>
                                            <tr>
                                              <th>Diskon :</th>
                                              <td><input type="text" class="form-control" name="diskon_barang" id="e_diskon_barang" onkeypress="return hanyaAngka(event)" placeholder="Diskon"></td>
                                            </tr>
                                            <tr>
                                              <th>Total :</th>
                                              <td><input type="text" class="form-control" name="total_harga" id="e_total_harga" placeholder="Total" readonly></td>
                                            </tr>
                                            <tr>
                                              <th>Keterangan :</th>
                                              <td><input type="text" class="form-control" name="ket" id="e_ket" placeholder="Keterangan"></td>
                                            </tr>
                                        </table>
                                      </div>
                                  </div>
                              </div>
                            </form>
                               <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary" data-dismiss="modal" id="submit-edit">Update</button> 
                            </div>
                          </div>
                        </div>
                        <script type="text/javascript">
                        $(document).ready(function(){

                           // jQuery Proses Delete Order
                            $("[name=tes]").click(function(){
                              var element = $(this);
                              var id = element.attr("id");
                              var isi = 'id=' + id;
                              if(confirm('Are you sure delete this data?'))
                              {
                               $.ajax({
                                type: 'POST',
                                url: "<?php echo site_url('order_prioritas/deleteTemp');?>",
                                data: isi,
                                success: function() {
                                  $('#dp').val('');
                                  $('#kekurangan').val('');
                                  $('#tabel-temp').load("<?php echo site_url('order_prioritas/data_temp_order');?>");
                                  $.ajax({
                                    type: 'ajax',
                                    url: "<?php echo site_url('order_prioritas/proses_ambil_total');?>",
                                    ansync : false,
                                    dataType: 'json',
                                    success: function(data) {
                                      var tot = 0;
                                      for (var i = 0; i < data.length; i++) {
                                       var temp = data[i].total;
                                       tot = parseInt(tot) + parseInt(temp);
                                      }
                                      $('#sub_total').val(tot);
                                      if (tot == 0) {
                                        $('#submitOrder').css("visibility", "hidden");
                                      }
                                    }
                                  });
                                }
                              });
                            }

                            });

                            // jQuery Proses Delete Order
                            $("[name=tombol-edit]").click(function(){
                              var element = $(this);
                              var id = element.attr("id");
                              var isi = 'id=' + id;

                               $.ajax({
                                url : "<?php echo site_url('order_prioritas/get_temp_order/')?>/" + id,
                                type: "GET",
                                dataType: "JSON",
                                success: function(data) {
                                  $('#form-edit-order')[0].reset(); // reset form on modals
                                  $('#edit-orderModal').modal('show');
                                  for (var i = 0; i < data.length; i++) {
                                      var des_barang = data[i].des_barang;
                                      var qty = data[i].qty;
                                      var sat = data[i].sat;
                                      var des_pekerjaan = data[i].des_pekerjaan; 
                                      var harga = data[i].harga;
                                      var diskon = data[i].diskon;
                                      var total = data[i].total;
                                      var ket = data[i].ket;  
                                 }
                                 $('#e_des_barang').val(des_barang);
                                 $('#e_qty_barang').val(qty);
                                 $('#e_sat_barang').val(sat);
                                 $('#e_des_pekerjaan').val(des_pekerjaan);
                                 $('#e_harga_barang').val(harga);
                                 $('#e_diskon_barang').val(diskon);
                                 $('#e_total_harga').val(total);
                                 $('#e_ket').val(ket);

                                   $('#e_harga_barang').on('keyup', function(){
                                    var qtyBarang = $('#e_qty_barang').val();
                                    var diskonBarang = $('#e_diskon_barang').val();
                                    var arr = diskonBarang.split("%");
                                    var diskon_temp = "";

                                      for (var i = 0; i < arr.length; i++) {
                                        diskon_temp += arr[i]
                                      }
                                    var temp_hargaBarang = $('#e_harga_barang').val();
                                    var arr2 = temp_hargaBarang.split(".");
                                    var hargaBarang = "";

                                      for (var i = 0; i < arr2.length; i++) {
                                        hargaBarang += arr2[i]
                                      }

                                      if (qtyBarang === "") {
                                        $('#e_total_harga').val(hargaBarang);
                                      }else{
                                        var totalHarga = qtyBarang * hargaBarang;
                                        totalHarga = totalHarga - (totalHarga * (diskon_temp / 100));
                                        $('#e_total_harga').val(totalHarga); 
                                      }
                                });

                                  $('#e_qty_barang').on('keyup', function(){
                                    var qtyBarang = $('#e_qty_barang').val();
                                    var diskonBarang = $('#e_diskon_barang').val();
                                    var arr = diskonBarang.split("%");
                                    var diskon_temp = "";

                                      for (var i = 0; i < arr.length; i++) {
                                        diskon_temp += arr[i]
                                      }
                                    var temp_hargaBarang = $('#e_harga_barang').val();
                                    var arr2 = temp_hargaBarang.split(".");
                                    var hargaBarang = "";

                                      for (var i = 0; i < arr2.length; i++) {
                                        hargaBarang += arr2[i]
                                      }

                                      if (qtyBarang === "") {
                                        $('#e_total_harga').val(hargaBarang);
                                      }else{
                                        var totalHarga = qtyBarang * hargaBarang;
                                        totalHarga = totalHarga - (totalHarga * (diskon_temp / 100));
                                        $('#e_total_harga').val(totalHarga); 
                                      }
                                });

                                  $('#e_diskon_barang').on('focusout', function(){
                                    var diskonBarang = $('#e_diskon_barang').val();
                                    var totHarga = $('#e_total_harga').val();

                                    var arr = diskonBarang.split("%");
                                    var diskon_temp = "";

                                      for (var i = 0; i < arr.length; i++) {
                                        diskon_temp += arr[i]
                                      }
                                    
                                    var diskon = totHarga * (diskon_temp / 100);
                                    var grandDiskon = totHarga - diskon;

                                    var persen = diskonBarang + "%";

                                    $('#e_diskon_barang').val(persen);
                                    $('#e_total_harga').val(grandDiskon);
                                    
                                });

                                  $('#e_diskon_barang').on('keydown', function(){
                                    var qtyBarang = $('#e_qty_barang').val();
                                    var temp_hargaBarang = $('#e_harga_barang').val();
                                    var arr = temp_hargaBarang.split(".");
                                    var hargaBarang = "";

                                      for (var i = 0; i < arr.length; i++) {
                                        hargaBarang += arr[i]
                                      }

                                      if (qtyBarang === "") {
                                        $('#e_total_harga').val(hargaBarang);
                                      }else{
                                        var totalHarga = qtyBarang * hargaBarang;
                                      $('#e_total_harga').val(totalHarga);
                                      }
                                    
                                });

                                  $('#submit-edit').click(function () {
                                    var data = $('#form-edit-order').serialize();
                                    $.ajax({
                                      type: 'POST',
                                      url: "<?php echo site_url('order_prioritas/edit_temp_order/');?>/" + id,
                                      data: data,
                                      success: function() {
                                        $('#tabel-temp').load("<?php echo site_url('order_prioritas/data_temp_order');?>");
                                        $.ajax({
                                          type: 'ajax',
                                          url: "<?php echo site_url('order_prioritas/proses_ambil_total');?>",
                                          ansync : false,
                                          dataType: 'json',
                                          success: function(data) {
                                            var tot = 0;
                                            for (var i = 0; i < data.length; i++) {
                                             var temp = data[i].total;
                                             tot = parseInt(tot) + parseInt(temp);
                                            }
                                            $('#sub_total').val(tot);
                                            $('#submitOrder').css("visibility", "visible");
                                          }
                                        });
                                      }
                                    });
                                  });

                                }
                              });

                            });

                         });
                        </script>
