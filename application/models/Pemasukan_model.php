<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pemasukan_model extends CI_Model{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	public function getData_so()
	{
		return $this->db->get_where('data_so', array('ket' => 'Belum Lunas'));
	}

	public function insertPemasukan()
	{	
		$pembayaran = $this->input->post('pembayaran');
		$so_number = $this->input->post('number_so');
		$tipe_pembayaran = $this->input->post('tipe_pembayaran');

		$dataSO = $this->db->get_where('data_so', array('so_number' => $so_number))->result();
		foreach ($dataSO as $row) {
			$kekurangan = $row->kekurangan;
			$nama_pelanggan = $row->nama_pelanggan;
			$saldo_so = $row->saldo;
		}

		$new_saldo_so = preg_replace('/[^A-Za-z0-9\  ]/', '', $saldo_so) + preg_replace('/[^A-Za-z0-9\  ]/', '', $pembayaran);
		$Newkekurangan = $kekurangan - preg_replace('/[^A-Za-z0-9\  ]/', '', $pembayaran);

		if ($Newkekurangan <= 0) {
			$ket = 'Lunas';
			$item = 'Pelunasan Order atas nama ';
		}else{
			$ket = 'Belum Lunas';
			$item = 'Pembayaran angsuran Order atas nama ';
		}

		if ($tipe_pembayaran == 'bank') {
			$saldo_bank = $this->db->get('cashflow_bank')->result();

			foreach ($saldo_bank as $bank) {
				$saldo_bank = $bank->saldo;
			}

			$now_saldo = $saldo_bank + preg_replace('/[^A-Za-z0-9\  ]/', '', $pembayaran);
			$isi_saldo = array(
								'tanggal' => date('d/m/y'),
								'item' => $item.$nama_pelanggan,
								'pengeluaran' => ' ',
								'pemasukan' => $pembayaran,
								'saldo' => $now_saldo
							);

			$this->db->insert('cashflow_bank', $isi_saldo);

		}else{
			$saldo_cash = $this->db->get('cashflow_cash')->result();

			foreach ($saldo_cash as $cash) {
				$saldo_cash = $cash->saldo;
			}

			$now_saldo = $saldo_cash + preg_replace('/[^A-Za-z0-9\  ]/', '', $pembayaran);
			$isi_saldo = array(
								'tanggal' => date('d/m/y'),
								'item' => $item.$nama_pelanggan,
								'pengeluaran' => ' ',
								'pemasukan' => $pembayaran,
								'saldo' => $now_saldo
							);

			$this->db->insert('cashflow_cash', $isi_saldo);
		}
 
		$data = array(
			'kekurangan' => $Newkekurangan,
			'pelunasan' => $pembayaran,
			'saldo' => $new_saldo_so,
			'ket' => $ket
		);

		$this->db->where('so_number', $so_number);
		$this->db->update('data_so', $data);

		$get_pendapatan = $this->db->get('pendapatan_so')->result();

		foreach ($get_pendapatan as $row) {
			$jumlah_pendapatan = $row->jumlah;
		}

		$new_jumlah_pendapatan = $jumlah_pendapatan + preg_replace('/[^A-Za-z0-9\  ]/', '', $pembayaran);
		$update_pendapatan = array('jumlah' => $new_jumlah_pendapatan);
		$this->db->update('pendapatan_so', $update_pendapatan);
	}

	public function pembayaran_cashbon()
	{	
		$id = $this->input->post('atas_nama');
		$tipe_pembayaran = $this->input->post('tipe_pembayaran');

		$data_karyawan = $this->db->get_where('tabel_karyawan', array('id' => $id))->result();

		foreach ($data_karyawan as $row) {
			$nama_karyawan = $row->nama_karyawan;
		}

		$item = 'Pembayaran Cash Bon '.$nama_karyawan;
		
		$jmlh_pembayaran = preg_replace('/[^A-Za-z0-9\  ]/', '', $this->input->post('jmlh_pembayaran'));

		if ($tipe_pembayaran == 'bank') {
			$saldo_bank = $this->db->get('cashflow_bank')->result();

			foreach ($saldo_bank as $bank) {
				$saldo_bank = $bank->saldo;
			}

			$now_saldo = $saldo_bank + preg_replace('/[^A-Za-z0-9\  ]/', '', $jmlh_pembayaran);
			$isi_saldo = array(
								'tanggal' => date('d/m/y'),
								'item' => $item,
								'pengeluaran' => ' ',
								'pemasukan' => $jmlh_pembayaran,
								'saldo' => $now_saldo
							);

			$this->db->insert('cashflow_bank', $isi_saldo);

		}else{
			$saldo_cash = $this->db->get('cashflow_cash')->result();

			foreach ($saldo_cash as $cash) {
				$saldo_cash = $cash->saldo;
			}

			$now_saldo = $saldo_cash + preg_replace('/[^A-Za-z0-9\  ]/', '', $jmlh_pembayaran);
			$isi_saldo = array(
								'tanggal' => date('d/m/y'),
								'item' => $item,
								'pengeluaran' => ' ',
								'pemasukan' => $jmlh_pembayaran,
								'saldo' => $now_saldo
							);

			$this->db->insert('cashflow_cash', $isi_saldo);
		}

		$data_sisa_pinjaman = $this->db->get_where('cashflow_cash_bon', array('id' => $id))->result();

		if (count($data_sisa_pinjaman) > 0) {
			foreach ($data_sisa_pinjaman as $row) {
			$sisa_pinjaman = $row->sisa_pinjaman;
			}
			$new_sisa_pinjaman = preg_replace('/[^A-Za-z0-9\  ]/', '', $sisa_pinjaman) - preg_replace('/[^A-Za-z0-9\  ]/', '', $jmlh_pembayaran);
		}else{
			$new_sisa_pinjaman = $jmlh_pembayaran;
		}

		$data = array(
			'tanggal' => date('d/m/y'),
			'id' => $id,
			'nama_karyawan' => $nama_karyawan,
			'pinjaman' => ' ',
			'pengembalian' => $jmlh_pembayaran,
			'sisa_pinjaman' => $new_sisa_pinjaman
		);

		$this->db->insert('cashflow_cash_bon', $data);
	}

	public function drop_cash()
	{
		$jmlh_pemasukan = $this->input->post('jmlh_drop_cash');

		$saldo_bank = $this->db->get('cashflow_bank')->result();

			foreach ($saldo_bank as $bank) {
				$saldo_bank = $bank->saldo;
			}

			$now_saldo_bank = $saldo_bank - preg_replace('/[^A-Za-z0-9\  ]/', '', $jmlh_pemasukan);
			$isi_saldo_bank = array(
								'tanggal' => date('d/m/y'),
								'item' => 'Pengambilan Drop Cash',
								'pengeluaran' => $jmlh_pemasukan,
								'pemasukan' => ' ',
								'saldo' => $now_saldo_bank
							);

			$this->db->insert('cashflow_bank', $isi_saldo_bank);

			$saldo_cash = $this->db->get('cashflow_cash')->result();

			if (count($saldo_cash) > 0) {

				foreach ($saldo_cash as $cash) {
				$saldo_cash = $cash->saldo;
				}

				$now_saldo = $saldo_cash + preg_replace('/[^A-Za-z0-9\  ]/', '', $jmlh_pemasukan);

			}else{
				$now_saldo = $jmlh_pemasukan;
			}

			
			$isi_saldo_cash = array(
								'tanggal' => date('d/m/y'),
								'item' => 'Drop Cash',
								'pengeluaran' => ' ',
								'pemasukan' => $jmlh_pemasukan,
								'saldo' => $now_saldo
							);

			$this->db->insert('cashflow_cash', $isi_saldo_cash);

			$saldo_awal = $this->db->get_where('saldo_awal', array('item' => 'Saldo Cash'))->result();

			foreach ($saldo_awal as $row) {

				if ($row->pemasukan == 0) {

					$saldo_awal_bank = $this->db->get_where('saldo_awal', array('item' => 'Saldo Bank'))->result();

					foreach ($saldo_awal_bank as $key) {
						$sb = $key->pemasukan - preg_replace('/[^A-Za-z0-9\  ]/', '', $jmlh_pemasukan);
					}

					$data2 = array('pemasukan' => $sb);
					$this->db->where('item', 'Saldo Bank');
					$this->db->update('saldo_awal', $data2);

					$sc = preg_replace('/[^A-Za-z0-9\  ]/', '', $jmlh_pemasukan);

					$data = array('pemasukan' => $sc);

					$this->db->where('item', 'Saldo Cash');
					$this->db->update('saldo_awal', $data);
				}
			}

	}

	public function bunga_admin_bank()
	{
		$jmlh_pemasukan_bank = $this->input->post('jmlh_pemasukan_bank');
		$tipe_pemasukan = $this->input->post('tipe_pemasukan');

		if ($tipe_pemasukan == 'admin_bank') {
			$item = 'Admin Bank';
			$where = array('item' => 'Admin Bank');

			$saldo_awal = $this->db->get_where('saldo_awal', $where)->result();

			foreach ($saldo_awal as $row) {
				$admin_bank = $row->pemasukan;
			}

			$saldo_admin_bank = preg_replace('/[^A-Za-z0-9\  ]/', '', $admin_bank) + preg_replace('/[^A-Za-z0-9\  ]/', '', $jmlh_pemasukan_bank);

			$data = array(	'tanggal' => date('d/m/y'),
							'pemasukan' => $saldo_admin_bank
						);

			$this->db->where($where);
			$this->db->update('saldo_awal', $data);

		}else{
			$item = 'Bunga Bank';
			$where = array('item' => 'Bunga Bank');

			$saldo_awal = $this->db->get_where('saldo_awal', $where)->result();

			foreach ($saldo_awal as $row) {
				$bunga_bank = $row->pemasukan;
			}

			$saldo_bunga_bank = preg_replace('/[^A-Za-z0-9\  ]/', '', $bunga_bank) + preg_replace('/[^A-Za-z0-9\  ]/', '', $jmlh_pemasukan_bank);

			$data = array(	'tanggal' => date('d/m/y'),
							'pemasukan' => $saldo_bunga_bank
						);

			$this->db->where($where);
			$this->db->update('saldo_awal', $data);
		}

		$saldo_bank = $this->db->get('cashflow_bank')->result();

			foreach ($saldo_bank as $bank) {
				$saldo_bank = $bank->saldo;
			}

			$now_saldo = $saldo_bank + preg_replace('/[^A-Za-z0-9\  ]/', '', $jmlh_pemasukan_bank);
			$isi_saldo = array(
								'tanggal' => date('d/m/y'),
								'item' => $item,
								'pengeluaran' => ' ',
								'pemasukan' => $jmlh_pemasukan_bank,
								'saldo' => $now_saldo
							);

			$this->db->insert('cashflow_bank', $isi_saldo);

	}

	
}
?>