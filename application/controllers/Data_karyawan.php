<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_karyawan extends CI_Controller {

	public function __construct(){
		parent::__construct();

		$this->load->model('data_karyawan_model');
		$this->load->library('form_validation');

		if ($this->session->userdata('status') != "login") {
			redirect('auth');
		}
	}

	public function index()
	{
		$data['title']='Data Karyawan | Noname.com';
		$data_karyawan['data_karyawan'] = $this->data_karyawan_model->getData()->result();

		$this->load->view('templates/header', $data);
		$this->load->view('data_karyawan', $data_karyawan);
		$this->load->view('templates/footer');
	}

	public function tambah_karyawan()
	{
		$this->data_karyawan_model->tambah_karyawan();
		redirect('data-karyawan');
	}

	public function hapus_karyawan()
	{
		$this->data_karyawan_model->hapus_karyawan();
		redirect('data-karyawan');
	}

	public function get_data_karyawan($id)
	{
		$data = $this->data_karyawan_model->getData_id($id);
		echo json_encode($data);
	}

	public function update_data_karyawan()
	{
		$this->data_karyawan_model->update_data_karyawan();
		redirect('data-karyawan');
	}

}
