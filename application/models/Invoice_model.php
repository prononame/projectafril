<?php
class Invoice_model extends CI_Model{

   function product_list(){

		return $this->db->get_where('data_so', array('ket' => 'Belum Lunas'))->result();

	}

	public function getData_so($id){
   		
    	$data = array('so_number' => $id);
        return $this->db->get_where('data_so', $data);
    }

    public function getData_po($id){
    	
    	$data = array('so_number' => $id);
        return $this->db->get_where('data_po', $data);
    }

 	public function getData_orders(){

    	
        return $this->db->get('data_order')->result();
    }
    public function getData_order($id){

        $data = array('so_number' => $id);
        return $this->db->get_where('data_order' , $data);
    }

     public function getNo_invoice(){

       $so = $this->input->post('stat');
         $data_so = $this->db->get_where('data_so', array('so_number' => $so ))->result();
        foreach ($data_so as $row) {
            $update_status = $row->nmr_invoice;
        }

        if ($update_status == '' or $update_status == 'IN-'.$so) {
            $status = 'IN-'.$so;
        }

        $where = array('so_number' => $so );
        $newstatus = array('nmr_invoice' => $status );

        $this->db->set($newstatus);
        $this->db->where($where);
        $this->db->update('data_so');
    
    }
}
?>