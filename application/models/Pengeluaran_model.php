<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengeluaran_model extends CI_Model{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	public function insertPengeluaran()
	{	
		$type = $this->input->post('type');
		$tanggal = $this->input->post('tanggal');
		$item = $this->input->post('item');
		$pengeluaran = $this->input->post('pengeluaran');
		$tipe_pembayaran = $this->input->post('tipe_pembayaran');
		$ket = $this->input->post('ket');

		if ($type == 'tabel_gojek') {

			$get_saldo_gopay = $this->db->get('saldo_gojek')->result();

			foreach ($get_saldo_gopay as $row) {
				$jumlah_saldo = $row->jumlah;
			}

			$new_jumlah_saldo = $jumlah_saldo - preg_replace('/[^A-Za-z0-9\  ]/', '', $pengeluaran);

			$update_saldo_gojek = array('jumlah' => $new_jumlah_saldo);
			$this->db->update('saldo_gojek', $update_saldo_gojek);

			$data = array(
							'tanggal' => $tanggal,
							'item' => $item,
							'pengeluaran' => $pengeluaran,
							'ket' => $ket
						);

			$this->db->insert($type, $data);

		}else{

			if ($tipe_pembayaran == 'bank') {
				$saldo_bank = $this->db->get('cashflow_bank')->result();

				foreach ($saldo_bank as $bank) {
					$saldo_bank = $bank->saldo;
				}

				$now_saldo = $saldo_bank - preg_replace('/[^A-Za-z0-9\  ]/', '', $pengeluaran);
				$isi_saldo = array(
									'tanggal' => date('d/m/y'),
									'item' => $item,
									'pengeluaran' => $pengeluaran,
									'pemasukan' => ' ',
									'saldo' => $now_saldo
								);

				$this->db->insert('cashflow_bank', $isi_saldo);

			}else{
				$saldo_cash = $this->db->get('cashflow_cash')->result();

				foreach ($saldo_cash as $cash) {
					$saldo_cash = $cash->saldo;
				}

				$now_saldo = $saldo_cash - preg_replace('/[^A-Za-z0-9\  ]/', '', $pengeluaran);
				$isi_saldo = array(
									'tanggal' => date('d/m/y'),
									'item' => $item,
									'pengeluaran' => $pengeluaran,
									'pemasukan' => ' ',
									'saldo' => $now_saldo
								);

				$this->db->insert('cashflow_cash', $isi_saldo);
			}

			$data = array(
				'tanggal' => $tanggal,
				'item' => $item,
				'pengeluaran' => $pengeluaran,
				'ket' => $ket

			);

			$this->db->insert($type, $data);

		}
	}

	public function peminjaman_cashbon()
	{	
		$id = $this->input->post('atas_nama');
		$tipe_pembayaran = $this->input->post('tipe_pembayaran');

		$data_karyawan = $this->db->get_where('tabel_karyawan', array('id' => $id))->result();

		foreach ($data_karyawan as $row) {
			$nama_karyawan = $row->nama_karyawan;
		}

		$item = 'Cash Bon '.$nama_karyawan;
		
		$jmlh_pinjaman = preg_replace('/[^A-Za-z0-9\  ]/', '', $this->input->post('jmlh_pinjaman'));

		if ($tipe_pembayaran == 'bank') {
			$saldo_bank = $this->db->get('cashflow_bank')->result();

			foreach ($saldo_bank as $bank) {
				$saldo_bank = $bank->saldo;
			}

			$now_saldo = $saldo_bank - preg_replace('/[^A-Za-z0-9\  ]/', '', $jmlh_pinjaman);
			$isi_saldo = array(
								'tanggal' => date('d/m/y'),
								'item' => $item,
								'pengeluaran' => $jmlh_pinjaman,
								'pemasukan' => ' ',
								'saldo' => $now_saldo
							);

			$this->db->insert('cashflow_bank', $isi_saldo);

		}else{
			$saldo_cash = $this->db->get('cashflow_cash')->result();

			foreach ($saldo_cash as $cash) {
				$saldo_cash = $cash->saldo;
			}

			$now_saldo = $saldo_cash - preg_replace('/[^A-Za-z0-9\  ]/', '', $jmlh_pinjaman);
			$isi_saldo = array(
								'tanggal' => date('d/m/y'),
								'item' => $item,
								'pengeluaran' => $jmlh_pinjaman,
								'pemasukan' => ' ',
								'saldo' => $now_saldo
							);

			$this->db->insert('cashflow_cash', $isi_saldo);
		}

		$data_sisa_pinjaman = $this->db->get_where('cashflow_cash_bon', array('id' => $id))->result();

		if (count($data_sisa_pinjaman) > 0) {
			foreach ($data_sisa_pinjaman as $row) {
			$sisa_pinjaman = $row->sisa_pinjaman;
			}
			$new_sisa_pinjaman = preg_replace('/[^A-Za-z0-9\  ]/', '', $sisa_pinjaman) + preg_replace('/[^A-Za-z0-9\  ]/', '', $jmlh_pinjaman);
		}else{
			$new_sisa_pinjaman = $jmlh_pinjaman;
		}

		

		$data = array(
			'tanggal' => date('d/m/y'),
			'id' => $id,
			'nama_karyawan' => $nama_karyawan,
			'pinjaman' => $jmlh_pinjaman,
			'pengembalian' => ' ',
			'sisa_pinjaman' => $new_sisa_pinjaman
		);

		$this->db->insert('cashflow_cash_bon', $data);

		$data_cashbon = $this->db->get_where('saldo_cashbon', array('id' => $id))->result();
		foreach ($data_cashbon as $row) {
			$old_saldo = $row->saldo_pinjaman; 
		}

		$new_saldo_cashbon = $old_saldo - preg_replace('/[^A-Za-z0-9\  ]/', '', $jmlh_pinjaman);

		$u_saldo_cashbon = array('saldo_pinjaman' => $new_saldo_cashbon);
		$where = array('id' => $id);

		$this->db->set($u_saldo_cashbon);
		$this->db->where($where);
		$this->db->update('saldo_cashbon');
	}

	public function insentif()
	{	
		$po_number = $this->input->post('po_number');
		$nama_sales = $this->input->post('nama_sales');
		$client = $this->input->post('client');
		$nilai_kontrak = $this->input->post('nilai_kontrak');
		$insentif = $this->input->post('insentif');
		$tipe_pembayaran = $this->input->post('tipe_pembayaran');

		if ($tipe_pembayaran == 'bank') {
			$saldo_bank = $this->db->get('cashflow_bank')->result();

			foreach ($saldo_bank as $bank) {
				$saldo_bank = $bank->saldo;
			}

			$item = 'Pemberian insentif kepada '.$nama_sales;

			$now_saldo = $saldo_bank - preg_replace('/[^A-Za-z0-9\  ]/', '', $insentif);
			$isi_saldo = array(
								'tanggal' => date('d/m/y'),
								'item' => $item,
								'pengeluaran' => $insentif,
								'pemasukan' => ' ',
								'saldo' => $now_saldo
							);

			$this->db->insert('cashflow_bank', $isi_saldo);

		}else{
			$saldo_cash = $this->db->get('cashflow_cash')->result();

			foreach ($saldo_cash as $cash) {
				$saldo_cash = $cash->saldo;
			}

			$item = 'Pemberian insentif kepada '.$nama_sales;

			$now_saldo = $saldo_cash - preg_replace('/[^A-Za-z0-9\  ]/', '', $insentif);
			$isi_saldo = array(
								'tanggal' => date('d/m/y'),
								'item' => $item,
								'pengeluaran' => $insentif,
								'pemasukan' => ' ',
								'saldo' => $now_saldo
							);

			$this->db->insert('cashflow_cash', $isi_saldo);
		}

		$data = array(
			'tanggal' => date('d/m/y'),
			'po_number' => $po_number,
			'sales' => $nama_sales,
			'client' => $client,
			'nilai_kontrak' => $nilai_kontrak,
			'insentif' => $insentif

		);

		$this->db->insert('tabel_insentif', $data);
	}

	public function getSaldo_gopay()
	{
		return $this->db->get('saldo_gojek');
	}

	public function gopay()
	{
		$pembelian_saldo = $this->input->post('jmlh_saldo_gopay');

		$get_saldo_gopay = $this->db->get('saldo_gojek')->result();

		foreach ($get_saldo_gopay as $row) {
			$jumlah_saldo = $row->jumlah;
		}

		$new_jumlah_saldo = $jumlah_saldo + preg_replace('/[^A-Za-z0-9\  ]/', '', $pembelian_saldo);

		$update_saldo_gojek = array('jumlah' => $new_jumlah_saldo);
		$this->db->update('saldo_gojek', $update_saldo_gojek);

		$saldo_bank = $this->db->get('cashflow_bank')->result();

			foreach ($saldo_bank as $bank) {
				$saldo_bank = $bank->saldo;
			}

			$item = 'Isi Saldo Go-Pay';

			$now_saldo = $saldo_bank - preg_replace('/[^A-Za-z0-9\  ]/', '', $pembelian_saldo);
			$isi_saldo = array(
								'tanggal' => date('d/m/y'),
								'item' => $item,
								'pengeluaran' => $pembelian_saldo,
								'pemasukan' => ' ',
								'saldo' => $now_saldo
							);

			$this->db->insert('cashflow_bank', $isi_saldo);


	}
	 
}
?>