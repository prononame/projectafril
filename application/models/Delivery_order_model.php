<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Delivery_order_model extends CI_Model{
	
	function data_list(){

         $this->db->select('*');
         $this->db->from('data_so');
         $this->db->join('data_po','data_po.so_number=data_so.so_number');
         $query = $this->db->get();
         return $query->result();    
	}

    public function update_status(){

        $so = $this->input->post('stat');
         $data_po = $this->db->get_where('data_po', array('so_number' => $so ))->result();
        foreach ($data_po as $row) {
            $update_status = $row->status;
        }

        if ($update_status == 'belum' or $update_status =='' or $update_status =='dikirim') {
            $status = 'dikirim';
        } 

        $where = array('so_number' => $so );
        $newstatus = array('status' => $status );

        $this->db->set($newstatus);
        $this->db->where($where);
        $this->db->update('data_po');
    
    }

	public function getData_so($id){
   		
    	$data = array('so_number' => $id);
        return $this->db->get_where('data_so', $data);
    }

    public function getData_po($id){
    	
    	$data = array('so_number' => $id);
        return $this->db->get_where('data_po', $data);
    }

 	public function getData_order($id){

    	$data = array('so_number' => $id);
        return $this->db->get_where('data_order', $data);
    }
}

?>