 
<div class="wrapper ">
    <div class="sidebar" data-color="purple" data-background-color="white" data-image="<?php echo base_url('assets/img/sidebar-1.jpg'); ?>">
      <div class="logo">
        <a href="#" class="simple-text logo-normal">
          <h6>DOUBLE A - HOUSE OF SUBLIMATION</h6>
        </a>
      </div>
      <div class="sidebar-wrapper">
        <ul class="nav">
          <li class="nav-item ">
            <a class="nav-link" href="<?php echo site_url('sales-order');?>">
              <i class="fa fa-shopping-cart"></i>
              <p>Sales Order</p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo site_url('purchase-order');?>">
              <i class="material-icons">local_offer</i>
              <p>Purchase Order</p>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="<?php echo site_url('invoice');?>">
              <i class="material-icons">content_paste</i>
              <p>Invoice</p>
            </a>
          </li>
          <li class="nav-item  active">
            <a class="nav-link" href="<?php echo base_url('delivery-order');?>">
              <i class="material-icons">local_shipping</i>
              <p>Delivery Order</p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo site_url('laporan-sales');?>">
              <i class="material-icons">content_paste</i>
              <p>Laporan</p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo site_url('pemasukan');?>">
              <i class="material-icons">monetization_on</i>
              <p>Pemasukan</p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo site_url('pengeluaran');?>">
              <i class="material-icons">money_off</i>
              <p>Pengeluaran</p>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <a class="navbar-brand" href="<?php echo base_url(); ?>">Delivery Order</a>
          </div>
         <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">
            <form class="navbar-form">
              <div class="input-group no-border">
                
                  <div class="ripple-container"></div>
                </button>
              </div>
            </form>
            <ul class="navbar-nav">
              <li class="nav-item dropdown">
                <a class="nav-link" href="#pablo" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">account_circle</i>
                  <p class="d-lg-none d-md-block">
                    Account
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <a class="dropdown-item" href="#"><?php echo $this->session->userdata('nama') ?></br></a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php echo site_url('/logout') ?>">Log out</a>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->
        <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-lg-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title" style="text-align: center;"><b>Delivery Order</h4>
                  <p class="card-category" style="text-align: center;"><b>Surat Jalan</p>
                </div>
        <div class="card">
        <div class="table-responsive table-striped">
          <table class="grid table table-bordered table-sortable" id="mydata" style="width:100%">
            <thead class="text-center">
                            <tr>
                              <th style="color: purple; font-weight: bold;">So Number</th>
                              <th style="color: purple; font-weight: bold;">Po Number</th>
                              <th style="color: purple; font-weight: bold;">Po Date</th>
                              <th style="color: purple; font-weight: bold;">Ship Via</th>
                              <th style="color: purple; font-weight: bold;">Nama Penerima</th>
                              <th style="color: purple; font-weight: bold;">Alamat</th>
                              <th style="color: purple; font-weight: bold;">No Telpon</th>
                              <th style="color: purple; font-weight: bold;">Status</th>
                              <th style="color: purple; font-weight: bold;">Action</th>
                            </tr>
                          </thead>
                        <tbody class="text-center" id="show_data">
                      </tbody>
                    </table>
           </div>
          </div>
        </div>

<!-- MODAL EDIT -->
        <form>
            <div class="modal fade" id="Modal_Edit" tabindex="-1" role="dialog">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><h3><b>Delivery Order</h3></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                 <div class = "modal-body">
                   <div class="table-responsive">
                          <table class="table">
                <tr>
                    <td width="220px">SO Number</td>
                    <td>:</td>
                    <td width="150px" name="sonumber_edit"></td>
                </tr>
                <tr>
                    <td>PO Number</td>
                    <td>:</td>
                    <td name="ponumber2"></td>
                </tr>
                <tr>
                    <td>Po Date</td>
                    <td>:</td>
                    <td name="podate2"></td>
                </tr>
                <tr>
                    <td>Ship Via</td>
                    <td>:</td>
                    <td name="shipvia2"></td>
                </tr>
                <tr>
                    <td>Nama Penerima</td>
                    <td>:</td>
                    <td name="namapenerima2"></td>
                </tr>
                <tr>
                    <td>Alamat</td>
                    <td>:</td>
                    <td name="alamat2"></td>
                </tr>
                <tr>
                    <td>No Telpon</td>
                    <td>:</td>
                    <td name="notlp2"></td>
                </tr>
            </table>
                  <div class="modal-footer">
                  <input type="text" class="form-control" id="catatan" placeholder="Catatan" size="100" autocomplete="off">
                  </div>
                  <div class="col">
                    <button type="button" type="submit" name="btn-printdo" class="btn btn-primary" onClick="window.location.reload()" style="margin-left: 180px">Print</button>
                  </div>
                </div>
              </div>
            </div>
            </form>
        <!--END MODAL EDIT-->

<script type="text/javascript" src="<?php echo base_url().'assets/js/jquery-3.2.1.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/js/jquery.dataTables.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/js/dataTables.bootstrap4.js'?>"></script>

<script type="text/javascript">
  $(document).ready(function(){
    show_product(); //call function show all product
    
   $('#mydata').DataTable({
    "ordering": false
});
     
    //function show all product
    function show_product(){
        $.ajax({
            type  : 'ajax',
            url   : '<?php echo site_url('delivery_order/list_data')?>',
            async : false,
            dataType : 'json',
            success : function(data){
                var html = '';
                var i;
                var btn = '';
                for(i=0; i<data.length; i++){
                  var temp_so = data[i].so_number;
                  var arr = temp_so.split("P");
                  var so_terpilih = "";
                  for (var t = 0; t < arr.length; t++) {
                      so_terpilih += arr[t]
                    }
                  var p_so = 'P' + so_terpilih;
                  if ((data[i].so_number == p_so) || (data[i].ket == 'Lunas')) {
<<<<<<< HEAD
=======
                    if (data[i].status == 'dikirim') {
                        btn = '<td style="text-align:right;">'+
                                    '<button type="button" class="btn btn-primary btn-sm" disabled >Detail</button>'+' '+
                                '</td>';
                              }else{
                           btn = '<td style="text-align:right;">'+
                                    '<a href="javascript:void(0);" class="btn btn-primary btn-sm mb1 bg-purple item_edit" data-so_number="'+data[i].so_number+'" data-po_number="'+data[i].po_number+'" data-po_date="'+data[i].po_date+'" data-ship_via="'+data[i].ship_via+'" data-nama_penerima="'+data[i].nama_penerima+'" data-alamat="'+data[i].alamat+'" data-no_tlp="'+data[i].no_tlp+'" >Detail</a>'+' '+
                                '</td>';
                              }
>>>>>>> projectafril
                     html += '<tr>'+
                            '<td>'+data[i].so_number+'</td>'+
                            '<td>'+data[i].po_number+'</td>'+
                            '<td>'+data[i].po_date+'</td>'+
                            '<td>'+data[i].ship_via+'</td>'+
                            '<td>'+data[i].nama_penerima+'</td>'+
                            '<td>'+data[i].alamat+'</td>'+
                            '<td>'+data[i].no_tlp+'</td>'+
                            '<td>'+data[i].status+'</td>'+
                            btn+
                            '</tr>';
                  }
                   
                }
                $('#show_data').html(html);
            }

        });
    }
        //get data for update record
        $('#show_data').on('click','.item_edit',function(){
            var so_number     = $(this).data('so_number');
            var po_number     = $(this).data('po_number');
            var po_date       = $(this).data('po_date');
            var ship_via      = $(this).data('ship_via');
            var nama_penerima = $(this).data('nama_penerima');
            var alamat        = $(this).data('alamat');
            var no_tlp        = $(this).data('no_tlp');

            
            $('#Modal_Edit').modal('show');
            $('[name="sonumber_edit"]').html(so_number);
            $('[name="ponumber2"]').html(po_number);
            $('[name="podate2"]').html(po_date);
            $('[name="shipvia2"]').html(ship_via);
            $('[name="namapenerima2"]').html(nama_penerima);
            $('[name="alamat2"]').html(alamat);
            $('[name="notlp2"]').html(no_tlp);
            $('[name="btn-printdo"]').attr('id',so_number);

            
        });

        //update record to database
        $('[name="btn-printdo"]').click(function(){
                              var element = $(this);
                              var id = element.attr("id");
                              var cat = $('#catatan').val();
                              if(confirm('Apakah Mau Cetak DO? ==> ' + id))
                            {
                             $.ajax({
                              type: 'POST',
                              url: "<?php echo base_url('delivery_order/cetak_delivery');?>",
                              data: {id:id, cat:cat},
                              success: function(data){
                              }});
                              var stat = $('[name="sonumber_edit"]').html();
                              var newstat = 'stat='+stat;
                                $.ajax({
                                type: "POST",
                                url: "<?php echo site_url('delivery_order/update_status');?>",
                                data: newstat,
                                
                            });
                            }

                            });
  });

</script>
  
