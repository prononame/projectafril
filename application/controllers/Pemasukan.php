<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pemasukan extends CI_Controller {

	public function __construct(){
		parent::__construct();

		$this->load->model('pemasukan_model');
		$this->load->model('data_karyawan_model');
		$this->load->library('form_validation');

		if ($this->session->userdata('status') != "login") {
			redirect('auth');
		}
	}

	public function index()
	{
		$data['title']='Pemasukan | Noname.com';
		$data2 = array(
			'dataSO' => $this->pemasukan_model->getData_so()->result(),
			'dataCashBon' => $this->data_karyawan_model->getData_cashbon()->result()
		);

		$this->load->view('templates/header', $data);
		$this->load->view('pemasukan', $data2);
		$this->load->view('templates/footer');
	}

	public function proses_insert()
	{	
		$this->pemasukan_model->insertPemasukan();
		redirect('pemasukan');
	}

	public function getData()
	{	
		$data = $this->pemasukan_model->getData_so()->result();
		echo json_encode($data);
	}

	public function pembayaran_cashbon()
	{
		$this->pemasukan_model->pembayaran_cashbon();
		redirect('pemasukan');
	}

	public function data_cashbon()
	{
		$data = $this->data_karyawan_model->getData_cashbon()->result();
		echo json_encode($data);
	}

	public function drop_cash()
	{
		$data = $this->pemasukan_model->drop_cash();
		redirect('pemasukan');
	}

	public function bunga_admin_bank()
	{
		$data = $this->pemasukan_model->bunga_admin_bank();
		redirect('pemasukan');
	}

}
