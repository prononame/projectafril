<div class="wrapper "> 
    <div class="sidebar" data-color="purple" data-background-color="white" data-image="<?php echo base_url('assets/img/sidebar-1.jpg'); ?>">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
      <div class="logo">
        <a href="#" class="simple-text logo-normal">
          PROJECTALFRIL
        </a>
      </div>
      <div class="sidebar-wrapper">
        <ul class="nav">
          <li class="nav-item active">
            <a class="nav-link" href="<?php echo site_url('laporan');?>">
              <i class="fa fa-shopping-cart"></i>
              <p>Laporan</p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo site_url('order-prioritas');?>">
              <i class="fa fa-shopping-cart"></i>
              <p>Order Prioritas</p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo site_url('s-pemasukan');?>">
              <i class="fa fa-shopping-cart"></i>
              <p>Pemasukan</p>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="<?php echo site_url('rekap');?>">
              <i class="material-icons">content_paste</i>
              <p>Rekap</p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo site_url('data-karyawan');?>">
              <i class="material-icons">content_paste</i>
              <p>Data Karyawan</p>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <a class="navbar-brand" href="<?php echo site_url('laporan'); ?>">Laporan</a>
          </div>
           <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">
            <form class="navbar-form">
              <div class="input-group no-border">
               
                  <div class="ripple-container"></div>
                </button>
              </div>
            </form>
            <ul class="navbar-nav">
              <li class="nav-item dropdown">
                <a class="nav-link" href="#pablo" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">account_circle</i>
                  <p class="d-lg-none d-md-block">
                    Account
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <a class="dropdown-item" href="#"><?php echo $this->session->userdata('nama') ?></a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php echo site_url('/logout') ?>">Log out</a>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->

      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-lg-12 col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">LAPORAN CASHFLOW</h4>
                  <p class="card-category"></p>
                </div>
                <div class="card-body">
                  <div class="row">
                    <div class="form-group col-md-2">
                      <label for="jenis_laporan_cashflow">Jenis Laporan</label>
                      <select class="form-control" name="jenis_laporan_cashflow" id="jenis_laporan_cashflow" required>
                        <option value="nol">Choose..</option>
                        <option value="cashflow_bank">Bank</option>
                        <option value="cashflow_cash">Cash</option>
                      </select>
                    </div>
                  </div>
                  <br>
                  <div class="row">
                    <div class="table-responsive">
                      <div id="table-cashflow"></div>
                    </div>
                  </div>
                  <br>
                  <a class="btn btn-primary" href="<?php echo site_url('laporan/cetak_cashflow'); ?>">Download Laporan</a>
                </div>
              </div>
            </div>
            <div class="col-lg-12 col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">LAPORAN ORDER</h4>
                  <p class="card-category"></p>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                      <table id="sales_order" name="table-sales-order" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead class="text-center">
                    <tr>
                        <th style="color: purple; font-weight: bold;">So Number</th>
                        <th style="color: purple; font-weight: bold;">So Date</th>
                        <th style="color: purple; font-weight: bold;">Nama</th>
                        <th style="color: purple; font-weight: bold;">Alamat</th>
                        <th style="color: purple; font-weight: bold;">Sub Total</th>
                        <th style="color: purple; font-weight: bold;">Drop Payment</th>
                        <th style="color: purple; font-weight: bold;">Kekurangan</th>
                        <th style="color: purple; font-weight: bold;">Pelunasan</th>
                        <th style="color: purple; font-weight: bold;">Saldo</th>
                        <th style="color: purple; font-weight: bold;">Keterangan</th>
                        <th style="color: purple; font-weight: bold;">Actions</th>
                    </tr>
                        </thead>
                        <tbody>
                      
                        </tbody>
                      </table>
                       <a class="btn btn-primary" href="<?php echo site_url('laporan/cetak_order'); ?>" style="margin-left: 800px">Download Laporan</a>
                    </div>
                </div>
              </div>
            </div>
            <div class="col-lg-12 col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">LAPORAN ORDER BULAN SEBELUMNYA</h4>
                  <p class="card-category"></p>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                      <table id="sales_order_sebelum" name="table-sales-order-sebelum" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead class="text-center">
                    <tr>
                        <th style="color: purple; font-weight: bold;">So Number</th>
                        <th style="color: purple; font-weight: bold;">So Date</th>
                        <th style="color: purple; font-weight: bold;">Nama</th>
                        <th style="color: purple; font-weight: bold;">Alamat</th>
                        <th style="color: purple; font-weight: bold;">Sub Total</th>
                        <th style="color: purple; font-weight: bold;">Drop Payment</th>
                        <th style="color: purple; font-weight: bold;">Kekurangan</th>
                        <th style="color: purple; font-weight: bold;">Pelunasan</th>
                        <th style="color: purple; font-weight: bold;">Saldo</th>
                        <th style="color: purple; font-weight: bold;">Keterangan</th>
                    </tr>
                        </thead>
                        <tbody>
                      
                        </tbody>
                      </table>
                      <a class="btn btn-primary" href="<?php echo site_url('laporan/cetak_order_bulanlalu'); ?>" style="margin-left: 800px">Download Laporan</a>
                    </div>
                </div>
              </div>
            </div>
            <div class="col-lg-12 col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">LAPORAN KEUANGAN</h4>
                  <p class="card-category"></p>
                </div>
                <div class="card-body">
                  <div class="row">
                    <div class="form-group col-md-2">
                      <label for="jenis_laporan_pengeluaran">Jenis Laporan</label>
                      <select class="form-control" name="jenis_laporan_pengeluaran" id="jenis_laporan_pengeluaran" required>
                        <option value="nol">Choose..</option>
                        <option value="tabel_aset">Aset</option>
                        <option value="tabel_bahan">Bahan</option>
                        <option value="tabel_jahit">Jahit</option>
                        <option value="tabel_pengiriman">Pengiriman</option>
                        <option value="tabel_gojek">Gojek</option>
                        <option value="tabel_beban_bulanan">Beban</option>
                        <option value="tabel_etc">Tidak Terduga</option>
                      </select>
                    </div>
                  </div>
                  <br>
                  <div class="row">
                    <div class="table-responsive">
                      <div id="table-pengeluaran"></div>
                    </div>
                  </div>
                  <br>
                  <a class="btn btn-primary" href="<?php echo site_url('laporan/cetak_keuangan'); ?>" style="margin-left: 800px">Download Laporan</a>
                </div>
              </div>
            </div>
            <div class="col-lg-12 col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">LAPORAN CASH BON</h4>
                  <p class="card-category"></p>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                      <table id="cashbon" name="table-cashbon" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead class="text-center">
                          <tr>
                            <th scope="col" style="color: purple; font-weight: bold;"><b>Tanggal</b></th>
                            <th scope="col" style="color: purple; font-weight: bold;"><b>Nama</b></th>
                            <th scope="col" style="color: purple; font-weight: bold;"><b>Pinjaman</b></th>
                            <th scope="col" style="color: purple; font-weight: bold;"><b>Pengembalian</b></th>
                            <th scope="col" style="color: purple; font-weight: bold;"><b>Sisa Pinjaman</b></th>
                          </tr>
                        </thead>
                        <tbody>
                      
                        </tbody>
                      </table>
                      <a class="btn btn-primary" href="<?php echo site_url('laporan/cetak_cashbon'); ?>" style="margin-left: 800px">Download Laporan</a>
                    </div>
                </div>
              </div>
            </div>
            <div class="col-lg-12 col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">LAPORAN INSENTIF</h4>
                  <p class="card-category"></p>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                      <table id="t_insentif" name="table-insentif" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead class="text-center">
                          <tr>
                            <th scope="col" style="color: purple; font-weight: bold;"><b>Tanggal</b></th>
                            <th scope="col" style="color: purple; font-weight: bold;"><b>Nomor PO</b></th>
                            <th scope="col" style="color: purple; font-weight: bold;"><b>Sales/Marketing</b></th>
                            <th scope="col" style="color: purple; font-weight: bold;"><b>Client</b></th>
                            <th scope="col" style="color: purple; font-weight: bold;"><b>Nilai Kontrak</b></th>
                            <th scope="col" style="color: purple; font-weight: bold;"><b>Insentif</b></th>
                          </tr>
                        </thead>
                        <tbody>
                      
                        </tbody>
                      </table>
                      <a class="btn btn-primary" href="<?php echo site_url('laporan/cetak_insentif'); ?>" style="margin-left: 800px">Download Laporan</a>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <!-- Bootstrap modal -->
<div class="modal fade" id="modal_detail" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><h3><b>Detail Data Order</h3></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <form method="post" action="<?php echo site_url('laporan/update_order'); ?>" id="update-order">
                  <div class="row">
                    <div class="form-group col-md-4">
                      <input type="text" class="form-control" name="so_number" id="so_number" readonly>
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-md-4">
                      <label for="isi_select">Deskripsi Barang</label>
                      <select name="des_barang_old" id="isi_select" class="form-control">
                      </select>
                    </div>
                    <div class="table-responsive">
                      <table class="table" id="isi_tabel">
                      </table>
                    </div>
                    <div class="row">
                      <div class="col-md-12 offsite-md-4">
                        <div id="form-dp">
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="modal-footer" id="footer-update">
                    
                </form>
              </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->


<script type="text/javascript" src="<?php echo base_url().'assets/js/jquery-3.2.1.js'?>"></script>
<script src="<?php echo base_url('assets/datatables/js/jquery.dataTables.min.js')?>"></script>
<script src="<?php echo base_url('assets/datatables/js/dataTables.bootstrap.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/js/dataTables.bootstrap4.js'?>"></script>

<script type="text/javascript">

var save_method; //for save method string
var table;
var table_so;
var table_insentif;
var table_cashflow;

$(document).ready(function() {

  $("#jenis_laporan_cashflow").on('change',function(){
    var jenis_laporan_cashflow = $(this).val();
    $('#table-cashflow').html(
                          '<table id="cashflow" name="table-cashflow" class="table table-striped table-bordered" cellspacing="0" width="100%">' +
                            '<thead class="text-center" style="color: purple; font-weight: bold;">' +
                              '<tr>' +
                                '<th scope="col"><b>Tanggal</b></th>' +
                                '<th scope="col"><b>Item</b></th>' +
                                '<th scope="col"><b>Pengeluaran</b></th>' +
                                '<th scope="col"><b>Pemasukan</b></th>' +
                                '<th scope="col"><b>Saldo</b></th>' +
                              '</tr>' +
                            '</thead>' +
                              '<tbody>' +
                      
                              '</tbody>' +
                          '</table>');
    $('[name=table-cashflow]').attr('id', jenis_laporan_cashflow);
    var id = $('[name=table-cashflow]').attr("id");
    var n_id = '#' + id;
      //datatables
      table_cashflow = $(n_id).DataTable({ 

          "ordering": false,
          "processing": true, //Feature control the processing indicator.
          "serverSide": true, //Feature control DataTables' server-side processing mode.
          "order": [], //Initial no order.

          // Load data for the table's content from an Ajax source
          "ajax": {
              "url": "<?php echo site_url('laporan/show_data_cashflow')?>",
              "type": "POST",
              "data": {jenis_laporan:jenis_laporan_cashflow}
          },

          //Set column definition initialisation properties.
          "columnDefs": [
          { 
              "targets": [ -1 ], //last column
              "orderable": false, //set not orderable
          },
          ],

      });

    });

  $("#jenis_laporan_pengeluaran").on('change',function(){
    var jenis_laporan_pengeluaran = $(this).val();
    $('#table-pengeluaran').html(
                          '<table id="pengeluaran" name="table-pengeluaran" class="table table-striped table-bordered" cellspacing="0" width="100%">' +
                            '<thead class="text-center" style="color: purple; font-weight: bold;">' +
                              '<tr>' +
                                '<th scope="col"><b>Tanggal</b></th>' +
                                '<th scope="col"><b>Item</b></th>' +
                                '<th scope="col"><b>Pengeluaran</b></th>' +
                                '<th scope="col"><b>Keterangan</b></th>' +
                              '</tr>' +
                            '</thead>' +
                              '<tbody>' +
                      
                              '</tbody>' +
                          '</table>');
    $('[name=table-pengeluaran]').attr('id', jenis_laporan_pengeluaran);
    var id = $('[name=table-pengeluaran]').attr("id");
    var n_id = '#' + id;
      //datatables
      table = $(n_id).DataTable({

          "ordering": false,
          "processing": true, //Feature control the processing indicator.
          "serverSide": true, //Feature control DataTables' server-side processing mode.
          "order": [], //Initial no order.

          // Load data for the table's content from an Ajax source
          "ajax": {
              "url": "<?php echo site_url('laporan/show_data_pengeluaran')?>",
              "type": "POST",
              "data": {jenis_laporan:jenis_laporan_pengeluaran}
          },

          //Set column definition initialisation properties.
          "columnDefs": [
          { 
              "targets": [ -1 ], //last column
              "orderable": false, //set not orderable
          },
          ],

      });

    });

    table = $('#cashbon').DataTable({

            "ordering": false,
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.

            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo site_url('laporan/show_data_cashbon')?>",
                "type": "POST",
                "data": {jenis_laporan:'cashflow_cash_bon'}
            },

            //Set column definition initialisation properties.
            "columnDefs": [
            { 
                "targets": [ -1 ], //last column
                "orderable": false, //set not orderable
            },
            ],

        });

    table_so = $('#sales_order').DataTable({

              "ordering": false,
              "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.

            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo site_url('laporan/show_data_so_su')?>",
                "type": "POST",
                "data": {jenis_laporan:'data_so'}
            },

            //Set column definition initialisation properties.
            "columnDefs": [
            { 
                "targets": [ -1 ], //last column
                "orderable": false, //set not orderable
            },
            ],

        });

    table_so_sebelum = $('#sales_order_sebelum').DataTable({

            "ordering": false,
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.

            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo site_url('laporan/show_data_so')?>",
                "type": "POST",
                "data": {jenis_laporan:'data_so_sebelum'}
            },

            //Set column definition initialisation properties.
            "columnDefs": [
            { 
                "targets": [ -1 ], //last column
                "orderable": false, //set not orderable
            },
            ],

        });

    table_insentif = $('#t_insentif').DataTable({

            "ordering": false,
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.

            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo site_url('laporan/show_data_insentif')?>",
                "type": "POST",
                "data": {jenis_laporan:'tabel_insentif'}
            },

            //Set column definition initialisation properties.
            "columnDefs": [
            { 
                "targets": [ -1 ], //last column
                "orderable": false, //set not orderable
            },
            ],

        });


    //datepicker
    $('.datepicker').datepicker({
        autoclose: true,
        format: "yyyy-mm-dd",
        todayHighlight: true,
        orientation: "top auto",
        todayBtn: true,
        todayHighlight: true,  
    });

    //set input/textarea/select event when change value, remove class error and remove text help block 
    $("input").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("textarea").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("select").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });

});



function add_person()
{
    save_method = 'add';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_form').modal('show'); // show bootstrap modal
    $('.modal-title').text('Add Person'); // Set Title to Bootstrap modal title
}

function edit_person(id)
{

    $('#isi_tabel').html(' '); // reset form on modals
    $('#footer-update').html(' ');

    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo site_url('laporan/detail_order/')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
          var html = '<option value="nol" selected>Choose...</option>';
          for (var i = 0; i < data.length; i++) {
            html +=  '<option value="'+data[i].des_barang+'">'+data[i].des_barang+'</option>';
            var so_number = data[i].so_number;
          }

          $('#so_number').val(so_number);
          $('#isi_select').html(html);

          $("#isi_select").on('change',function(){
              if ($(this).val() == 'nol') {
                $('#isi_tabel').html('<div class="col-md-12">'+
                                        '<div class="alert alert-info" align="center" role="alert">'+
                                          'Pilih Deskripsi Barang Dulu Bro'+
                                        '</div>'+
                                      '</div>');
                $('#footer-update').html(' ');

              }else{
                for (var i = 0; i < data.length; i++) {
                  if (data[i].des_barang == $(this).val()) {
                   var des_barang = data[i].des_barang;
                   var qty_barang = data[i].qty;
                   var sat_barang = data[i].sat;
                   var des_pekerjaan = data[i].des_pekerjaan;
                   var harga_barang = data[i].harga;
                   var diskon_barang = data[i].diskon;
                   var total_harga = data[i].total;
                   var ket = data[i].ket;

                   break;
                  }
                }
                 var isi_tabel = '<tr>' +
                                  '<th>Deskripsi Barang :</th>' +
                                  '<th><input type="text" class="form-control" name="des_barang" id="des_barang" value="'+des_barang+'"></th>' +
                                '</tr>' +
                                '<tr>' +
                                  '<th>Qty :</th>' +
                                  '<td><input type="text" class="form-control" name="qty_barang" id="qty_barang" onkeypress="return hanyaAngka(event)" value="'+qty_barang+'"></td>' +
                                '</tr>' +
                                '<tr>' +
                                  '<th>Satuan :</th>' +
                                  '<td><input type="text" class="form-control" name="sat_barang" id="sat_barang" value="'+sat_barang+'"></td>' +
                                '</tr>' +
                                '<tr>' +
                                  '<th>Deskripsi Pekerjaan :</th>' +
                                  '<td><input type="text" class="form-control" name="des_pekerjaan" id="des_pekerjaan" value="'+des_pekerjaan+'"></td>' +
                                '</tr>' +
                                '<tr>' +
                                  '<th>Harga :</th>' +
                                  '<td><input type="text" class="form-control" name="harga_barang" id="harga_barang" onkeypress="return hanyaAngka(event)" value="'+harga_barang+'"></td>' +
                                '</tr>' +
                                '<tr>' +
                                  '<th>Diskon :</th>' +
                                  '<td><input type="text" class="form-control" name="diskon_barang" id="diskon_barang" onkeypress="return hanyaAngka(event)" value="'+diskon_barang+'"></td>' +
                                '</tr>' +
                                '<tr>' +
                                  '<th>Total :</th>' +
                                  '<td><input type="text" class="form-control" name="total_harga" id="total_harga" value="'+total_harga+'" readonly></td>' +
                                '</tr>' +
                                '<tr>' +
                                  '<th>Keterangan :</th>' +
                                  '<td><input type="text" class="form-control" name="ket" id="ket" value="'+ket+'"></td>' +
                                '</tr>';
                                $('#isi_tabel').html(isi_tabel);
                                $('#footer-update').html('<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>'+
                                  '<button type="submit" class="btn btn-primary" name="tombol-update-order" id="tombol-update-order">Submit</button>');

                                $('#harga_barang').on('keyup', function(){
                                  var qtyBarang = $('#qty_barang').val();
                                  var diskonBarang = $('#diskon_barang').val();
                                  var arr = diskonBarang.split("%");
                                  var diskon_temp = "";

                                    for (var i = 0; i < arr.length; i++) {
                                      diskon_temp += arr[i]
                                    }
                                  var temp_hargaBarang = $('#harga_barang').val();
                                  var arr2 = temp_hargaBarang.split(".");
                                  var hargaBarang = "";

                                    for (var i = 0; i < arr2.length; i++) {
                                      hargaBarang += arr2[i]
                                    }

                                    if (qtyBarang === "") {
                                      $('#total_harga').val(hargaBarang);
                                    }else{
                                      var totalHarga = qtyBarang * hargaBarang;
                                      totalHarga = totalHarga - (totalHarga * (diskon_temp / 100));
                                      $('#total_harga').val(totalHarga); 
                                    }

                                    if (totalHarga > total_harga) {
                                      $('#form-dp').html('<label for="dp"><b>Penambahan DP</b></label>'+
                                                          '<input class="form-control" name="u_dp" id="dp" required>'+
                                                          'Jenis Pembayaran :'+
                                                          '<br>'+
                                                          '<div class="form-check form-check-inline">'+
                                                            '<input type="radio" name="tipe_pembayaran" id="tipe_pembayaran_bank" value="bank" required>'+
                                                            '<label class="form-check-label" for="tipe_pembayaran_bank">Bank</label>'+
                                                          '</div>'+
                                                          '<div class="form-check form-check-inline">'+
                                                            '<input type="radio" name="tipe_pembayaran" id="tipe_pembayaran_cash" value="cash" required>'+
                                                            '<label class="form-check-label" for="tipe_pembayaran_cash">Cash</label>'+
                                                          '</div>')
                                    }else{
                                      $('#form-dp').html(' ');
                                    }
                              });

                                $('#qty_barang').on('keyup', function(){
                                  var qtyBarang = $('#qty_barang').val();
                                  var diskonBarang = $('#diskon_barang').val();
                                  var arr = diskonBarang.split("%");
                                  var diskon_temp = "";

                                    for (var i = 0; i < arr.length; i++) {
                                      diskon_temp += arr[i]
                                    }
                                  var temp_hargaBarang = $('#harga_barang').val();
                                  var arr2 = temp_hargaBarang.split(".");
                                  var hargaBarang = "";

                                    for (var i = 0; i < arr2.length; i++) {
                                      hargaBarang += arr2[i]
                                    }

                                    if (qtyBarang === "") {
                                      $('#total_harga').val(hargaBarang);
                                    }else{
                                      var totalHarga = qtyBarang * hargaBarang;
                                      totalHarga = totalHarga - (totalHarga * (diskon_temp / 100));
                                      $('#total_harga').val(totalHarga); 
                                    }

                                    if (totalHarga > total_harga) {
                                      $('#form-dp').html('<label for="dp"><b>Penambahan DP</b></label>'+
                                                          '<input class="form-control" name="u_dp" id="dp" required>'+
                                                          'Jenis Pembayaran :'+
                                                          '<br>'+
                                                          '<div class="form-check form-check-inline">'+
                                                            '<input type="radio" name="tipe_pembayaran" id="tipe_pembayaran_bank" value="bank" required>'+
                                                            '<label class="form-check-label" for="tipe_pembayaran_bank">Bank</label>'+
                                                          '</div>'+
                                                          '<div class="form-check form-check-inline">'+
                                                            '<input type="radio" name="tipe_pembayaran" id="tipe_pembayaran_cash" value="cash" required>'+
                                                            '<label class="form-check-label" for="tipe_pembayaran_cash">Cash</label>'+
                                                          '</div>')
                                    }else{
                                      $('#form-dp').html(' ');
                                    }
                              });

                                $('#diskon_barang').on('focusout', function(){
                                  var diskonBarang = $('#diskon_barang').val();
                                  var totHarga = $('#total_harga').val();

                                  var arr = diskonBarang.split("%");
                                  var diskon_temp = "";

                                    for (var i = 0; i < arr.length; i++) {
                                      diskon_temp += arr[i]
                                    }
                                  
                                  var diskon = totHarga * (diskon_temp / 100);
                                  var grandDiskon = totHarga - diskon;

                                  var persen = diskonBarang + "%";

                                  $('#diskon_barang').val(persen);
                                  $('#total_harga').val(grandDiskon);

                                 if (totalHarga > total_harga) {
                                      $('#form-dp').html('<label for="dp"><b>Penambahan DP</b></label>'+
                                                          '<input class="form-control" name="u_dp" id="dp" required>'+
                                                          'Jenis Pembayaran :'+
                                                          '<br>'+
                                                          '<div class="form-check form-check-inline">'+
                                                            '<input type="radio" name="tipe_pembayaran" id="tipe_pembayaran_bank" value="bank" required>'+
                                                            '<label class="form-check-label" for="tipe_pembayaran_bank">Bank</label>'+
                                                          '</div>'+
                                                          '<div class="form-check form-check-inline">'+
                                                            '<input type="radio" name="tipe_pembayaran" id="tipe_pembayaran_cash" value="cash" required>'+
                                                            '<label class="form-check-label" for="tipe_pembayaran_cash">Cash</label>'+
                                                          '</div>')
                                    }else{
                                      $('#form-dp').html(' ');
                                    }
                                  
                              });

                                $('#diskon_barang').on('keydown', function(){
                                  var qtyBarang = $('#qty_barang').val();
                                  var temp_hargaBarang = $('#harga_barang').val();
                                  var arr = temp_hargaBarang.split(".");
                                  var hargaBarang = "";

                                    for (var i = 0; i < arr.length; i++) {
                                      hargaBarang += arr[i]
                                    }

                                    if (qtyBarang === "") {
                                      $('#total_harga').val(hargaBarang);
                                    }else{
                                      var totalHarga = qtyBarang * hargaBarang;
                                    $('#total_harga').val(totalHarga);
                                    }

                                    if (totalHarga > total_harga) {
                                      $('#form-dp').html('<label for="dp"><b>Penambahan DP</b></label>'+
                                                          '<input class="form-control" name="u_dp" id="dp" required>'+
                                                          'Jenis Pembayaran :'+
                                                          '<br>'+
                                                          '<div class="form-check form-check-inline">'+
                                                            '<input type="radio" name="tipe_pembayaran" id="tipe_pembayaran_bank" value="bank" required>'+
                                                            '<label class="form-check-label" for="tipe_pembayaran_bank">Bank</label>'+
                                                          '</div>'+
                                                          '<div class="form-check form-check-inline">'+
                                                            '<input type="radio" name="tipe_pembayaran" id="tipe_pembayaran_cash" value="cash" required>'+
                                                            '<label class="form-check-label" for="tipe_pembayaran_cash">Cash</label>'+
                                                          '</div>')
                                    }else{
                                      $('#form-dp').html(' ');
                                    }
                                  
                              });
                
                 }
            });

            $('.modal-title').text(''); // Set title to Bootstrap modal title
            $('#modal_detail').modal('show'); // show bootstrap modal when complete loaded

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}

function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax 
}

function save()
{
    $('#btnSave').text('saving...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    var url;

    if(save_method == 'add') {
        url = "<?php echo site_url('person/ajax_add')?>";
    } else {
        url = "<?php echo site_url('person/ajax_update')?>";
    }

    // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {

            if(data.status) //if success close modal and reload ajax table
            {
                $('#modal_form').modal('hide');
                reload_table();
            }
            else
            {
                for (var i = 0; i < data.inputerror.length; i++) 
                {
                    $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                    $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
                }
            }
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        }
    });
}

function delete_person(id)
{
    if(confirm('Are you sure delete this data?'))
    {
        // ajax delete data to database
        $.ajax({
            url : "<?php echo site_url('person/ajax_delete')?>/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                //if success reload ajax table
                $('#modal_form').modal('hide');
                reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });

    }
}

</script>

