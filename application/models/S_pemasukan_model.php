<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class S_pemasukan_model extends CI_Model{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function insert_saldo_awal()
	{
		$jenis_simpanan = $this->input->post('jenis_simpanan');
		$jmlh_saldo = preg_replace('/[^A-Za-z0-9\  ]/', '', $this->input->post('jmlh_saldo'));

		$data = array(
					   'tanggal' => date('d/m/y'),
					   'saldo_awal' => $jmlh_saldo,
					   'jumlah' => $jmlh_saldo
					);

		if ($jenis_simpanan == 'saldo_bank') {
			$this->db->insert($jenis_simpanan, $data);

			$isi_saldo = array(
							    'tanggal' => date('d/m/y'),
							    'item' => 'fund',
							    'pengeluaran' => ' ',
							    'pemasukan' => $jmlh_saldo,
							    'saldo' => $jmlh_saldo
							);

			$this->db->insert('cashflow_bank', $isi_saldo);

			$saldo_awal = $this->db->get_where('saldo_awal', array('item' => 'Saldo Bank'))->result();

			foreach ($saldo_awal as $row) {

				if ($row->pemasukan == 0) {
					$sc = preg_replace('/[^A-Za-z0-9\  ]/', '', $jmlh_saldo);

					$data = array(	'tanggal' => date('d/m/y'),
								  	'pemasukan' => $sc
									);

					$this->db->where('item', 'Saldo Bank');
					$this->db->update('saldo_awal', $data);
				}
			}

		}else{
			$this->db->insert($jenis_simpanan, $data);

			$isi_saldo = array(
							    'tanggal' => date('d/m/y'),
							    'item' => 'fund',
							    'pengeluaran' => ' ',
							    'pemasukan' => $jmlh_saldo,
							    'saldo' => $jmlh_saldo
							);

			$this->db->insert('cashflow_cash', $isi_saldo);
			$saldo_awal = $this->db->get_where('saldo_awal', array('item' => 'Saldo Cash'))->result();

			foreach ($saldo_awal as $row) {

				if ($row->pemasukan == 0) {
					$sc = preg_replace('/[^A-Za-z0-9\  ]/', '', $jmlh_saldo);

					$data = array(	'tanggal' => date('d/m/y'),
									'pemasukan' => $sc
								);

					$this->db->where('item', 'Saldo Cash');
					$this->db->update('saldo_awal', $data);
				}
			}
		}
		
	}

	public function insert_safty_oprasional()
	{
		$jmlh_oprasional = preg_replace('/[^A-Za-z0-9\  ]/', '', $this->input->post('jmlh_oprasional'));

		$data = array(
					   'tanggal' => date('d/m/y'),
					   'safty_oprasional' => $jmlh_oprasional
					);
		$data_oprasional = $this->db->get('safty_oprasional')->result();

		if (count($data_oprasional) > 0) {
			$this->db->update('safty_oprasional', $data);
		}else{
			$this->db->insert('safty_oprasional', $data);
		}
	}

}
?>