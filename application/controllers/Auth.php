<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

  public function __construct(){
    parent::__construct();

    $this->load->model('login_model');
    $this->load->library('form_validation');
  }

   public function index()
   {
    if ($this->session->userdata('error_stat') == "yes") {
      $error = $this->session->userdata('error');
    }else{
      $error = ' ';
    }

    $data = array(
                   'tittle' => 'Login | double-a.com',
                   'error' => $error
                );

    $unset = array('error','error_stat');
    $this->session->unset_userdata($unset);

    $this->load->view('login', $data);
   }

   function proses_login(){

     $user = $this->input->post('name');
     $pass = $this->input->post('pwd');

     $where = array(
                     'username' => $user,
                     'password' => $pass
                  );

     $cek = $this->login_model->proseslogin($where)->result();

     foreach ($cek as $row) {
       $nama = $row->nama_karyawan;
       $level = $row->level;
     }

     if($cek > 0){
      
      $data_session = array(
                             'username' => $user,
                             'nama' => $nama,
                             'level' => $level,
                             'status' => "login"
                           );

      $this->session->set_userdata($data_session);
      $lvl = $this->session->userdata('level');

      if ($lvl == "sales") {
        redirect('sales-order');
      }elseif ($lvl == "superuser") {
        redirect('laporan');
      }else{
        $error = 'Modal-error';
        $data_error = array(
                             'error' => $error,
                             'error_stat' => 'yes'
                           );
        $this->session->set_userdata($data_error);
        redirect('auth');
      }

    }

  }
 

}