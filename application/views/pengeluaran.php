<div class="wrapper ">
    <div class="sidebar" data-color="purple" data-background-color="white" data-image="<?php echo base_url('assets/img/sidebar-1.jpg'); ?>">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
      <div class="logo">
        <a href="#" class="simple-text logo-normal">
          <h6>DOUBLE A - HOUSE OF SUBLIMATION</h6>
        </a>
      </div>
      <div class="sidebar-wrapper">
        <ul class="nav">
          <li class="nav-item ">
            <a class="nav-link" href="<?php echo site_url('sales-order');?>">
              <i class="fa fa-shopping-cart"></i>
              <p><b>Sales Order</p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo site_url('purchase-order');?>">
              <i class="material-icons">local_offer</i>
              <p>Purchase Order</p>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="<?php echo site_url('invoice');?>">
              <i class="material-icons">content_paste</i>
              <p>Invoice</p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url('delivery-order');?>">
              <i class="material-icons">local_shipping</i>
              <p>Delivery Order</p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo site_url('laporan-sales');?>">
              <i class="material-icons">content_paste</i>
              <p>Laporan</p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo site_url('pemasukan');?>">
              <i class="material-icons">monetization_on</i>
              <p>Pemasukan</p>
            </a>
          </li>
          <li class="nav-item   active">
            <a class="nav-link" href="<?php echo site_url('pengeluaran');?>">
              <i class="material-icons">money_off</i>
              <p>Pengeluaran</p>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <a class="navbar-brand" href="<?php echo site_url('pengeluaran'); ?>">Pengeluaran</a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">
            <form class="navbar-form">
              <div class="input-group no-border">
                
                 
              </div>
            </form>
            <ul class="navbar-nav">
              
              <li class="nav-item dropdown">
                <a class="nav-link" href="#pablo" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">account_circle</i>
                  <p class="d-lg-none d-md-block">
                    Account
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <a class="dropdown-item" href="#"><?php echo $this->session->userdata('nama') ?></a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php echo site_url('/logout') ?>">Log out</a>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->

      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-lg-12 col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">INPUT PENGELUARAN</h4>
                  <p class="card-category"></p>
                </div>
                <div class="card-body"> 
                  <form method="post" action="<?php echo base_url('pengeluaran/proses-pengeluaran'); ?>" name="submitPengeluaran">
                  <div class="row">
                    <div class="form-group col-md-4">
                      <label for="typePengeluaran">Tipe Pengeluaran</label>
                      <select id="typePengeluaran" name="type" class="form-control">
                        <option value="nol" selected>Choose...</option>
                        <option value="tabel_aset">Aset</option>
                        <option value="tabel_bahan">Bahan Baku</option>
                        <option value="tabel_jahit">Jahit</option>
                        <option value="tabel_pengiriman">Pengiriman</option>
                        <option value="tabel_gojek">Gojek</option>
                        <option value="tabel_beban_bulanan">Beban Bulanan</option>
                        <option value="tabel_etc">Tidak Terduga</option>
                      </select>
                    </div>
                    <div class="offset-md-2">
                      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#Modal-Peminjaman-CashBon" style="height: 40px">
                        Peminjaman Cash Bon
                      </button>
                      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#Modal-Insentif" style="height: 40px">
                        Insentif
                      </button>
                      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#Modal-Gopay" style="height: 40px">
                        Saldo GoPay
                      </button>
                    </div>
                  </div>
                  <br>
                  <div class="form-row">
                    <div class="form-group col-md-2">
                      <label for="inputTanggal">Tanggal</label>
                      <br>
                      <input type="text" class="form-control" id="inputTanggal" name="tanggal" value="<?php echo date('d/m/y'); ?>" readonly>
                    </div>
                    <div class="form-group col-md-4">
                      <label for="inputItem">Item</label>
                      <br>
                      <input type="text" class="form-control" id="inputItem" name="item" required>
                    </div>
                    <div class="form-group col-md-3">
                      <label for="inputPengeluaran">Pengeluaran</label>
                      <br>
                      <div id="input_p"></div>
                      <span style="color: red; font-size: 12px" id="ket_saldo_gopay"></span>
                    </div>
                    <div class="form-group col-md-3">
                      <label for="inputKeterangan">Keterangan</label>
                      <br>
                      <input type="text" class="form-control" id="inputKeterangan" name="ket">
                    </div>
                  </div>
                  <div id="jenis_pembayaran"></div>
                    <button type="submit" name="submitPengeluaran" id="submitPengeluaran" onclick="Swal('Sukses')" style="visibility: hidden;" class="btn btn-primary">Submit</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>

        <!-- Modal Peminjaman Cash Bon -->
        <div class="modal fade" id="Modal-Peminjaman-CashBon" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Pinjaman Cash Bon</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <form method="post" action="<?php echo site_url('pengeluaran/peminjaman_cashbon'); ?>" name="peminjaman-cashbon">
                  <div class="form-group">
                    <label for="atas_nama">Atas Nama</label>
                    <select id="atas_nama" name="atas_nama" class="form-control">
                      <option value="nol">Choose...</option>
                      <?php 
                        foreach ($data_karyawan as $row) {
                          if ($row->saldo_pinjaman != 0) {
                            echo '<option value="'.$row->id.'">'.$row->nama_karyawan.'</option>';
                          }
                        }
                      ?>
                    </select>
                  </div>
                  <br>
                  <div class="form-group">
                    <label for="jmlh_pinjaman">Jumlah Pinjaman</label>
                    <input type="text" class="form-control" id="jmlh_pinjaman" name="jmlh_pinjaman" onkeypress="return hanyaAngka(event)" required>
                    <span id="ket_pinjaman"></span>
                  </div>
                  Jenis Pembayaran :
                      <br>
                    <div class="form-check form-check-inline">
                      <input type="radio" name="tipe_pembayaran" id="tipe_pembayaran_bank_cashbon" value="bank" required>
                      <label class="form-check-label" for="tipe_pembayaran_bank_cashbon">Bank</label>
                    </div>
                    <div class="form-check form-check-inline">
                      <input type="radio" name="tipe_pembayaran" id="tipe_pembayaran_cash_cashbon" value="cash" required>
                      <label class="form-check-label" for="tipe_pembayaran_cash_cashbon">Cash</label>
                    </div>
                    <br>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary" name="peminjaman-cashbon" onclick="Swal('Sukses')" id="tombol-pinjam-cashbon" style="visibility: hidden;">Submit</button>
                </form>
              </div>
            </div>
          </div>
        </div>

        <!-- Modal Insentif -->
        <div class="modal fade" id="Modal-Insentif" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Insentif</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <form method="post" action="<?php echo site_url('pengeluaran/insentif'); ?>" name="pengeluaran-insentif">
                  <div class="row">
                    <div class="table-responsive">
                      <table class="table">
                        <tr>
                          <th>Nomor PO :</th>
                          <th><input type="text" class="form-control" id="po_number" name="po_number" required></th>
                         </tr>
                         <tr>
                          <th>Sales/Marketing :</th>
                          <td><input type="text" class="form-control" id="nama_sales" name="nama_sales" required></td>
                         </tr>
                         <tr>
                          <th>Client :</th>
                          <td><input type="text" class="form-control" id="client" name="client"></td>
                         </tr>
                         <tr>
                          <th>Nilai Kontrak :</th>
                          <td><input type="text" class="form-control" id="nilai_kontrak" name="nilai_kontrak" onkeypress="return hanyaAngka(event)" required></td>
                         </tr>
                         <tr>
                          <th>Insentif :</th>
                          <td><input type="text" class="form-control" id="insentif" name="insentif" onkeypress="return hanyaAngka(event)" required></td>
                         </tr>
                      </table>
                    </div>                   
                  </div>
                  Jenis Pembayaran :
                      <br>
                    <div class="form-check form-check-inline">
                      <input type="radio" name="tipe_pembayaran" id="tipe_pembayaran_bank_insentif" value="bank" required>
                      <label class="form-check-label" for="tipe_pembayaran_bank_insentif">Bank</label>
                    </div>
                    <div class="form-check form-check-inline">
                      <input type="radio" name="tipe_pembayaran" id="tipe_pembayaran_cash_insentif" value="cash" required>
                      <label class="form-check-label" for="tipe_pembayaran_cash_insentif">Cash</label>
                    </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary" name="beban-bulanan" id="beban-bulanan">Submit</button>
                </form>
              </div>
            </div>
          </div>
        </div>

      </div>
  
  <!-- Modal Gopay -->
        <div class="modal fade" id="Modal-Gopay" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Pembelian Saldo GoPay</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <form method="post" action="<?php echo site_url('pengeluaran/gopay'); ?>" name="pengeluaran-gopay">
                  <div class="row">
                    <div class="table-responsive">
                      <table class="table">
                        <?php

                        foreach ($saldo_gopay as $row) {
                          $isi_saldo_gopay = $row->jumlah;
                        }

                        ?>
                        <tr>
                          <th>Sisa Saldo :</th>
                          <td><input type="text" class="form-control" onkeypress="return hanyaAngka(event)" id="sisa_saldo_gopay" value="<?php echo "Rp. " . number_format($isi_saldo_gopay,0,',','.'); ?>" readonly></td>
                         </tr>
                         <tr>
                          <th>Jumlah Saldo Yang Dibeli :</th>
                          <td><input type="text" class="form-control" id="jmlh_saldo_gopay" name="jmlh_saldo_gopay" onkeypress="return hanyaAngka(event)" required></td>
                         </tr>
                      </table>
                    </div>                   
                  </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary" name="bpembelian-gopay" id="pembelian-gopay">Submit</button>
                </form>
              </div>
            </div>
          </div>
        </div>
