<div class="wrapper ">
    <div class="sidebar" data-color="purple" data-background-color="white" data-image="<?php echo base_url('assets/img/sidebar-1.jpg'); ?>">
     <div class="logo">
        <a href="#" class="simple-text logo-normal">
          PROJECTALFRIL
        </a>
      </div>
      <div class="sidebar-wrapper">
        <ul class="nav">
          <li class="nav-item">
            <a class="nav-link" href="<?php echo site_url('laporan');?>">
              <i class="fa fa-shopping-cart"></i>
              <p>Laporan</p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo site_url('order_prioritas');?>">
              <i class="fa fa-shopping-cart"></i>
              <p>Order Prioritas</p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo site_url('s-pemasukan');?>">
              <i class="fa fa-shopping-cart"></i>
              <p>Pemasukan</p>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="<?php echo site_url('rekap');?>">
              <i class="material-icons">content_paste</i>
              <p>Rekap</p>
            </a>
          </li>
          <li class="nav-item active">
            <a class="nav-link" href="<?php echo base_url('data-karyawan');?>">
              <i class="material-icons">content_paste</i>
              <p>Data Karyawan</p>
            </a>
          </li>
        </ul>
      </div>
    </div>

    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <a class="navbar-brand" href="<?php echo site_url('data-karyawan');?>">Data Karyawan</a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">
            <form class="navbar-form">
              <div class="input-group no-border">
               
                  <div class="ripple-container"></div>
                </button>
              </div>
            </form>
            <ul class="navbar-nav">
              <li class="nav-item dropdown">
                <a class="nav-link" href="#pablo" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">account_circle</i>
                  <p class="d-lg-none d-md-block">
                    Account
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <a class="dropdown-item" href="#"><?php echo $this->session->userdata('nama') ?></a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php echo site_url('/logout') ?>">Log out</a>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->

      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-lg-12 col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">DATA KARYAWAN</h4>
                  <p class="card-category"></p>
                </div>
                <div class="card-body">
                  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#Modal-Tambah-Karyawan">
                    Tambah Karyawan
                  </button>
                <br>
                <br>
                  <div class="table-responsive">
                    <table class="table">
                      <thead>
                        <tr>
                          <th scope="col"><b>No</b></th>
                          <th scope="col"><b>Nama Karyawan</b></th>
                          <th scope="col"><b>Alamat</b></th>
                          <th scope="col"><b>No Telfon</b></th>
                          <th scope="col"><b>Username</b></th>
                          <th scope="col"><b>Password</b></th>
                          <th scope="col"><b>Action</b></th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php
                      $no = 1;
                      foreach ($data_karyawan as $row) {
                      ?>
                        <tr>
                          <th><?php echo $no++ ?></th>
                          <td><?php echo $row->nama_karyawan ?></td>
                          <td><?php echo $row->alamat ?></td>
                          <td><?php echo $row->no_tlp ?></td>
                          <td><?php echo $row->username ?></td>
                          <td><?php echo $row->password ?></td>
                          <td>
                            <button type="button" class="btn btn-info" name="update-karyawan" data-toggle="modal" id="<?php echo $row->id ?>" data-target="#Modal-Update-Karyawan">
                              Edit
                            </button>
                            <button class='btn btn-danger' name="delete-karyawan" id='<?php echo $row->id ?>'>Delete</button>
                          </td>
                        </tr>
                      <?php
                        }
                      ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
        <!-- Modal -->
        <div class="modal fade" id="Modal-Tambah-Karyawan" tabindex="-1" role="dialog" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Data Karyawan Baru</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <form method="post" action="<?php echo base_url('data-karyawan/tambah-karyawan');?>" name="tambah_karyawan">
                  <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" class="form-control" id="nama_karyawan" name="nama_karyawan" required>
                  </div>
                  <div class="form-group">
                    <label for="alamat">Alamat</label>
                    <input type="text" class="form-control" id="alamat" name="alamat" required>
                  </div>
                  <div class="form-group">
                    <label for="no_tlp">No Telfon</label>
                    <input type="text" class="form-control" id="no_tlp" name="no_tlp" required>
                  </div>
                  <div class="form-group">
                    <label for="username">Username</label>
                    <input type="text" class="form-control" id="username" name="username" required>
                  </div>
                  <div class="form-group">
                    <label for="pass">Password</label>
                    <input type="password" class="form-control" id="pass" name="pass" required>
                  </div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary" name="tambah_karyawan">Save</button>
                </form>
              </div>
            </div>
          </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="Modal-Update-Karyawan" tabindex="-1" role="dialog" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Update Data Karyawan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <form method="post" id="u_karyawan">
                  <div class="form-group">
                    <label for="nama">ID</label>
                    <input type="text" class="form-control" id="u_id" name="id" required>
                  </div>
                  <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" class="form-control" id="u_nama_karyawan" name="u_nama_karyawan" required>
                  </div>
                  <div class="form-group">
                    <label for="alamat">Alamat</label>
                    <input type="text" class="form-control" id="u_alamat" name="u_alamat" required>
                  </div>
                  <div class="form-group">
                    <label for="no_tlp">No Telfon</label>
                    <input type="text" class="form-control" id="u_no_tlp" name="u_no_tlp" required>
                  </div>
                  <div class="form-group">
                    <label for="username">Username</label>
                    <input type="text" class="form-control" id="u_username" name="U_username" required>
                  </div>
                  <div class="form-group">
                    <label for="pass">Password Lama</label>
                    <input type="password" class="form-control" id="u_pass_lama" name="pass_lama" required>
                    <span style="color: red; font-size: 13px" id="error_pass_lama"></span>
                  </div>
                  <div class="form-group">
                    <label for="pass">Password Baru</label>
                    <input type="password" class="form-control" id="u_pass_baru" name="pass_baru" required>
                  </div>
                  <div class="form-group">
                    <label for="pass">Confrim Password Baru</label>
                    <input type="password" class="form-control" id="u_confrim_pass_baru" name="confrim_pass_baru" required>
                    <span style="color: red; font-size: 13px" id="error_confrim_pass_lama"></span>
                  </div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="button" class="btn btn-primary" name="tombol-update-karyawan">Update</button>
                </form>
              </div>
            </div>
          </div>
        </div>