<div class="wrapper ">
    <div class="sidebar" data-color="purple" data-background-color="white" data-image="<?php echo base_url('assets/img/sidebar-1.jpg'); ?>">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
      <div class="logo">
        <a href="#" class="simple-text logo-normal">
          PROJECTALFRIL
        </a>
      </div>
      <div class="sidebar-wrapper">
        <ul class="nav">
          <li class="nav-item">
            <a class="nav-link" href="<?php echo site_url('laporan');?>">
              <i class="fa fa-shopping-cart"></i>
              <p>Laporan</p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo site_url('order-prioritas');?>">
              <i class="fa fa-shopping-cart"></i>
              <p>Order Prioritas</p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo site_url('s-pemasukan');?>">
              <i class="fa fa-shopping-cart"></i>
              <p>Pemasukan</p>
            </a>
          </li>
          <li class="nav-item active">
            <a class="nav-link" href="<?php echo site_url('rekap');?>">
              <i class="material-icons">content_paste</i>
              <p>Rekap</p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url('data-karyawan');?>">
              <i class="material-icons">content_paste</i>
              <p>Data Karyawan</p>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <a class="navbar-brand" href="<?php echo site_url('rekap');?>">Laporan Rekap</a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">
            <form class="navbar-form">
              <div class="input-group no-border">
               
                  <div class="ripple-container"></div>
                </button>
              </div>
            </form>
            <ul class="navbar-nav">
              <li class="nav-item dropdown">
                <a class="nav-link" href="#pablo" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">account_circle</i>
                  <p class="d-lg-none d-md-block">
                    Account
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <a class="dropdown-item" href="#"><?php echo $this->session->userdata('nama') ?></a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php echo site_url('/logout') ?>">Log out</a>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->

      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-lg-12 col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">Rekapitulasi</h4>
                  <p class="card-category"></p>
                </div>
                <div class="card-body">
                  <div class="row">
                    <a class="btn btn-primary" href="<?php echo site_url('rekap'); ?>">Reload Data</a>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#Modal-Pembukuan">
                      Pembukuan
                    </button>
                    <br>
                    <div class="table-responsive">
                      <table id="rekap" name="table-rekap" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                          <tr>
                            <th scope="col"><b>No</b></th>
                            <th scope="col"><b>Item</b></th>
                             <th scope="col"><b>Pemasukan</b></th>
                            <th scope="col"><b>Pengeluaran</b></th>
                            <th scope="col"><b>Keterangan</b></th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php
                          $saldo_awal = 0;
                          $pengeluaran_aset = 0;
                          $pengeluaran_bahan = 0;
                          $pengeluaran_jahit = 0;
                          $pengeluaran_pengiriman = 0;
                          $pengeluaran_etc = 0;
                          $pengeluaran_beban = 0;
                          $pengeluaran_insentif = 0;
                          $pengeluaran_cashbon = 0;
                          $pengeluaran_gojek = 0;
                          $pemasukan_cashbon = 0;
                          $pengeluaran_cashflow_bank = 0;
                          $pemasukan_cashflow_bank = 0;
                          $pengeluaran_cashflow_cash = 0;
                          $pemasukan_cashflow_cash = 0;
                          $keseluruhan_cashflow_bank = 0;
                          $keseluruhan_cashflow_cash = 0;

                          foreach ($data_saldo_awal as $row) {
                            if ($row->pemasukan == ' ') {
                              $saldo_awal += 0;
                            }else{
                              $saldo_awal += preg_replace('/[^A-Za-z0-9\  ]/', '', $row->pemasukan);
                            }
                          }

                          foreach ($pendapatan_sales_order as $row) {
                            $pendapatan_so = preg_replace('/[^A-Za-z0-9\  ]/', '', $row->jumlah);
                          }

                          foreach ($data_aset as $row) {
                            $pengeluaran_aset += preg_replace('/[^A-Za-z0-9\  ]/', '', $row->pengeluaran);
                          }

                          foreach ($data_bahan as $row) {
                            $pengeluaran_bahan += preg_replace('/[^A-Za-z0-9\  ]/', '', $row->pengeluaran);
                          }

                          foreach ($data_jahit as $row) {
                            $pengeluaran_jahit += preg_replace('/[^A-Za-z0-9\  ]/', '', $row->pengeluaran);
                          }

                          foreach ($data_pengiriman as $row) {
                            $pengeluaran_pengiriman += preg_replace('/[^A-Za-z0-9\  ]/', '', $row->pengeluaran);
                          }

                          foreach ($data_etc as $row) {
                            $pengeluaran_etc += preg_replace('/[^A-Za-z0-9\  ]/', '', $row->pengeluaran);
                          }

                          foreach ($data_beban as $row) {
                            $pengeluaran_beban += preg_replace('/[^A-Za-z0-9\  ]/', '', $row->pengeluaran);
                          }

                          foreach ($data_insentif as $row) {
                            $pengeluaran_insentif += preg_replace('/[^A-Za-z0-9\  ]/', '', $row->insentif);
                          }

                          foreach ($data_cashbon as $row) {
                            if ($row->pinjaman == ' ') {
                              $pengeluaran_cashbon += 0;
                            }else{
                              $pengeluaran_cashbon += preg_replace('/[^A-Za-z0-9\  ]/', '', $row->pinjaman);
                            }

                            if ($row->pengembalian == ' ') {
                              $pemasukan_cashbon += 0;
                            }else{
                              $pemasukan_cashbon += preg_replace('/[^A-Za-z0-9\  ]/', '', $row->pengembalian);
                            }
                            
                            
                          }

                          foreach ($data_safty as $row) {
                            $safty_oprasional = $row->safty_oprasional;
                          }

                          foreach ($data_saldo_gopay as $row) {
                            $saldo_gojek = $row->jumlah;
                          }

                          foreach ($data_gojek as $row) {
                            $pengeluaran_gojek += preg_replace('/[^A-Za-z0-9\  ]/', '', $row->pengeluaran);
                          }
                          
                          $sub_pemasukan = $saldo_awal + $pendapatan_so + $pemasukan_cashbon;
                          $sub_pengeluaran = $pengeluaran_aset + $pengeluaran_bahan + $pengeluaran_jahit + $pengeluaran_pengiriman + $pengeluaran_gojek + $pengeluaran_cashbon + $pengeluaran_etc + $pengeluaran_beban + $pengeluaran_insentif;
                          $sub_akhir = $sub_pemasukan - $sub_pengeluaran;
                          ?>
                          <tr>
                            <td>1</td>
                            <th scope="row">Saldo Awal</th>
                            <td><?php echo "Rp. " . number_format($saldo_awal,2,',','.'); ?></td>
                            <td></td>
                            <td></td>
                          </tr>
                          <tr>
                            <td>2</td>
                            <th scope="row">Pendapatan Sales Order</th>
                            <td><?php echo "Rp. " . number_format($pendapatan_so,2,',','.'); ?></td>
                            <td></td>
                            <td></td>
                          </tr>
                          <tr>
                            <td>3</td>
                            <th scope="row">Pengadaan Aset</th>
                            <td></td>
                            <td><?php echo "Rp. " . number_format($pengeluaran_aset,2,',','.'); ?></td>
                            <td></td>
                          </tr>
                          <tr>
                            <td>4</td>
                            <th scope="row">Bahan Baku</th>
                            <td></td>
                            <td><?php echo "Rp. " . number_format($pengeluaran_bahan,2,',','.'); ?></td>
                            <td></td>
                          </tr>
                          <tr>
                            <td>5</td>
                            <th scope="row">Pekerjaan Jahit</th>
                            <td></td>
                            <td><?php echo "Rp. " . number_format($pengeluaran_jahit,2,',','.'); ?></td>
                            <td></td>
                          </tr>
                          <tr>
                            <td>6</td>
                            <th scope="row">Pengiriman</th>
                            <td></td>
                            <td><?php echo "Rp. " . number_format($pengeluaran_pengiriman,2,',','.'); ?></td>
                            <td></td>
                          </tr>
                          <tr>
                            <td>7</td>
                            <th scope="row">Gojek</th>
                            <td></td>
                            <td><?php echo "Rp. " . number_format($pengeluaran_gojek,2,',','.'); ?></td>
                            <td></td>
                          </tr>
                          <tr>
                            <td>8</td>
                            <th scope="row">Biaya Tidak Terduga</th>
                            <td></td>
                            <td><?php echo "Rp. " . number_format($pengeluaran_etc,2,',','.'); ?></td>
                            <td></td>
                          </tr>
                          <tr>
                            <td>9</td>
                            <th scope="row">Beban Bulanan</th>
                            <td></td>
                            <td><?php echo "Rp. " . number_format($pengeluaran_beban,2,',','.'); ?></td>
                            <td></td>
                          </tr>
                          <tr>
                            <td>10</td>
                            <th scope="row">Insentif</th>
                            <td></td>
                            <td><?php echo "Rp. " . number_format($pengeluaran_insentif,2,',','.'); ?></td>
                            <td></td>
                          </tr>
                          <tr>
                            <td>11</td>
                            <th scope="row">Cash Bon</th>
                            <td><?php echo "Rp. " . number_format($pemasukan_cashbon,2,',','.'); ?></td>
                            <td><?php echo "Rp. " . number_format($pengeluaran_cashbon,2,',','.'); ?></td>
                            <td></td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered">
                          <tr>
                            <th width="300px">Sub Total Pemasukan :</th>
                            <td><?php echo "Rp. " . number_format($sub_pemasukan,2,',','.'); ?></td>
                          </tr>
                          <tr>
                            <th>Sub Total Pengeluaran :</th>
                            <td><?php echo "Rp. " . number_format($sub_pengeluaran,2,',','.'); ?></td>
                          </tr>
                          <tr>
                            <th>Saldo Akhir :</th>
                            <td><?php echo "Rp. " . number_format($sub_akhir,2,',','.'); ?></td>
                          </tr>
                        </table>
                    </div>
                    <a href="<?php echo site_url('rekap/cetak_rekap'); ?>"></a>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-12 col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">Laba Rugi</h4>
                  <p class="card-category"></p>
                </div>
                <div class="card-body">
                  <div class="row">
                    <h4>Pendapatan Produksi</h4>
                    <div class="table-responsive">
                      <table class="table table-striped table-bordered">
                        <tr>
                          <th width="300px">Saldo Awal :</th>
                          <td><?php echo "Rp. " . number_format($saldo_awal,2,',','.'); ?></td>
                        </tr>
                        <tr>
                          <th>Pendapatan Sales Order :</th>
                          <td><?php echo "Rp. " . number_format($pendapatan_so,2,',','.'); ?></td>
                        </tr>
                        <tr>
                          <th>Pendapatan Kotor :</th>
                          <th><?php $pendapatan_kotor = $saldo_awal + $pendapatan_so; echo "Rp. " . number_format($pendapatan_kotor,2,',','.'); ?></th>
                        </tr>
                      </table>
                  </div>
                </div>
                <div class="row">
                    <h4>Harga Pokok Produksi</h4>
                    <div class="table-responsive">
                      <table class="table table-striped table-bordered">
                        <tr>
                          <th width="300px">Bahan Baku :</th>
                          <td><?php echo "Rp. " . number_format($pengeluaran_bahan,2,',','.'); ?></td>
                        </tr>
                        <tr>
                          <th>Pekerjaan Jahit :</th>
                          <td><?php echo "Rp. " . number_format($pengeluaran_jahit,2,',','.'); ?></td>
                        </tr>
                        <tr>
                          <th>Pendapatan Setelah Dikurangi Harga Pokok Produksi :</th>
                          <th><?php $pendapatan_pokok = ($saldo_awal + $pendapatan_so) - ($pengeluaran_bahan + $pengeluaran_jahit); echo "Rp. " . number_format($pendapatan_pokok,2,',','.'); ?></th>
                        </tr>
                      </table>
                  </div>
                </div>
                <div class="row">
                    <h4>Beban Produksi</h4>
                    <div class="table-responsive">
                      <table class="table table-striped table-bordered">
                        <tr>
                          <th width="300px">Pengiriman :</th>
                          <td><?php echo "Rp. " . number_format($pengeluaran_pengiriman,2,',','.'); ?></td>
                        </tr>
                        <tr>
                          <th>Biaya Tidak Terduga :</th>
                          <td><?php echo "Rp. " . number_format($pengeluaran_etc,2,',','.'); ?></td>
                        </tr>
                        <tr>
                          <th>Beban Bulanan :</th>
                          <td><?php echo "Rp. " . number_format($pengeluaran_beban,2,',','.'); ?></td>
                        </tr>
                        <tr>
                          <th>Insentif :</th>
                          <td><?php echo "Rp. " . number_format($pengeluaran_insentif,2,',','.'); ?></td>
                        </tr>
                        <tr>
                          <th>Cash Bon Karyawan :</th>
                          <td><?php echo "Rp. " . number_format($pengeluaran_cashbon,2,',','.'); ?></td>
                        </tr>
                        <tr>
                          <th>Pendapatan Bersih :</th>
                          <th><?php $pendapatan_bersih = $pendapatan_pokok - ($pengeluaran_pengiriman + $pengeluaran_etc + $pengeluaran_beban + $pengeluaran_insentif + $pengeluaran_cashbon);  echo "Rp. " . number_format($pendapatan_bersih,2,',','.'); ?></th>
                        </tr>
                      </table>
                  </div>
                </div>
                </div>
              </div>
            </div>

                <div class="col-lg-12 col-md-12">
                  <div class="card">
                    <div class="card-header card-header-primary">
                      <h4 class="card-title">Profit</h4>
                      <p class="card-category"></p>
                    </div>
                    <div class="card-body">
                      <div class="row">
                          <div class="table-responsive">
                            <table class="table table-striped table-bordered">
                              <tr>
                                <th width="300px">Pendapatan Bersih :</th>
                                <th><?php echo "Rp. " . number_format($pendapatan_bersih,2,',','.'); ?></th>
                              </tr>
                            </table>
                          </div>
                        </div>
                        <div class="row">
                          <h4>Operasional</h4>
                            <div class="table-responsive">
                              <table class="table table-striped table-bordered">
                                <tr>
                                  <th width="300px">Safty Oprasional :</th>
                                  <td><?php echo "Rp. " . number_format($safty_oprasional,2,',','.'); ?></td>
                                </tr>
                                <tr>
                                  <th>Profit Bersih Setelah dikurangi Safty Oprasional :</th>
                                  <th><?php $profit_kotor = $pendapatan_bersih - $safty_oprasional; echo "Rp. " . number_format($profit_kotor,2,',','.'); ?></th>
                                </tr>
                              </table>
                            </div>
                          </div>
                            <div class="row">
                            <h4>Profit Infestor</h4>
                            <div class="table-responsive">
                              <table class="table table-striped table-bordered">
                                <tr>
                                  <th width="300px">Profit Bersih :</th>
                                  <th><?php $profit_bersih_infestor = $profit_kotor * 0.7; echo "Rp. " . number_format($profit_bersih_infestor,2,',','.'); ?></th>
                                </tr>
                                <tr>
                                  <th>Profit Berbentuk Aset :</th>
                                  <td><?php echo "Rp. " . number_format($pengeluaran_aset,2,',','.'); ?></td>
                                </tr>
                                <tr>
                                  <th>Profit Berbentuk Cash :</th>
                                  <td><?php $profit_cash = $profit_bersih_infestor - $pengeluaran_aset; echo "Rp. " . number_format($profit_cash,2,',','.'); ?></td>
                                </tr>
                              </table>
                            </div>
                          </div>
                          <div class="row">
                            <h4>Profit Direksi</h4>
                            <div class="table-responsive">
                              <table class="table table-striped table-bordered">
                                <tr>
                                  <th width="300px">Profit Bersih :</th>
                                  <th><?php echo "Rp. " . number_format($profit_kotor * 0.3,2,',','.'); ?></th>
                                </tr>
                              </table>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
          </div>
        </div>
      </div>

      <!-- Modal Pembukuan -->
        <div class="modal fade" id="Modal-Pembukuan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Pembukuan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <form method="post" action="<?php echo site_url('rekap/pembukuan'); ?>" name="peminjaman-cashbon">
                  <div class="form-group">
                    <label for="saldo_awal_bank">Saldo Awal Bank Bulan depan</label>
                    <br>
                    <input type="text" class="form-control" id="saldo_awal_bank" name="saldo_awal_bank" required>
                  </div>
                  <div class="form-group">
                    <label for="saldo_awal_cash">Saldo Awal Cash Bulan depan</label>
                    <br>
                    <input type="text" class="form-control" id="saldo_awal_cash" name="saldo_awal_cash" required>
                  </div>
                  <div class="form-group">
                    <label for="oprasional_bulan_depan">Safty Oprasional Bulan depan</label>
                    <br>
                    <input type="text" class="form-control" id="oprasional_bulan_depan" name="oprasional_bulan_depan" required>
                  </div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary" name="safty_oprasional" id="tombol-oprasional">Submit</button>
                </form>
              </div>
            </div>
          </div>
        </div>