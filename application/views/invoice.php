
<div class="wrapper ">
    <div class="sidebar" data-color="purple" data-background-color="white" data-image="<?php echo base_url('assets/img/sidebar-1.jpg'); ?>">
      <div class="logo">
        <a href="#" class="simple-text logo-normal">
          <h6>DOUBLE A - HOUSE OF SUBLIMATION</h6>
        </a>
      </div>
      <div class="sidebar-wrapper">
        <ul class="nav">
          <li class="nav-item ">
            <a class="nav-link" href="<?php echo site_url('sales-order');?>">
              <i class="fa fa-shopping-cart"></i>
              <p>Sales Order</p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo site_url('purchase-order');?>">
              <i class="material-icons">local_offer</i>
              <p>Purchase Order</p>
            </a>
          </li>
          <li class="nav-item active">
            <a class="nav-link" href="<?php echo site_url('invoice');?>">
              <i class="material-icons">content_paste</i>
              <p>Invoice</p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url('delivery-order');?>">
              <i class="material-icons">local_shipping</i>
              <p>Delivery Order</p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo site_url('laporan-sales');?>">
              <i class="fa fa-shopping-cart"></i>
              <p>Laporan</p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo site_url('pemasukan');?>">
              <i class="material-icons">monetization_on</i>
              <p>Pemasukan</p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo site_url('pengeluaran');?>">
              <i class="material-icons">money_off</i>
              <p>Pengeluaran</p>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <a class="navbar-brand" href="<?php echo base_url(); ?>">INVOICE</a>
          </div>
         <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">
            <form class="navbar-form">
              <div class="input-group no-border">
               
                  <div class="ripple-container"></div>
                </button>
              </div>
            </form>
            <ul class="navbar-nav">
              <li class="nav-item dropdown">
                <a class="nav-link" href="#pablo" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">account_circle</i>
                  <p class="d-lg-none d-md-block">
                    Account
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <a class="dropdown-item" href="#"><?php echo $this->session->userdata('nama') ?></br></a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php echo site_url('/logout') ?>">Log out</a>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-lg-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title" style="text-align: center;"><b>INVOICE</h4>
                    <p class="card-category" style="text-align: center;"><b></p>
                  <p class="card-category"></p>
                </div>
        <div class="card">
        <div class="table-responsive table-striped">
          <table class="grid table table-bordered table-sortable" id="mydata">
            <thead class="text-center">
                    <tr>
                        <th style="color: purple; font-weight: bold;">So Number</th>
                        <th style="color: purple; font-weight: bold;">So Date</th>
                        <th style="color: purple; font-weight: bold;">Nama</th>
                        <th style="color: purple; font-weight: bold;">Alamat</th>
                        <th style="color: purple; font-weight: bold;">No telp</th>
                        <th style="color: purple; font-weight: bold;">Sub Total</th>
                        <th style="color: purple; font-weight: bold;">Drop Payment</th>
                        <th style="color: purple; font-weight: bold;">Kekurangan</th>
                        <th style="color: purple; font-weight: bold;">Pelunasan</th>
                        <th style="color: purple; font-weight: bold;">Keterangan</th>
                        <th style="color: purple; font-weight: bold;">Actions</th>
                    </tr>
                </thead>
                <tbody class="text-center" id="show_data">  
                </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
   <!-- MODAL EDIT -->
        <form>
            <div class="modal fade" id="Modal_Edit" tabindex="-1" role="dialog">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><h3><b>INVOICE</h3></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                 <div class = "modal-body">
                   <div class="table-responsive">
                          <table class="table">
                <tr>
                    <td width="220px">SO Number</td>
                    <td>:</td>
                    <td width="150px" name="sonumber_edit"></td>
                </tr>
                <tr>
                    <td>SO Date</td>
                    <td>:</td>
                    <td name="sodate_edit"></td>
                </tr>
                <tr>
                    <td>Nama Pelanggan</td>
                    <td>:</td>
                    <td name="nama_edit"></td>
                </tr>
                <tr>
                    <td>Alamat</td>
                    <td>:</td>
                    <td name="alamat2"></td>
                </tr>
                <tr>
                    <td>No Telp</td>
                    <td>:</td>
                    <td name="notlp2"></td>
                </tr>
                <tr>
                    <td>Sub Total</td>
                    <td>:</td>
                    <td name="subtotal2"></td>
                </tr>
                <tr>
                    <td>Drop Payment</td>
                    <td>:</td>
                    <td name="dp"></td>
                </tr>
                <tr>
                    <td>Kekurangan</td>
                    <td>:</td>
                    <td name="kekurangan2"></td>
                </tr>
                 <tr>
                    <td>Pelunasan</td>
                    <td>:</td>
                    <td name="pelunasan2"></td>
                </tr>
                <tr>
                    <td>Keterangan</td>
                    <td>:</td>
                    <td name="ket2"></td>
                </tr>

            </table>
            <div class="table-responsive">
             <table class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
            <thead class="text-center">
                    <tr>
                      <th width="10px" style="font-weight: bold;">No</th>
                        <th width="200px" style="font-weight: bold;">Barang</th>
                        <th width="50px" style="font-weight: bold;">Qty</th>
                        <th width="100px" style="font-weight: bold;">Harga/Pcs</th>
                        <th width="10px" style="font-weight: bold;">Disc</th>
                    </tr>
                </thead>
                <tbody class="text-center" id="show_data2">
                    
                </tbody>
            </table>
                  <div class="modal-footer">
                    
                          <input type="text" class="form-control" id="catatan" placeholder="Catatan" size="100" autocomplete="off">
                         
                    <button type="button" type="submit" id="tes" name="btn-printinvoice" class="btn btn-primary" onClick="window.location.reload()">Print</button>
                  </div>
                </div>
              </div>
            </div>
            </form>
        <!--END MODAL EDIT-->

<script type="text/javascript" src="<?php echo base_url().'assets/js/jquery-3.2.1.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/js/jquery.dataTables.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/js/dataTables.bootstrap4.js'?>"></script>

<script type="text/javascript">
  $(document).ready(function(){
    show_product(); 
    $('#mydata').DataTable({
          "ordering": false
      }); 
  function show_product(){
        $.ajax({
            type  : 'ajax',
            url   : '<?php echo site_url('invoice/product_data')?>',
            async : false,
            dataType : 'json',
            success : function(data){
                var html = '';
                var i;
                for(i=0; i<data.length; i++){
                    html += '<tr>'+
                            '<td>'+data[i].so_number+'</td>'+
                            '<td>'+data[i].so_date+'</td>'+
                            '<td>'+data[i].nama_pelanggan+'</td>'+
                            '<td>'+data[i].alamat+'</td>'+
                            '<td>'+data[i].no_tlp+'</td>'+
                            '<td>'+data[i].sub_total+'</td>'+
                            '<td>'+data[i].drop_payment+'</td>'+
                            '<td>'+data[i].kekurangan+'</td>'+
                            '<td>'+data[i].pelunasan+'</td>'+
                            '<td>'+data[i].ket+'</td>'+
                            '<td style="text-align:right;">'+
                                    '<a href="javascript:void(0);" class="btn btn-primary btn-sm mb1 bg-purple item_edit" data-so_number="'+data[i].so_number+'" data-so_date="'+data[i].so_date+'" data-nama_pelanggan="'+data[i].nama_pelanggan+'" data-alamat="'+data[i].alamat+'" data-no_tlp="'+data[i].no_tlp+'" data-sub_total="'+data[i].sub_total+'" data-drop_payment="'+data[i].drop_payment+'" data-kekurangan="'+data[i].kekurangan+'" data-pelunasan=" '+data[i].pelunasan+'" data-ket="'+data[i].ket+'">Detail</a>'+' '+
                                '</td>'+
                            '</tr>';
                }
                $('#show_data').html(html);
            }

        });
    }
        //get data for update record
        $('#show_data').on('click','.item_edit',function(){
            var so_number       = $(this).data('so_number');
            var so_date         = $(this).data('so_date');
            var nama_pelanggan  = $(this).data('nama_pelanggan');
            var alamat          = $(this).data('alamat');
            var no_tlp          = $(this).data('no_tlp');
            var sub_total       = $(this).data('sub_total');
            var drop_payment    = $(this).data('drop_payment');
            var kekurangan      = $(this).data('kekurangan');
            var pelunasan       = $(this).data('pelunasan');
            var ket             = $(this).data('ket'); 

            $.ajax({          
                      type: 'ajax',
                      url: '<?php echo site_url('invoice/data_order');?>',
                      async : false,
                      dataType : 'json',
                      success: function(data) {
                      var html = '';
                      var i;
                      $no = 1;
                        for (var i = 0; i < data.length; i++){
                          if (so_number == data[i].so_number){
                              html += '<tr>'+
                                      '<td>'+$no++ +'</td>'+
                                      '<td>'+data[i].des_barang+'</td>'+
                                      '<td>'+data[i].qty+'</td>'+
                                      '<td>'+data[i].harga+'</td>'+
                                      '<td>'+data[i].diskon+'</td>'+
                                      '</tr>';
                           }
                        }
                      $('#show_data2').html(html);
                      }
                  });
          
            
            $('#Modal_Edit').modal('show');
            $('[name="sonumber_edit"]').html(so_number);
            $('[name="sodate_edit"]').html(so_date);
            $('[name="nama_edit"]').html(nama_pelanggan);
            $('[name="alamat2"]').html(alamat);
            $('[name="notlp2"]').html(no_tlp);
            $('[name="subtotal2"]').html(sub_total);
            $('[name="dp"]').html(drop_payment);
            $('[name="kekurangan2"]').html(kekurangan);
            $('[name="pelunasan2"]').html(pelunasan);
            $('[name="ket2"]').html(ket);
            $('[name="btn-printinvoice"]').attr('id',so_number);

            
        });

        //btnprint
        $('[name="btn-printinvoice"]').click(function(){
                              var element = $(this);
                              var id = element.attr("id");
                              var cat = $('#catatan').val();
                              var isi = 'id=' + id;
                              if(confirm('Apakah Mau Cetak Invoice? ==> ' + id))
                            {
                             $.ajax({
                              type: 'POST',
                              url: "<?php echo base_url('invoice/cetak_invoice');?>",
                              data: {id:id, cat:cat},
                              success: function(data){
                              }});
                              var stat = $('[name="sonumber_edit"]').html();
                              var noinvoice = 'stat='+stat;
                                $.ajax({
                                type: "POST",
                                url: "<?php echo site_url('invoice/getno_invoice');?>",
                                data: noinvoice,
                            });
                            }

                            });
  });

</script>
