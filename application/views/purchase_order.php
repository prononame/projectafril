<div class="wrapper ">
    <div class="sidebar" data-color="purple" data-background-color="white" data-image="<?php echo base_url('assets/img/sidebar-1.jpg'); ?>">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
      <div class="logo">
        <a href="#" class="simple-text logo-normal">
          <h6>DOUBLE A - HOUSE OF SUBLIMATION</h6>
        </a>
      </div>
      <div class="sidebar-wrapper">
        <ul class="nav">
          <li class="nav-item">
            <a class="nav-link" href="<?php echo site_url('sales-order');?>">
              <i class="fa fa-shopping-cart"></i>
              <p>Sales Order</p>
            </a>
          </li>
          <li class="nav-item active">
            <a class="nav-link" href="<?php echo site_url('purchase-order');?>">
              <i class="material-icons">local_offer</i>
              <p>Purchase Order</p>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="<?php echo site_url('invoice');?>">
              <i class="material-icons">content_paste</i>
              <p>Invoice</p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url('delivery-order');?>">
              <i class="material-icons">local_shipping</i>
              <p>Delivery Order</p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo site_url('laporan-sales');?>">
              <i class="material-icons">content_paste</i>
              <p>Laporan</p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo site_url('pemasukan');?>">
              <i class="material-icons">monetization_on</i>
              <p>Pemasukan</p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo site_url('pengeluaran');?>">
              <i class="material-icons">money_off</i>
              <p>Pengeluaran</p>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <a class="navbar-brand" href="<?php echo site_url('purchase-order'); ?>">Purcashe order</a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">
            <ul class="navbar-nav">
              <li class="nav-item dropdown">
                <a class="nav-link" href="#pablo" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">person</i>
                  <p class="d-lg-none d-md-block">
                    Account
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <a class="dropdown-item" href="#"><?php echo $this->session->userdata('nama') ?></a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php echo site_url('/logout') ?>">Log out</a>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-lg-12 col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">PURCHASE ORDER</h4>
                  <p class="card-category"></p>
                </div>
                <div class="card-body">
                  <div class="row">
                    <div class="col-lg-4 col-md-12">
                      <div class="card">
                        <div class="card-header card-header-primary">
                          <h6 class="card-title">Order By :</h6>
                        </div>
                        <div class="card-body">
                         <form method="post" action="<?php echo site_url('purchaseorder/proses_order'); ?>" name="submitOrder">
                          <div class="form-group">
                            <input type="text" class="form-control" name="nama_perusahaan" id="nama_perusahaan" placeholder="PT GOJEK" readonly>
                          </div>
                          <div class="form-group">
                            <textarea class="form-control" name="alamat_perusahaan" id="alamat_perusahaan" placeholder="JL.Ibrahin Adjie" rows="3" readonly></textarea>
                          </div>
                          <div class="form-group">
                            <input type="text" class="form-control" name="telfon_perusahaan" id="telfon_perusahaan" placeholder="08111232321" readonly>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-4 col-md-12">
                      <div class="card">
                        <div class="card-header card-header-primary">
                          <h6 class="card-title">Supplier :</h6>
                        </div>
                        <div class="card-body">
                          <div class="form-group">
                            <input type="text" class="form-control" name="nama_supplier" id="nama_supplier" placeholder="Nama Pelanggan" required>
                          </div>
                          <div class="form-group">
                            <textarea class='autocomplete form-control' name="alamat_supplier" id="alamat_supplier" placeholder="Alamat" rows="3" required></textarea>
                          </div>
                          <div class="form-group">
                            <input type="text" class='autocomplete form-control' name="telfon_supplier" id="telfon_supplier" onkeypress="return hanyaAngka(event)" placeholder="No Telfon" required>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-4 col-md-12">
                      <div class="card">
                        <div class="card-body">
                        <div class="form-group">
                             <label for="po_date">PO Date</label>
                            <input type="date" class="form-control" name="po_date" id="po_date" placeholder="PO Date">
                          </div>
                          <div class="form-group">
                            <input type="text" class="form-control" name="po_number" id="po_number" onkeypress="return hanyaAngka(event)" placeholder="PO Number" required>
                          </div>
                          <div class="form-group">
                            <label for="ship_via">Ship Via</label>
                            <select class="form-control" name="ship_via" id="ship_via" required>
                              <option>JNE</option>
                              <option>TIKI</option>
                              <option>GO-JEK</option>
                              <option>J&T</option>
                            </select>
                          </div>
                          <!-- Button trigger modal -->
                          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#orderModal">
                            Order
                          </button>
                        </div>
                      </div>
                    </div>
                  </div><!--row-->

                  <div class="card">
                      <div class="card-body">
                        <div id="tabel-temp"></div>
                        <div class="row" id="semua">
                          <div class="col-md-4 offset-md-8">
                            <h4><b>Keseluruhan</b></h4>
                          <div class="form-group">
                            <input type="text" class="form-control" name="subtotal" id="subtotal" value="0">
                          </div>
                          <div class="form-group">
                             <input type="text" class="form-control" name="drop" id="drop" onkeypress="return hanyaAngka(event)" placeholder="Drop Payment">
                          </div>
                          <div class="form-group">
                             <input type="text" class="form-control" name="kurang" id="kurang" onkeypress="return hanyaAngka(event)" placeholder="Kekurangan">
                          </div>
                          <div class="form-group">
                            <input type="text" class="form-control" name="lunas" id="lunas" onkeypress="return hanyaAngka(event)" placeholder="Pelunasan">
                          </div>
                          <button type="submit" name="submitOrder" class="btn btn-primary">Submit</button>
                        </div>
                        </div>
                      </form><br>

                        <!-- Modal -->
                        <div class="modal fade" id="orderModal" tabindex="-1" role="dialog" aria-labelledby="orderModal" aria-hidden="true">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Order Barang</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                              <div class="modal-body">
                                <form method="post" id="order-temp">
                                    <div class="row">
                                      <div class="table-responsive">
                                        <table class="table">
                                            <tr>
                                              <th>Deskripsi Barang :</th>
                                              <th><input type="text" class="form-control" name="des_barang" id="des_barang" placeholder="Deskripsi Barang"></th>
                                            </tr>
                                            <tr>
                                              <th>Qty :</th>
                                              <td><input type="text" class="form-control" name="qty_barang" id="qty_barang" onkeypress="return hanyaAngka(event)" placeholder="Qty"></td>
                                            </tr>
                                            <tr>
                                              <th>Satuan :</th>
                                              <td><input type="text" class="form-control" name="sat_barang" id="sat_barang" placeholder="Sat"></td>
                                            </tr>
                                            <tr>
                                              <th>Deskripsi Pekerjaan :</th>
                                              <td><input type="text" class="form-control" name="des_pekerjaan" id="des_pekerjaan" placeholder="Deskripsi Pekerjaan"></td>
                                            </tr>
                                            <tr>
                                              <th>Harga :</th>
                                              <td><input type="text" class="form-control" name="harga_barang" id="harga_barang" onkeypress="return hanyaAngka(event)" placeholder="Harga"></td>
                                            </tr>
                                            <tr>
                                              <th>Diskon :</th>
                                              <td><input type="text" class="form-control" name="diskon_barang" id="diskon_barang" onkeypress="return hanyaAngka(event)" placeholder="Diskon"></td>
                                            </tr>
                                            <tr>
                                              <th>Total :</th>
                                              <td><input type="text" class="form-control" name="total_harga" id="total_harga" placeholder="Total" readonly></td>
                                            </tr>
                                            <tr>
                                              <th>Keterangan :</th>
                                              <td><input type="text" class="form-control" name="ket" id="ket" placeholder="Keterangan"></td>
                                            </tr>
                                        </table>
                                  </div>
                                </form>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <a class="btn btn-primary" href="#" id="saveData" data-dismiss="modal"><i class="fa fa-check-circle fa-lg"></i></a> 
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <!-- Modal Order Success -->
        <div class="modal fade" id="<?php echo $order_success; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="alert alert-success" role="alert">
              <i class="fa fa-check-circle fa-lg bg-white"></i> Order berhasil.
            </div>
          </div>
        </div>