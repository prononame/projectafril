<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekap_model extends CI_Model{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}


	public function get_saldo_awal()
	{
		return $this->db->get('saldo_awal');
	}


	public function get_data_pendapatan()
	{
		return $this->db->get('pendapatan_so');
	}

	public function get_data_aset()
	{
		return $this->db->get('tabel_aset');
	}

	public function get_data_bahan()
	{
		return $this->db->get('tabel_bahan');
	}

	public function get_data_jahit()
	{
		return $this->db->get('tabel_jahit');
	}

	public function get_data_pengiriman()
	{
		return $this->db->get('tabel_pengiriman');
	}

	public function get_data_etc()
	{
		return $this->db->get('tabel_etc');
	}

	public function get_data_beban()
	{
		return $this->db->get('tabel_beban_bulanan');
	}

	public function get_data_insentif()
	{
		return $this->db->get('tabel_insentif');
	}

	public function get_data_cashbon()
	{
		return $this->db->get('cashflow_cash_bon');
	}

	public function get_data_safty()
	{
		return $this->db->get('safty_oprasional');
	}

	public function get_data_cashflow_bank()
	{
		return $this->db->get('cashflow_bank');
	}

	public function get_data_cashflow_cash()
	{
		return $this->db->get('cashflow_cash');
	}

	public function get_data_gojek()
	{
		return $this->db->get('tabel_gojek');
	}

	public function get_saldo_gopay()
	{
		return $this->db->get('saldo_gojek');
	}

	public function update_all_data()
	{	
		$this->db->empty_table('cashflow_bank');
		$this->db->empty_table('cashflow_cash');
		$this->db->empty_table('cashflow_cash_bon');
		$this->db->empty_table('jumlah_order');
		$this->db->update('pendapatan_so', array('jumlah' => '0'));

		$where = array('ket' => 'Lunas');
		$data_so = $this->db->get_where('data_so', $where)->result();

		foreach ($data_so as $row) {
			$so_number = $row->so_number;

			$this->db->where('so_number', $so_number);
			$this->db->delete('data_order');
			$this->db->where('so_number', $so_number);
			$this->db->delete('data_po');

		}

		$this->db->where($where);
		$this->db->delete('data_so');

		$safty_oprasional = preg_replace('/[^A-Za-z0-9\  ]/', '', $this->input->post('oprasional_bulan_depan'));
		$saldo_awal_bank = preg_replace('/[^A-Za-z0-9\  ]/', '', $this->input->post('saldo_awal_bank'));
		$saldo_awal_cash = preg_replace('/[^A-Za-z0-9\  ]/', '', $this->input->post('saldo_awal_cash'));

		$this->db->update('safty_oprasional', array('tanggal' => date('d/m/y'), 'safty_oprasional' => $safty_oprasional));

		$isi_saldo_bank = array(
							    'tanggal' => date('d/m/y'),
							    'item' => 'fund',
							    'pengeluaran' => ' ',
							    'pemasukan' => $saldo_awal_bank,
							    'saldo' => $saldo_awal_bank
							);

		$isi_saldo_cash = array(
							    'tanggal' => date('d/m/y'),
							    'item' => 'fund',
							    'pengeluaran' => ' ',
							    'pemasukan' => $saldo_awal_cash,
							    'saldo' => $saldo_awal_cash
							);

		$this->db->insert('cashflow_bank', $isi_saldo_bank);
		$this->db->insert('cashflow_cash', $isi_saldo_cash);

		$update_saldo_awal = array('tanggal' => date('d/m/y'), 'pemasukan' => '0');
		$this->db->update('saldo_awal', $update_saldo_awal);

		$update_saldo_bank = array('tanggal' => date('d/m/y'), 'pemasukan' => $saldo_awal_bank);
		$this->db->where('item', 'Saldo Bank');
		$this->db->update('saldo_awal', $update_saldo_bank);

		$update_saldo_cash = array('tanggal' => date('d/m/y'), 'pemasukan' => $saldo_awal_cash);
		$this->db->where('item', 'Saldo Cash');
		$this->db->update('saldo_awal', $update_saldo_cash);

		$update_saldo_cashbon = array('saldo_pinjaman' => '500000');
		$this->db->update('saldo_cashbon', $update_saldo_cashbon);

		$this->db->empty_table('tabel_aset');
		$this->db->empty_table('tabel_bahan');
		$this->db->empty_table('tabel_jahit');
		$this->db->empty_table('tabel_beban_bulanan');
		$this->db->empty_table('tabel_etc');
		$this->db->empty_table('tabel_gojek');
		$this->db->empty_table('tabel_insentif');
		$this->db->empty_table('tabel_pengiriman');

	}
	
}
?>