<div class="wrapper ">
    <div class="sidebar" data-color="purple" data-background-color="white" data-image="<?php echo base_url('assets/img/sidebar-1.jpg'); ?>">
      <div class="logo">
        <a href="#" class="simple-text logo-normal">
          <h6>DOUBLE A - HOUSE OF SUBLIMATION</h6>
        </a>
      </div>
      <div class="sidebar-wrapper">
        <ul class="nav">
          <li class="nav-item ">
            <a class="nav-link" href="<?php echo site_url('sales-order');?>">
              <i class="fa fa-shopping-cart"></i>
              <p>Sales Order</p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo site_url('purchase-order');?>">
              <i class="material-icons">local_offer</i>
              <p>Purchase Order</p>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="<?php echo site_url('invoice');?>">
              <i class="material-icons">content_paste</i>
              <p>Invoice</p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url('delivery-order');?>">
              <i class="material-icons">local_shipping</i>
              <p>Delivery Order</p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo site_url('laporan-sales');?>">
              <i class="material-icons">content_paste</i>
              <p>Laporan</p>
            </a>
          </li>
          <li class="nav-item active">
            <a class="nav-link" href="<?php echo site_url('pemasukan');?>">
              <i class="material-icons">monetization_on</i>
              <p>Pemasukan</p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo site_url('pengeluaran');?>">
              <i class="material-icons">money_off</i>
              <p>Pengeluaran</p>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <a class="navbar-brand" href="<?php echo site_url('pemasukan'); ?>">Pemasukan</a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">
            <form class="navbar-form">
              <div class="input-group no-border">
              </div>
            </form>
            <ul class="navbar-nav">
              
              <li class="nav-item dropdown">
                <a class="nav-link" href="#pablo" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">account_circle</i>
                  <p class="d-lg-none d-md-block">
                    Account
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <a class="dropdown-item" href="#"><?php echo $this->session->userdata('nama') ?></a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php echo site_url('/logout') ?>">Log out</a>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->

      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-lg-12 col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">INPUT PEMASUKAN</h4>
                  <p class="card-category"></p>
                </div>
                <div class="card-body">
                  <div class="row">
                    <div class="form-group col-md-4">
                      <label for="typePemasukan">Tipe Pemasukan</label>
                      <select id="typePemasukan" name="type" class="form-control">
                        <option value="nol" selected>Choose...</option>
                        <option value="data_so">Pelunasan atau Pembayaran Order</option>
                      </select>
                    </div>
                    <div class="form-group col-md-4" id="selectCari" style="visibility: hidden;">
                      <label for="selectSO">Select Nomor SO</label>
                      <select id="selectSO" name="selectSO" class="form-control">
                        <option value="nol" selected>Choose...</option>
                        <?php 
                        foreach ($dataSO as $row) {
                          echo '<option value="'.$row->so_number.'">'.$row->so_number.'</option>';
                        }
                        ?>
                      </select>
                    </div>
                    <div class="form-group col-md-4">
                      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#Modal-Pembayaran-CashBon" style="height: 40px">
                        Pembayaran Cash Bon
                      </button>
                      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#Modal-Drop-Cash" style="height: 40px">
                        Drop Cash
                      </button>
                      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#Modal-Admin-Bank" style="height: 40px">
                        Admin/Bunga bank
                      </button>
                    </div>
                  </div>
                  <br>
                  <form method="post" action="<?php echo base_url('pemasukan/proses-pemasukan'); ?>" name="submitPemasukan">
                    <div id="tabelData"></div>
                  </form>
              </div>
            </div>
          </div>
        </div>
        <!-- Modal Peminjaman Cash Bon -->
        <div class="modal fade" id="Modal-Pembayaran-CashBon" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Pembayaran Cash Bon</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <form method="post" action="<?php echo site_url('pemasukan/pembayaran_cashbon'); ?>" name="peminjaman-cashbon">
                  <div class="form-group">
                    <label for="atas_nama">Atas Nama</label>
                    <select name="atas_nama" id="atas_nama_pembayar" class="form-control">
                      <option value="nol">Choose...</option>
                      <?php 
                        foreach ($dataCashBon as $row) {
                          if ($row->saldo_pinjaman != 500000) {
                            echo '<option value="'.$row->id.'">'.$row->nama_karyawan.'</option>';
                          }
                        }
                      ?>
                    </select>
                  </div>
                  <br>
                  <div class="form-group">
                    <label for="jmlh_pinjaman">Jumlah Pembayaran</label>
                    <input type="text" class="form-control" id="jmlh_pembayaran" name="jmlh_pembayaran" onkeypress="return hanyaAngka(event)" required>
                    <span id="ket_bayar"></span>
                  </div>
                  Jenis Pembayaran :
                      <br>
                    <div class="form-check form-check-inline">
                      <input type="radio" name="tipe_pembayaran" id="tipe_pembayaran_bank_cashbon" value="bank" required>
                      <label class="form-check-label" for="tipe_pembayaran_bank_cashbon">Bank</label>
                    </div>
                    <div class="form-check form-check-inline">
                      <input type="radio" name="tipe_pembayaran" id="tipe_pembayaran_cash_cashbon" value="cash" required>
                      <label class="form-check-label" for="tipe_pembayaran_cash_cashbon">Cash</label>
                    </div>
                    <br>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="submit" onclick="Swal('Sukses')" class="btn btn-primary" name="pembayaran-cashbon" id="tombol-bayar-cashbon" style="visibility: hidden;">Submit</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>

      <!-- Modal Drop Cash -->
        <div class="modal fade" id="Modal-Drop-Cash" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Pemasukan Drop Cash</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <form method="post" action="<?php echo site_url('pemasukan/drop_cash'); ?>" name="pemasukan-drop-cash">
                  <div class="form-group">
                    <label for="jmlh_pinjaman">Jumlah Drop Cash</label>
                    <input type="text" class="form-control" id="jmlh_drop_cash" name="jmlh_drop_cash" onkeypress="return hanyaAngka(event)" required>
                  </div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary" id="tombol-drop-cash">Submit</button>
                </form>
              </div>
            </div>
          </div>
        </div>

        <!-- Modal Drop Cash -->
        <div class="modal fade" id="Modal-Admin-Bank" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Pemasukan Admin/Bunga Bank </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <form method="post" action="<?php echo site_url('pemasukan/bunga_admin_bank'); ?>" name="pemasukan-drop-cash">
                  Pemasukan Untuk :
                  <br>
                <div class="form-check form-check-inline">
                  <input type="radio" name="tipe_pemasukan" id="tipe_admin_bank" value="admin_bank" required>
                  <label class="form-check-label" for="tipe_admin_bank">Admin Bank</label>
                </div>
                <div class="form-check form-check-inline">
                  <input type="radio" name="tipe_pemasukan" id="tipe_bunga_bank" value="bunga_bank" required>
                  <label class="form-check-label" for="tipe_bunga_bank">Bunga Bank</label>
                </div>
                <br>
                <br>
                  <div class="form-group">
                    <label for="jmlh_pemasukan">Jumlah Pemasukan</label>
                    <input type="text" class="form-control" id="jmlh_pemasukan_bank" name="jmlh_pemasukan_bank" onkeypress="return hanyaAngka(event)" required>
                  </div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary" id="tombol-bunga">Submit</button>
                </form>
              </div>
            </div>
          </div>
        </div>