<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekap extends CI_Controller {

	public function __construct(){
		parent::__construct();

		$this->load->model('rekap_model');
		$this->load->library('fpdf');
		$this->load->library('form_validation');

		if ($this->session->userdata('status') != "login") {
			redirect('auth');
		}
	}

	public function index()
	{
		$data['title']='Laporan Rekap | Noname.com';
		$data_rekap = array( 
							'pendapatan_sales_order' => $this->rekap_model->get_data_pendapatan()->result(), 
							'data_aset' => $this->rekap_model->get_data_aset()->result(),
							'data_bahan' => $this->rekap_model->get_data_bahan()->result(),
							'data_jahit' => $this->rekap_model->get_data_jahit()->result(),
							'data_pengiriman' => $this->rekap_model->get_data_pengiriman()->result(),
							'data_etc' => $this->rekap_model->get_data_etc()->result(),
							'data_beban' => $this->rekap_model->get_data_beban()->result(),
							'data_insentif' => $this->rekap_model->get_data_insentif()->result(),
							'data_cashbon' => $this->rekap_model->get_data_cashbon()->result(),
							'data_safty' => $this->rekap_model->get_data_safty()->result(),
							'data_cashflow_bank' => $this->rekap_model->get_data_cashflow_bank()->result(),
							'data_cashflow_cash' => $this->rekap_model->get_data_cashflow_cash()->result(),
							'data_saldo_awal' => $this->rekap_model->get_saldo_awal()->result(),
							'data_gojek' => $this->rekap_model->get_data_gojek()->result(),
							'data_saldo_gopay' => $this->rekap_model->get_saldo_gopay()->result()
						); 

		$this->load->view('templates/header', $data);
		$this->load->view('rekap', $data_rekap);
		$this->load->view('templates/footer');
	}

	public function pembukuan()
	{
		$this->rekap_model->update_all_data();
		redirect('rekap');
	}

	public function cetak_rekap()
	{	
		$pendapatan_sales_order =  $this->rekap_model->get_data_pendapatan()->result();
		$data_aset = $this->rekap_model->get_data_aset()->result();
		$data_bahan = $this->rekap_model->get_data_bahan()->result();
		$data_jahit = $this->rekap_model->get_data_jahit()->result();
		$data_pengiriman = $this->rekap_model->get_data_pengiriman()->result();
		$data_etc = $this->rekap_model->get_data_etc()->result();
		$data_beban = $this->rekap_model->get_data_beban()->result();
		$data_insentif = $this->rekap_model->get_data_insentif()->result();
		$data_cashbon = $this->rekap_model->get_data_cashbon()->result();
		$data_safty = $this->rekap_model->get_data_safty()->result();
		$data_cashflow_bank = $this->rekap_model->get_data_cashflow_bank()->result();
		$data_cashflow_cash = $this->rekap_model->get_data_cashflow_cash()->result();
		$data_saldo_awal = $this->rekap_model->get_saldo_awal()->result();
		$data_gojek = $this->rekap_model->get_data_gojek()->result();
		$data_saldo_gopay = $this->rekap_model->get_saldo_gopay()->result();		

		//Lebar A4= 210mm
		//Margin default= 10mm tiap sisi
		//Bidang Horizontal yang dapat ditulisi= 210 - (10*2) = 190mm

		  $saldo_awal = 0;
          $pengeluaran_aset = 0;
          $pengeluaran_bahan = 0;
          $pengeluaran_jahit = 0;
          $pengeluaran_pengiriman = 0;
          $pengeluaran_etc = 0;
          $pengeluaran_beban = 0;
          $pengeluaran_insentif = 0;
          $pengeluaran_cashbon = 0;
          $pengeluaran_gojek = 0;
          $pemasukan_cashbon = 0;
          $pengeluaran_cashflow_bank = 0;
          $pemasukan_cashflow_bank = 0;
          $pengeluaran_cashflow_cash = 0;
          $pemasukan_cashflow_cash = 0;
          $keseluruhan_cashflow_bank = 0;
          $keseluruhan_cashflow_cash = 0;

          foreach ($data_saldo_awal as $row) {
            if ($row->pemasukan == ' ') {
              $saldo_awal += 0;
            }else{
              $saldo_awal += preg_replace('/[^A-Za-z0-9\  ]/', '', $row->pemasukan);
            }
          }

          foreach ($pendapatan_sales_order as $row) {
            $pendapatan_so = preg_replace('/[^A-Za-z0-9\  ]/', '', $row->jumlah);
          }

          foreach ($data_aset as $row) {
            $pengeluaran_aset += preg_replace('/[^A-Za-z0-9\  ]/', '', $row->pengeluaran);
          }

          foreach ($data_bahan as $row) {
            $pengeluaran_bahan += preg_replace('/[^A-Za-z0-9\  ]/', '', $row->pengeluaran);
          }

          foreach ($data_jahit as $row) {
            $pengeluaran_jahit += preg_replace('/[^A-Za-z0-9\  ]/', '', $row->pengeluaran);
          }

          foreach ($data_pengiriman as $row) {
            $pengeluaran_pengiriman += preg_replace('/[^A-Za-z0-9\  ]/', '', $row->pengeluaran);
          }

          foreach ($data_etc as $row) {
            $pengeluaran_etc += preg_replace('/[^A-Za-z0-9\  ]/', '', $row->pengeluaran);
          }

          foreach ($data_beban as $row) {
            $pengeluaran_beban += preg_replace('/[^A-Za-z0-9\  ]/', '', $row->pengeluaran);
          }

          foreach ($data_insentif as $row) {
            $pengeluaran_insentif += preg_replace('/[^A-Za-z0-9\  ]/', '', $row->insentif);
          }

          foreach ($data_cashbon as $row) {
            if ($row->pinjaman == ' ') {
              $pengeluaran_cashbon += 0;
            }else{
              $pengeluaran_cashbon += preg_replace('/[^A-Za-z0-9\  ]/', '', $row->pinjaman);
            }

            if ($row->pengembalian == ' ') {
              $pemasukan_cashbon += 0;
            }else{
              $pemasukan_cashbon += preg_replace('/[^A-Za-z0-9\  ]/', '', $row->pengembalian);
            }
            
            
          }

          foreach ($data_safty as $row) {
            $safty_oprasional = $row->safty_oprasional;
          }

          foreach ($data_saldo_gopay as $row) {
            $saldo_gojek = $row->jumlah;
          }

          foreach ($data_gojek as $row) {
            $pengeluaran_gojek += preg_replace('/[^A-Za-z0-9\  ]/', '', $row->pengeluaran);
          }
          
          $sub_pemasukan = $saldo_awal + $pendapatan_so + $pemasukan_cashbon;
          $sub_pengeluaran = $pengeluaran_aset + $pengeluaran_bahan + $pengeluaran_jahit + $pengeluaran_pengiriman + $pengeluaran_gojek + $pengeluaran_cashbon + $pengeluaran_etc + $pengeluaran_beban + $pengeluaran_insentif;
          $sub_akhir = $sub_pemasukan - $sub_pengeluaran;
          $pendapatan_kotor = $saldo_awal + $pendapatan_so;

		$this->fpdf->AddPage();

		//SetFont ke Arial, B, 10pt
		$this->fpdf->SetFont('Arial','B', 12);
		//Cell(lebar, tinggi, teks, border, end line, [align])
		$this->fpdf->Cell(0, 5, 'DOUBLE A - HOUSE OF SUBLIMATION',0,1, 'C');
		$this->fpdf->Cell(0, 1, '', 0,1, 'C');
		$this->fpdf->Cell(0, 5, 'LAPORAN REKAPITULASI KEUANGAN '.date('F Y'),0,1, 'C');

		$this->fpdf->Cell(0, 10, '', 0,1, 'C');

		$this->fpdf->SetFont('Arial','B',10);

		$this->fpdf->Cell(7,6,'NO',1,0);
		$this->fpdf->Cell(97,6,'Item',1,0);
		$this->fpdf->Cell(40,6,'Pemasukan',1,0);
		$this->fpdf->Cell(40,6,'Pengeluaran',1,1);

		 
		$this->fpdf->SetFont('Arial','',10);

		$item = array("Saldo Awal","S.O Bulan Sebelumnya","S.O".date('F'),"Pengadaan Aset","Bahan Baku","Pekerjaan Jahit","Pengiriman","Biaya Tidak Terduga","Beban Bulanan","Insentif","Cash Bon");
		$data = array(
						array(
							'item'		=> 'Saldo Awal',
							'pemasukan' => $saldo_awal,
							'pengeluaran' => '-'
						),
						array(
							'item'		=> 'Pendapatan Sales Order',
							'pemasukan' => $pendapatan_so,
							'pengeluaran' => '-'
						),
						array(
							'item'		=> 'Pengadaan Aset',
							'pemasukan' => '-',
							'pengeluaran' => $pengeluaran_aset
						),
						array(
							'item'		=> 'Bahan Baku',
							'pemasukan' => '-',
							'pengeluaran' => $pengeluaran_bahan
						),
						array(
							'item'		=> 'Pekerjaan Jahit',
							'pemasukan' => '-',
							'pengeluaran' => $pengeluaran_jahit
						),
						array(
							'item'		=> 'Pengiriman',
							'pemasukan' => '-',
							'pengeluaran' => $pengeluaran_pengiriman
						),
						array(
							'item'		=> 'Pengiriman',
							'pemasukan' => '-',
							'pengeluaran' => $pengeluaran_pengiriman
						),
						array(
							'item'		=> 'Gojek',
							'pemasukan' => '-',
							'pengeluaran' => $pengeluaran_gojek
						),
						array(
							'item'		=> 'Biaya Tidak Terduga',
							'pemasukan' => '-',
							'pengeluaran' => $pengeluaran_etc
						),
						array(
							'item'		=> 'Beban Bulanan',
							'pemasukan' => '-',
							'pengeluaran' => $pengeluaran_beban
						),
						array(
							'item'		=> 'Insentif',
							'pemasukan' => '-',
							'pengeluaran' => $pengeluaran_insentif
						),
						array(
							'item'		=> 'Cash Bon',
							'pemasukan' => $pemasukan_cashbon,
							'pengeluaran' => $pengeluaran_cashbon
						)
					);
		
		$no = 1;
		
		foreach ($data as $row) {

			if ($row['pemasukan'] == '-') {
              $pemasukan = '-';
            }else{
              $pemasukan = "Rp. ".number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $row['pemasukan']),2,',','.');
            }

            if ($row['pengeluaran'] == '-') {
              $pengeluaran = '-';
            }else{
              $pengeluaran = "Rp. ".number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $row['pengeluaran']),2,',','.');
            }

			$this->fpdf->Cell(7,6,$no++,1,0);
			$this->fpdf->Cell(97,6,$row['item'],1,0);
			$this->fpdf->Cell(40,6,$pemasukan,1,0);
			$this->fpdf->Cell(40,6,$pengeluaran,1,1);

			}

			$this->fpdf->Cell(0, 5, '', 0,1, 'C');

			$this->fpdf->Cell(84,6,'',0,0);
			$this->fpdf->Cell(20,6,'Sub Total',1,0);
			$this->fpdf->Cell(40,6,"Rp. ".number_format($sub_pemasukan,2,',','.'),1,0);
			$this->fpdf->Cell(40,6,"Rp. ".number_format($sub_pengeluaran,2,',','.'),1,1);
			$saldo_akhir = $sub_pemasukan - $sub_pengeluaran;
			$this->fpdf->Cell(84,6,'',0,0);
			$this->fpdf->Cell(20,6,'Saldo Akhir',1,0);
			$this->fpdf->Cell(40,6,"Rp. ".number_format($saldo_akhir,2,',','.'),1,1);

			$this->fpdf->AddPage();

			//SetFont ke Arial, B, 10pt
			$this->fpdf->SetFont('Arial','B', 12);
			//Cell(lebar, tinggi, teks, border, end line, [align])
			$this->fpdf->Cell(0, 5, 'DOUBLE A - HOUSE OF SUBLIMATION',0,1, 'C');
			$this->fpdf->Cell(0, 1, '', 0,1, 'C');
			$this->fpdf->Cell(0, 5, 'LAPORAN LABA RUGI BULAN '.date('F Y'),0,1, 'C');

			$this->fpdf->Cell(0, 10, '', 0,1, 'C');

			$this->fpdf->SetFont('Arial','B',10);

			$this->fpdf->Cell(0, 7, 'Pendapatan Produksi', 0,1, 'L');

			$this->fpdf->Cell(57,6,'Saldo Awal ',0,0);
			$this->fpdf->Cell(3,6,': ',0,0);
			$this->fpdf->Cell(35,6,"Rp. ".number_format($saldo_awal,2,',','.'),0,1);
			$this->fpdf->Cell(57,6,'Pendapatan Sales Order',0,0);
			$this->fpdf->Cell(3,6,': ',0,0);
			$this->fpdf->Cell(35,6,"Rp. ".number_format($pendapatan_so,2,',','.'),0,1);

			$this->fpdf->Line(10, 51, 210-10, 51);
			$this->fpdf->Cell(0, 1, '', 0,1, 'C');
			$this->fpdf->Cell(120,6,'',0,0);
			$this->fpdf->Cell(35,6,'Pendapatan Kotor',0,0);
			$this->fpdf->Cell(35,6,"Rp. ".number_format($pendapatan_kotor,2,',','.'),0,1);

			$this->fpdf->Cell(0, 5, '', 0,1, 'C');

			$this->fpdf->Cell(0, 7, 'Harga Pokok Produksi', 0,1, 'L');

			$this->fpdf->Cell(57,6,'Bahan Baku ',0,0);
			$this->fpdf->Cell(3,6,': ',0,0);
			$this->fpdf->Cell(35,6,"Rp. ".number_format($pengeluaran_bahan,2,',','.'),0,1);
			$this->fpdf->Cell(57,6,'Pekerjaan Jahit',0,0);
			$this->fpdf->Cell(3,6,': ',0,0);
			$this->fpdf->Cell(35,6,"Rp. ".number_format($pengeluaran_jahit,2,',','.'),0,1);
			
			$this->fpdf->Line(10, 81, 210-10, 81);

			$phpp = $pendapatan_kotor - ($pengeluaran_bahan + $pengeluaran_jahit);
			$this->fpdf->Cell(0, 1, '', 0,1, 'C');
			$this->fpdf->Cell(60,6,'',0,0);
			$this->fpdf->Cell(95,6,'Pendapatan Setelah Dikurangi Harga Pokok Produksi',0,0);
			$this->fpdf->Cell(35,6,"Rp. ".number_format($phpp,2,',','.'),0,1);

			$this->fpdf->Cell(0, 5, '', 0,1, 'C');

			$this->fpdf->Cell(0, 7, 'Beban Produksi', 0,1, 'L');

			$this->fpdf->Cell(57,6,'Pengiriman ',0,0);
			$this->fpdf->Cell(3,6,': ',0,0);
			$this->fpdf->Cell(35,6,"Rp. ".number_format($pengeluaran_pengiriman,2,',','.'),0,1);
			$this->fpdf->Cell(57,6,'Biaya Tidak Terduga',0,0);
			$this->fpdf->Cell(3,6,': ',0,0);
			$this->fpdf->Cell(35,6,"Rp. ".number_format($pengeluaran_etc,2,',','.'),0,1);
			$this->fpdf->Cell(57,6,'Beban Bulanan ',0,0);
			$this->fpdf->Cell(3,6,': ',0,0);
			$this->fpdf->Cell(35,6,"Rp. ".number_format($pengeluaran_beban,2,',','.'),0,1);
			$this->fpdf->Cell(57,6,'Insentif',0,0);
			$this->fpdf->Cell(3,6,': ',0,0);
			$this->fpdf->Cell(35,6,"Rp. ".number_format($pengeluaran_insentif,2,',','.'),0,1);
			$this->fpdf->Cell(57,6,'Cash Bon Karyawan',0,0);
			$this->fpdf->Cell(3,6,': ',0,0);
			$this->fpdf->Cell(35,6,"Rp. ".number_format($pengeluaran_cashbon,2,',','.'),0,1);
			
			$this->fpdf->Line(10, 130, 210-10, 130);

			$pendapatan_bersih = $phpp - ($pengeluaran_pengiriman + $pengeluaran_etc + $pengeluaran_beban + $pengeluaran_insentif + $pengeluaran_cashbon);

			$this->fpdf->Cell(0, 1, '', 0,1, 'C');

			$this->fpdf->Cell(120,6,'',0,0);
			$this->fpdf->Cell(35,6,'Pendapatan Bersih',0,0);
			$this->fpdf->Cell(35,6,"Rp. ".number_format($pendapatan_bersih,2,',','.'),0,1);

			$this->fpdf->AddPage();

			//SetFont ke Arial, B, 10pt
			$this->fpdf->SetFont('Arial','B', 12);
			//Cell(lebar, tinggi, teks, border, end line, [align])
			$this->fpdf->Cell(0, 5, 'DOUBLE A - HOUSE OF SUBLIMATION',0,1, 'C');
			$this->fpdf->Cell(0, 1, '', 0,1, 'C');
			$this->fpdf->Cell(0, 5, 'LAPORAN PROFIT '.date('F Y'),0,1, 'C');

			$this->fpdf->Cell(0, 10, '', 0,1, 'C');

			$this->fpdf->SetFont('Arial','B',10);

			$this->fpdf->Cell(120,6,'',0,0);
			$this->fpdf->Cell(35,6,'Pendapatan Bersih',0,0);
			$this->fpdf->Cell(35,6,"Rp. ".number_format($pendapatan_bersih,2,',','.'),0,1);

			$this->fpdf->Cell(0, 7, 'Oprasional', 0,1, 'L');

			$this->fpdf->Cell(57,6,'Safty Oprasional ',0,0);
			$this->fpdf->Cell(3,6,': ',0,0);
			$this->fpdf->Cell(35,6,"Rp. ".number_format($safty_oprasional,2,',','.'),0,1);

			$pbso = $pendapatan_bersih - $safty_oprasional;

			$this->fpdf->Line(10, 50, 210-10, 50);
			$this->fpdf->Cell(60,6,'',0,0);
			$this->fpdf->Cell(95,6,'Profit Bersih Setelah Dikurangi Safty Oprasional',0,0);
			$this->fpdf->Cell(35,6,"Rp. ".number_format($pbso,2,',','.'),0,1);

			$this->fpdf->Cell(0, 5, '', 0,1, 'C');

			$this->fpdf->Cell(0, 7, 'Profit Infestor', 0,1, 'L');

			$pbi = $pbso * 70 / 100;

			$this->fpdf->Cell(57,6,'Profit Bersih ',0,0);
			$this->fpdf->Cell(3,6,': ',0,0);
			$this->fpdf->Cell(35,6,"Rp. ".number_format($pbi,2,',','.'),0,1);
			$this->fpdf->Cell(57,6,'Perofit Berbentuk Aset',0,0);
			$this->fpdf->Cell(3,6,': ',0,0);
			$this->fpdf->Cell(35,6,"Rp. ".number_format($pengeluaran_aset,2,',','.'),0,1);
			$this->fpdf->Cell(57,6,'Perofit Berbentuk Cash',0,0);
			$this->fpdf->Cell(3,6,': ',0,0);
			$pbc = $pbi - $pengeluaran_aset;
			$this->fpdf->Cell(35,6,"Rp. ".number_format($pbc,2,',','.'),0,1);

			$this->fpdf->Cell(0, 5, '', 0,1, 'C');

			$this->fpdf->Cell(0, 7, 'Profit Direksi', 0,1, 'L');
			$pbd = $pbso * 30 / 100;
			$this->fpdf->Cell(57,6,'Profit Bersih ',0,0);
			$this->fpdf->Cell(3,6,': ',0,0);
			$this->fpdf->Cell(35,6,"Rp. ".number_format($pbd,2,',','.'),0,1);
			
			$this->fpdf->Line(10, 104, 210-10, 104);

			$this->fpdf->Cell(103,6,'',0,0);
			$this->fpdf->Cell(52,6,'Oprasional Bulan '.date('F',strtotime("+1 month")),0,0);
			$this->fpdf->Cell(35,6,"Rp. ".number_format($safty_oprasional,2,',','.'),0,1);

			$this->fpdf->Output('Laporan-Rekapitulasi-'.date('F Y').'.pdf', 'I');
	}

}
