<div class="table-responsive">
   <table class="table">
     <thead>
      <tr>
        <th scope="col"><b>No</b></th>
        <th scope="col"><b>Po Number</b></th>
        <th scope="col"><b>PO Date</b></th>
        <th scope="col"><b>Ship Via</b></th>
        <th scope="col"><b>Nama Penerima</b></th>
        <th scope="col"><b>Alamat</b></th>
        <th scope="col"><b>No Telfon</b></th>
      </tr>
     </thead>
     <tbody>
      <?php
        $no = 1;
        foreach ($delivery_order as $row) {
      ?>
      <tr>
        <th><?php echo $no++ ?></th>
        <td><?php echo $row->po_number ?></td>
        <td><?php echo $row->po_date ?></td>
        <td><?php echo $row->ship_via ?></td>
        <td><?php echo $row->nama_penerima ?></td>
        <td><?php echo $row->alamat ?></td>
        <td><?php echo $row->no_tlp ?></td>
      </tr>
      <?php
        }
      ?>
     </tbody>
   </table>
</div>