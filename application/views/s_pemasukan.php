<div class="wrapper "> 
    <div class="sidebar" data-color="purple" data-background-color="white" data-image="<?php echo base_url('assets/img/sidebar-1.jpg'); ?>">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
      <div class="logo">
        <a href="#" class="simple-text logo-normal">
          PROJECTALFRIL
        </a>
      </div>
      <div class="sidebar-wrapper">
        <ul class="nav">
          <li class="nav-item">
            <a class="nav-link" href="<?php echo site_url('laporan');?>">
              <i class="fa fa-shopping-cart"></i>
              <p>Laporan</p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo site_url('order-prioritas');?>">
              <i class="fa fa-shopping-cart"></i>
              <p>Order Prioritas</p>
            </a>
          </li>
          <li class="nav-item active">
            <a class="nav-link" href="<?php echo site_url('s-pemasukan');?>">
              <i class="fa fa-shopping-cart"></i>
              <p>Pemasukan</p>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="<?php echo site_url('rekap');?>">
              <i class="material-icons">content_paste</i>
              <p>Rekap</p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url('data-karyawan');?>">
              <i class="material-icons">content_paste</i>
              <p>Data Karyawan</p>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <a class="navbar-brand" href="<?php echo site_url('s-pemasukan');?>">Pemasukan</a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">
            <form class="navbar-form">
              <div class="input-group no-border">
               
                  <div class="ripple-container"></div>
                </button>
              </div>
            </form>
            <ul class="navbar-nav">
              <li class="nav-item dropdown">
                <a class="nav-link" href="#pablo" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">account_circle</i>
                  <p class="d-lg-none d-md-block">
                    Account
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <a class="dropdown-item" href="#"><?php echo $this->session->userdata('nama') ?></a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php echo site_url('/logout') ?>">Log out</a>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->

      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-lg-12 col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">INPUT PEMASUKAN</h4>
                  <p class="card-category"></p>
                </div>
                <div class="card-body">
                  <div class="row">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#Modal-Saldo-Awal">
                      Saldo Awal
                    </button>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#Modal-Safty-Oprasional">
                      Safty Oprasional
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <!-- Modal Pemasukan Saldo Awal -->
        <div class="modal fade" id="Modal-Saldo-Awal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Saldo Awal</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <form method="post" action="<?php echo site_url('s_pemasukan/saldo_awal'); ?>" name="peminjaman-cashbon">
                  <div class="form-group">
                    <label for="atas_nama">Saldo awal untuk:</label>
                    <select name="jenis_simpanan" id="jenis_simpanan" class="form-control">
                      <option value="nol">Choose...</option>
                      <option value="saldo_bank">Bank</option>
                      <option value="saldo_cash">Cash</option>
                    </select>
                  </div>
                  <br>
                  <div class="form-group">
                    <label for="jmlh_pinjaman">Jumlah Saldo</label>
                    <input type="text" class="form-control" id="jmlh_saldo" name="jmlh_saldo" required>
                  </div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary" name="saldo-awal" id="tombol-saldo-awal" style="visibility: hidden;">Submit</button>
                </form>
              </div>
            </div>
          </div>
        </div>

        <!-- Modal Pemasukan Safty Oprasional -->
        <div class="modal fade" id="Modal-Safty-Oprasional" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Safty Oprasional</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <form method="post" action="<?php echo site_url('s_pemasukan/safty_oprasional'); ?>" name="peminjaman-cashbon">
                  <div class="form-group">
                    <label for="jmlh_oprasional">Jumlah Safty Oprasional</label>
                    <input type="text" class="form-control" id="jmlh_oprasional" name="jmlh_oprasional" required>
                  </div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary" name="safty_oprasional" id="tombol-oprasional">Submit</button>
                </form>
              </div>
            </div>
          </div>
        </div>
 