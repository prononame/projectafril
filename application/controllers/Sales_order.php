<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sales_order extends CI_Controller {

	public function __construct(){
		parent::__construct();

		$this->load->model('sales_order_model');
		$this->load->library('form_validation');

		if ($this->session->userdata('status') != "login") {
			redirect('auth');
		}

	}

	public function index()
	{
		if ($this->session->userdata('success_stat') == "yes") {
	      $success = $this->session->userdata('success');
	    }else{
	      $success = ' ';
	    }

		$data['title']='Sales Order | double-a.com';
		$JumlahOrderan = $this->sales_order_model->getJumlah_order()->result();

		$data2 = array(
						'jumlah_order' => $JumlahOrderan,
						'order_success' => $success 
					);

		$this->load->view('templates/header', $data);
		$this->load->view('sales_order', $data2);
		$this->load->view('templates/footer');
	}

	public function proses_temp_order()
	{	
		$this->sales_order_model->save_temp_order();
	}

	public function proses_ambil_total()
	{	
		$data = $this->sales_order_model->getData()->result();
		echo json_encode($data);
	}

	public function get_temp_order($id)
	{	
		$data = $this->sales_order_model->getData_by_id($id)->result();
		echo json_encode($data);
	}

	public function edit_temp_order($id)
	{	
		$data = $this->sales_order_model->edit_temp_order($id);
	}

	public function deleteTemp()
	{	
		$id = $this->input->post('id');
		$this->sales_order_model->deleteTemp($id);
	}

	public function proses_order()
	{	
		$this->sales_order_model->getData_so();
		$this->sales_order_model->getData_po();
		$this->sales_order_model->getData_order();
		$this->sales_order_model->insert_data_order();
		$this->sales_order_model->delete_temp();

		$success = 'Modal-success';
        $data_success = array(
                             'success' => $success,
                             'success_stat' => 'yes'
                           );
        $this->session->set_userdata($data_success);

		redirect('sales-order');
	}

	function data_temp_order(){
        $data['temp_sales_order'] = $this->sales_order_model->getData()->result();
        $this->load->view('tabel_sales_order', $data);
    }

    public function autocomplete(){
    	// tangkap variabel keyword dari URL
		$keyword = $this->uri->segment(3);

		// cari di database
		$data = $this->db->from('tabel_pelanggan')->like('nama_pelanggan',$keyword)->get();	

		// format keluaran di dalam array
		foreach($data->result() as $row)
		{
			$arr['query'] = $keyword;
			$arr['suggestions'][] = array(
				'value'	=>$row->nama_pelanggan,
				'alamat'	=>$row->alamat,
				'telfon'	=>$row->no_tlp

			);
		}
		// minimal PHP 5.2
		echo json_encode($arr);
    }

    public function cetak_so()
	{	
		$data_so =  $this->sales_order_model->getData_so()->result();
		foreach ($data_so as $so) {
			$so_number = $so->so_number;
			$so_date = $so->so_date;
			$nama_pelanggan = $so->nama_pelanggan;
			$alamat_pelanggan = $so->alamat;
			$no_tlp_pelanggan = $so->no_tlp;
			$grand_total = $so->sub_total;
			$dp = $so->drop_payment;
			$catatan = $so->catatan;
			$kekurangan = $so->kekurangan; 
		}

		$data_po =  $this->sales_order_model->getData_po()->result();
		foreach ($data_po as $po) {
			$po_number = $po->po_number;
			$po_date = $po->po_date;
			$ship_via = $po->ship_via;
			$nama_penerima = $po->nama_penerima;
			$alamat_pengiriman = $po->alamat;
			$no_tlp_penerima = $po->no_tlp;
		}

		$grand_total = "Rp. " . number_format($grand_total,2,',','.');
		$dp = "Rp. ".$dp.",00";
		$kekurangan = "Rp. " . number_format($kekurangan,2,',','.');

		$this->load->library('fpdf');

		//Lebar A4= 210mm
		//Margin default= 10mm tiap sisi
		//Bidang Horizontal yang dapat ditulisi= 210 - (10*2) = 190mm

		$this->fpdf->AddPage("L");

		//SetFont ke Arial, B, 10pt
		$this->fpdf->SetFont('Arial','B', 15);

		$this->fpdf->Image(base_url().'assets/img/logo.png', 5, 0, 30, 0);

		//Cell(lebar, tinggi, teks, border, end line, [align])
		$this->fpdf->Cell(0, 15, 'DOUBLE A - HOUSE OF SUBLIMATION',0,1, 'C');
		$this->fpdf->Cell(0, 1, '', 0,1, 'C');
		$this->fpdf->Cell(0, 5, 'SALES ORDER',0,1, 'C');

		$this->fpdf->Cell(0, 10, '', 0,1, 'C');

		$this->fpdf->SetFont('Arial','', 11);

		$this->fpdf->Cell(70, 5, 'Order By :',0);
		$this->fpdf->Cell(29,6,'',0);
		$this->fpdf->Cell(70, 5, 'Pengiriman :',0);
		$this->fpdf->Cell(35,6,'',0);
		$this->fpdf->Cell(72, 6, 'SO DATE : '.$so_date,1);

		$this->fpdf->Cell(0, 6, '', 0,1, 'C');

		$this->fpdf->Cell(91, 6,$nama_pelanggan,1);
		$this->fpdf->Cell(3, 6,'',0);
		$this->fpdf->Cell(96, 6,$nama_penerima,1);
		$this->fpdf->Cell(14,6,'',0);
		$this->fpdf->Cell(72, 6,'SO NUMBER : '.$so_number,1);

		$this->fpdf->Cell(0, 6, '', 0,1, 'C');

		$this->fpdf->Cell(91, 12,$alamat_pelanggan,1);
		$this->fpdf->Cell(3, 6,'',0);
		$this->fpdf->Cell(96, 12,$alamat_pengiriman,1);
		$this->fpdf->Cell(14,6,'',0);
		$this->fpdf->Cell(72, 6,'SHIP VIA : '.$ship_via,1);

		$this->fpdf->Cell(0, 6, '', 0,1, 'C');

		$this->fpdf->Cell(67, 6,'',0);
		$this->fpdf->Cell(3, 6,'',0);
		$this->fpdf->Cell(67, 6,'',0);
		$this->fpdf->Cell(67,6,'',0);
		$this->fpdf->Cell(72, 6,'PO DATE : '.$po_date,1);

		$this->fpdf->Cell(0, 6, '', 0,1, 'C');

		$this->fpdf->Cell(91, 6,$no_tlp_pelanggan,1);
		$this->fpdf->Cell(3, 6,'',0);
		$this->fpdf->Cell(96, 6,$no_tlp_penerima,1);
		$this->fpdf->Cell(14,6,'',0);
		$this->fpdf->Cell(72, 6,'PO NUMBER : '.$po_number,1);

		$this->fpdf->Cell(0, 10, '', 0,1, 'C');

		$this->fpdf->SetFont('Arial','B',10);

		$this->fpdf->Cell(7,6,'NO',1,0, 'C');
		$this->fpdf->Cell(65,6,'Deskripsi Barang',1,0, 'C');
		$this->fpdf->Cell(9,6,'Qty',1,0, 'C');
		$this->fpdf->Cell(9,6,'Sat',1,0, 'C');
		$this->fpdf->Cell(65,6,'Deskripsi Pekerjaan',1,0, 'C');
		$this->fpdf->Cell(35,6,'Harga',1,0, 'C');
		$this->fpdf->Cell(14,6,'Diskon',1,0, 'C');
		$this->fpdf->Cell(35,6,'Total',1,0, 'C');
		$this->fpdf->Cell(37,6,'Keterangan',1,1, 'C');
		 
		$this->fpdf->SetFont('Arial','',10);
		//Table with 2 rows and 4 columns
		$this->fpdf->SetWidths(array(7,65,9,9,65,35,14,35,37));
		$this->fpdf->SetAligns(array('C','','C','C','','C','C','C',''));
		$this->fpdf->SetFont('Arial','',10);
		
		$this->fpdf->SetWidths(array(7,65,9,9,65,35,14,35,37));

		$no = 1;
		$r = 1;
		$data_order =  $this->sales_order_model->getData_order()->result();
		foreach ($data_order as $row) {
			$harga = "Rp. " . number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $row->harga),0,',','.');
			$total = "Rp. " . number_format(preg_replace('/[^A-Za-z0-9\  ]/', '', $row->total),0,',','.');
			$this->fpdf->Row(array($no++,$row->des_barang,$row->qty,$row->sat,$row->des_pekerjaan,$harga,$row->diskon,$total,$row->ket));
			$r += 1;
		} 

		for ($i=$r; $i < 11; $i++) { 
			$this->fpdf->Row(array($no++,'','','','','','','',''));
		} 

			$this->fpdf->Image(base_url().'assets/img/wm.png', 0, 0, 300, 0);

		$this->fpdf->Cell(0, 3, ' ', 0,1, 'L');
		$this->fpdf->Cell(0, 7, 'Catatan :', 0,1, 'L');
		$this->fpdf->Cell(190, 12,$catatan,1);
		$this->fpdf->Cell(15, 6,'',0);
		$this->fpdf->Cell(35, 6,'Grand Total ',1);
		$this->fpdf->Cell(36, 6,$grand_total,1,'C');

		$this->fpdf->Cell(0, 6, '', 0,1, 'C');
		
		$this->fpdf->Cell(7,6,'',0);
		$this->fpdf->Cell(90, 12,'',0);
		$this->fpdf->Cell(108, 6,'',0);
		$this->fpdf->Cell(35, 6,'Drop Payment ',1);
		$this->fpdf->Cell(36, 6,$dp,1,'C');

		$this->fpdf->Cell(0, 6, '', 0,1, 'C');
		
		$this->fpdf->Cell(7,6,'',0);
		$this->fpdf->SetFont('Arial','B',10);
		$this->fpdf->Cell(90, 12,'PAYMENT : BCA A/N ALFRILO MUHAMMAD RISALDY, 1390460071',0);
		$this->fpdf->SetFont('Arial','',10);
		$this->fpdf->Cell(108, 6,'',0);

		$this->fpdf->Cell(0, 6, '', 0,1, 'C');
		$this->fpdf->SetFont('Arial','B',10);

		$this->fpdf->Cell(5,6,'',0);
		$this->fpdf->Cell(10, 12,'SUPERVISOR',0);
		$this->fpdf->Cell(84, 6,'',0);
		$this->fpdf->Cell(10, 12,'ADMIN',0);
		$this->fpdf->Cell(96, 6,'',0);
		
		$this->fpdf->Cell(0, 15, '', 0,1, 'C');
		$this->fpdf->SetFont('Arial','',10);

		$this->fpdf->Cell(1,5,'',0);
		$this->fpdf->Cell(10, 12,'_________________',0);
		$this->fpdf->Cell(77, 6,'',0);
		$this->fpdf->Cell(10, 12,'_________________',0);

		$this->fpdf->Cell(0, 3, '', 0,1, 'C');

		$this->fpdf->Cell(1,6,'',0);
		$this->fpdf->Cell(10, 12,'Tanggal : '.$so_date,0);
		$this->fpdf->Cell(77, 6,'',0);
		$this->fpdf->Cell(10, 12,'Tanggal : '.$so_date,0);


		$this->fpdf->Output($so_number.'.pdf', 'I');

		$unset = array('success','success_stat');
		$this->session->unset_userdata($unset);
	}
}
